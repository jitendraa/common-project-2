-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 10, 2020 at 06:19 AM
-- Server version: 5.6.47-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vip_number_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `plain_pass` varchar(255) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `type` int(10) NOT NULL DEFAULT '1',
  `role` varchar(25) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_by` int(12) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'Active',
  `ip_addr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `fname`, `lname`, `plain_pass`, `phone`, `type`, `role`, `entry_date`, `entry_by`, `status`, `ip_addr`) VALUES
(1, 'admin', '95228f3702f78bbb4cbbd356a01a5e2b', '', 'admin', '', 'ADMIN!@12', '', 1, '', '2020-03-12 16:56:51', 0, 'Active', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_slug` text NOT NULL,
  `name` varchar(128) NOT NULL,
  `image` varchar(100) NOT NULL,
  `company` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_addr` varchar(128) NOT NULL,
  `status` varchar(128) NOT NULL DEFAULT 'Active',
  `popularity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `title`, `title_slug`, `name`, `image`, `company`, `description`, `meta_title`, `meta_keyword`, `meta_description`, `entry_by`, `entry_date`, `ip_addr`, `status`, `popularity`) VALUES
(4, 'MYSTERY OF PITRU DOSHA IN YOUR LIFEddfdfd', 'mystery-of-pitru-dosha-in-your-lifeddfdfd', 'Sumant And Sumeet Kaul', 'b3c5beaf92fd11ddb9f41f975755d653.png', 'Sumant And Sumeet Kaul', '<p style=\"text-align:justify\"><strong>The most common Karmic trouble seen in almost every house is between daughter-in-law and mother-in-law. Karma Guru ji, can the technique resolve this Karma?</strong></p>\r\n\r\n<p style=\"text-align:justify\">Many people come with the karma of daughter-in-law and mother in law; it&rsquo;s a prevalent karma all over the world. However, people don&rsquo;t realize that it&rsquo;s not an individual karma but it&rsquo;s a family karma. The family travels together with a family karma. Members of the family do not consciously take this karma but the family consciousness decides along with unconscious of the individual to delegate the karma. So whenever there is a mother-in-law daughter-in-law dispute and you can see it in the ancestors also in that family. It means at the great-grandfather level, the mother-in-law died early or had some trauma and couldn&rsquo;t give love to her daughter-in-law and this way the soul has been forgotten. In the family, now, there is a dosha &nbsp;I.e defect regarding not receiving love between mother in law and daughter in law. The new daughter in laws who come in that family unconsciously will take that family karma and out of bonding, sympathy, and love will take the burden of their mother in law&rsquo;s - mother In law to represent them in family because they didn&rsquo;t get love with their daughter in law and to settle that karma the new daughter in law has karma with her mother in law. This script continuously runs until someone in the family consciousness breaks it.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>What can be the underlying cause of migraines and depression?</strong></p>\r\n\r\n<p style=\"text-align:justify\">One lady aborted a child and her next child was having migraines. Her mother started expecting a lot from her because she lost her earlier child and that suppressed grief is stored inside the mother and the present child is identifying with that depression and sadness of lost sibling because that lost sibling hasn&rsquo;t been acknowledged. That&rsquo;s why Kashmir and Palestine and Syria problem cannot be solved unless they acknowledge what&rsquo;s happen to their ancestors and pray for them because the ancestors are giving pain to progeny and it is an endless cycle</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Why do people fall in love, get married and fight and divorce?</strong></p>\r\n\r\n<p style=\"text-align:justify\">One of the major reasons for this is the ancestral defect. Ancestral defect comes from our family members who have not ascended to the highest dimension and we are caught up them through our DNA. For example, Children in the family may behave in an erratic manner become aggressive and angry because of the energetic cord of our ancestors whose desires are left unfulfilled. Our unsatisfied ancestors make us attract business &amp; LIFE partners who are going to betray us and create obstacles. It is affecting the entire world population and the catch is that they do not even know it. Mother sends the child to USA to study and the child then marries a girl and relocates there which deprives the mother of the affection of the child and daughter in law. The child feels happy and attracts a life partner who betrays him and many times have children with sickness</p>\r\n\r\n<p style=\"text-align:justify\"><strong>There are Rituals done for Pitru Dosh eradication. Does it help?</strong></p>\r\n\r\n<p style=\"text-align:justify\">Rituals are just formal processes and not emotional processes. So they might only be mechanically doing it. They do a funeral, but not really release the grief from the DNA AND THIS GRIEF STAYS WITH THE PERSON EATING HIS BODY FROM INSIDE. In cases of inter-caste marriages or family disputes, members are thrown out of family which is a wrong Karma. So, this upsets the order of the family and the parents curse their children and it affects them genetically bringing problems for the children as well as the parents and it stays as a genetic curse even after they die. Only a proper karmic healing can resolve the issue. Brahmin who does the puja ritual himself has ancestral defects. So, the spirit does not listen to that Brahmin as the spirit only listens to mindful and pious yogi.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Karma Guru ji, how can we tackle a child or an adult with attention disorder and extreme anger issues?</strong></p>\r\n\r\n<p style=\"text-align:justify\">In India, mental illness is a taboo. Every physical issue in your life is based on a mental program. For example, a child with attention disorder is aggressive, hyper, anxious or depressed. Even though 50% of the children in the world go through these phases, however, there is some extra drama in their life which makes them have attention disorder. These children exhibit this behavior to attract attention from the mother. They want the mother to acknowledge them. A fetus is the unconscious mind of the mother. The source of this attention seeking behavior comes from the time of conception of the child. Additionally, a miscarriage or an abortion before the conception adds to the fear of the mother for the child that the child will not make it and the energy of the aborted child looms over, Karma Healing facilitates by going to the time and before conception removing all the negative fear and energy which transforms behavior of the child. So, it is not the child who needs healing the most. It is the mother. All the mental illnesses are the result of the unhealed ancestral defect.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>How does my family Karma affect my life?</strong></p>\r\n\r\n<p>Your family karma is like a tree and you are a branch of that tree. A traumatic event in the history of a family can create generational waves impacting future off springs, Divorces, Migrations, Wars or acts of violence, Early deaths, Mothers who died giving birth, Abortions, Exclusions, Dis- eases and Frauds all affect our family karma. One may attract the partner based on the family Karma. Mother-in-law gets daughter-in-law with lot of love and affection to only fight with her later on. This is Karma.</p>\r\n\r\n<p><strong>How can we heal it?</strong></p>\r\n\r\n<p>This can only be healed through special techniques of ancestral defects. Now, most of the readers are married. They will not leave their partner although they might be having marital issues. The beauty of the technique is that the moment you heal the family tree karma, you will observe that the behaviour of your family members and even extended family will completely heal and will become cordial.</p>\r\n\r\n<p>An uncordial relationship of a parent with their father and mother or also between their parents will make the that person behave like father or mother unconsciously because that parent when they were a child were missing the father&#39;s attention and support and this parent starts to look for that support in their children and the child feels burden and ends up losing the childhood at a young age to act like a good father to his mother or father. This spoils the family dynamics and harms relationships in the family. So, it is the mother who has to release the defect. It can only be resolved by following 11 steps of Karma Yoga and releasing the defect in Samadhi state. This defect is continuing in the world for centuries. Karmayog healing is the Original Shiva Technique which people just knows as a mere term but the actual technique was kept a secret for deserving few .In this technique, we see the past as an objective observer and transform the memory of all the present and future Karma in Samadhi State to a joyous or a neutral karma.</p>\r\n', 'Mystery of pitru dosha in your life', 'Mystery of pitru dosha in your life', 'Mystery of pitru dosha in your life', 1, '2018-06-27 09:22:16', '157.38.200.169', 'Active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `contact_id` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_id`, `name`, `email`, `phone`, `message`, `entry_date`) VALUES
(1, 'Jitendra Purviya', 'jitendra.purviya19@gmail.com', '9685243467', 'ssasdsdasd', '2018-11-03 18:29:08'),
(2, 'test', 'kontakt@madgoats.no', '9898989898', 'safsfas', '2019-03-19 17:22:22'),
(3, 'test', 'kontakt@madgoats.no', 'test', 'adasdsa', '2019-03-19 18:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry_master`
--

CREATE TABLE `enquiry_master` (
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `trial_id` int(11) NOT NULL,
  `alt_mobile` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry_master`
--

INSERT INTO `enquiry_master` (`fname`, `lname`, `mobile`, `email`, `address`, `trial_id`, `alt_mobile`, `amount`, `status`, `entry_date`) VALUES
('jitendra', NULL, '9685243467', 'jitendra@sets.com', 'fsddsfs', 1, '7668-000786', '', '1', '2020-03-12 18:54:50'),
('finnegan walters', NULL, 'finnegan walters', 'finneganwalters.sc.1836636782@supersendme.org', 'finnegan walters', 2, 'finnegan walters', '', '', '2020-03-18 19:07:04'),
('jerry jackson', NULL, 'jerry jackson', 'jerryjackson.gm.1087555142@gcheck.xyz', 'jerry jackson', 3, 'jerry jackson', '', '', '2020-03-20 08:26:29');

-- --------------------------------------------------------

--
-- Table structure for table `get_newsletter`
--

CREATE TABLE `get_newsletter` (
  `newsltr_id` int(10) NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT 'website',
  `email` varchar(255) NOT NULL,
  `subscription` int(1) NOT NULL DEFAULT '1',
  `entry_by` int(10) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `my_numbers`
--

CREATE TABLE `my_numbers` (
  `id` int(11) NOT NULL,
  `number` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `sum` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `my_numbers`
--

INSERT INTO `my_numbers` (`id`, `number`, `amount`, `sum`) VALUES
(1, '7668-000786', '1200', '20=1'),
(2, '8303-000786', '1200', '15=1'),
(3, '91427-39786', '2000', '20=1'),
(4, '77860 80004', '1000', '12=1'),
(5, '786 786 6874', '2000', '20=1'),
(6, '9153274786', '2000', '20=1'),
(7, '786 786 6874', '2000', '20=1'),
(8, '9153274786', '2000', '20=1'),
(9, '80 70 60 0786', '2000', '20=1'),
(10, '786-786-3004', '2000', '20=1'),
(11, '99412-57000', '2000', '20=1'),
(12, '95516-79000', '2000', '20=1'),
(13, '91074-20000', '2000', '20=1'),
(14, '81007 00000', '2000', '20=1'),
(15, '727 000 69 00', '2000', '20=2'),
(16, ' 7999992253', '2000', '20=2'),
(17, '799999 2260', '2000', '20=2'),
(18, '9578 000042', '2000', '20=2'),
(19, ' 97507-00028', '2000', '20=2'),
(20, '97000-51222', '2000', '20=2'),
(21, '9865-000066', '2000', '20=2'),
(22, '767 0000 695', '2000', '20=2'),
(23, '721 0000980', '2000', '20=2'),
(24, '890 804 7777 ', '2000', '20=2'),
(25, '8909 72 3333', '2000', '20=2'),
(26, '727 000 69 00', '2000', '20=2'),
(27, '727 000 69 00', '2000', '20=2'),
(28, '727 000 69 00', '2000', '20=2'),
(29, '727 000 69 00', '2000', '20=2'),
(30, '727 000 69 00', '2000', '20=2'),
(31, '727 000 69 00', '2000', '20=2'),
(32, '727 000 69 00', '2000', '20=2'),
(33, '96146-54444', '2000', '20=1'),
(34, '97007-42222', '2000', '20=1');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `txn_id` varchar(255) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `payment_method` varchar(50) NOT NULL DEFAULT '0',
  `total_price` double NOT NULL,
  `coupon_discount` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL DEFAULT 'NULL',
  `product_delivery_status` varchar(100) DEFAULT 'New',
  `return_status` varchar(100) NOT NULL DEFAULT 'Null',
  `reason_id` int(50) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_date` datetime NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active',
  `reason_des` text NOT NULL,
  `return_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `o_details_id` int(20) NOT NULL,
  `order_id` int(11) NOT NULL,
  `audio_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_price` double NOT NULL,
  `prod_status` varchar(100) NOT NULL,
  `cancel_status` int(2) NOT NULL DEFAULT '0' COMMENT '0 = not cancel and 1 = cancel',
  `return_status` varchar(100) NOT NULL DEFAULT 'NULL',
  `reason_id` int(11) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `reason_des` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `page_type` varchar(255) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `page_description` text NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active',
  `ip_addr` varchar(100) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(100) NOT NULL,
  `entry_by` int(10) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `title`, `description`, `image`, `entry_date`, `ip_addr`, `entry_by`, `status`) VALUES
(8, 'ADVENTURE TOURS', '      ', 'cc58c384c0f39645c5a0a7104add0cca.jpg', '2018-10-08 14:29:21', '::1', 1, 'Active'),
(9, 'WANT THIS TOO?', '', 'cac73d8bf3ac0e50fadb7287ecf941ff.jpg', '2018-10-08 14:30:18', '::1', 1, 'Active'),
(10, 'BUY ON ENVATO ', '   ', '42fa34a9cad040e2ce57197e48cc0def.jpg', '2018-10-08 14:37:17', '::1', 1, 'Active'),
(11, 'GREAT ADVENTURES', '', 'ec52dd7d439aa56bd87b14752aaa61f8.jpg', '2018-10-08 14:31:04', '::1', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonials_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonials_id`, `name`, `image`, `description`, `location`, `entry_by`, `entry_date`, `ip_addr`, `status`) VALUES
(2, 'TAPASYA', 'c95150a8b5ac8a5a9fa418dcc85d67a0.jpg', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal making it look like readable English\r\n', 'Delhi', 1, '2018-10-10', '::1', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `oauth_provider` varchar(255) NOT NULL,
  `oauth_uid` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `entry_by` int(20) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `oauth_provider`, `oauth_uid`, `mobile`, `plain_password`, `entry_by`, `entry_date`, `ip_addr`, `status`) VALUES
(1, 'Vikram thakur', '94ecec962f3dba6a08484bed4b21bd47', 'vikky.raghuwanshi@gmail.com', '', '', '9871593665', '786810', 0, '2018-06-03 10:01:03', '::1', 'Active'),
(2, 'Jitendra thakur', 'e10adc3949ba59abbe56e057f20f883e', 'jeetsirts19@gmail.com', '', '', '8010533449', '123456', 0, '2017-11-08 19:20:10', '206.72.206.123', 'Active'),
(3, 'Aditya', '827ccb0eea8a706c4c34a16891f84e7b', 'adityatm28@gmail.com', '', '', '9999169816', '12345', 0, '2017-12-08 16:35:11', '47.31.13.178', 'Active'),
(4, 'vikram purviya', 'e10adc3949ba59abbe56e057f20f883e', 'vikky.raghuwanshi@gmail.comm', '', '', '9871593665', '123456', 0, '2018-04-19 17:18:33', '::1', 'Active'),
(5, 'vanti', 'e10adc3949ba59abbe56e057f20f883e', 'banti@admin.com', '', '', '8010533449', '123456', 0, '2018-05-06 14:07:15', '::1', 'Active'),
(6, 'Dr Shirish Singhal', '7689a9cc8291281833c9facf395b0fcf', 'ssinghaldr@gmail.com', '', '', '9826876745', 'Shirish10', 0, '2018-05-27 13:56:49', '171.61.30.81', 'Active'),
(7, 'Anjani', '98a15eb73bb41ee05d647bab5b0a9d5c', 'anjani.kumar@aksoft.in', '', '', '8285318531', '363320', 0, '2018-06-19 05:53:10', '47.31.195.188', 'Active'),
(8, 'Priyanka Yerandekar ', 'f007108461e9e1938e4391f2e7959f86', 'yerandekarpriyanka777@gmail.com', '', '', '9833566507', 'priyanka21', 0, '2018-05-31 15:15:49', '42.109.7.104', 'Active'),
(9, 'Marshall Patel', 'b3d97746dbb45e92dc083db205e1fd14', 'marshallpatel@gmail.com', '', '', '9879002525', 'phoenix', 0, '2018-05-31 17:37:43', '43.242.116.211', 'Active'),
(10, 'Bhanumathy Velendra ', '877f04a2a2ff0981823e65960302cac4', 'banuveli@gmail.com', '', '', '9940687872', 'kambankau', 0, '2018-06-02 03:53:22', '49.205.218.103', 'Active'),
(11, 'Ashna Malhotra ', '114576718507068675b5789faee9bf16', 'ashna@amlf.net', '', '', '9619044072', 'Ashna123', 0, '2018-06-02 05:32:28', '42.109.53.237', 'Active'),
(12, 'Muniraju Muniswamy ', 'bd6d3e9d343e6ecc8c0822b38e30a428', 'drmuniraju30@Yahoo.co.on', '', '', '9880139779', 'FORgot1973@', 0, '2018-06-03 03:01:48', '157.49.137.203', 'Active'),
(13, 'Vipul Kukreja', '81304f0c3562fa005671954b66ff845e', 'vipul.kukreja@gmail.com', '', '', '9820422311', 'janu0603', 0, '2018-06-05 09:22:18', '122.170.130.223', 'Active'),
(14, 'Ajay Sharma', 'b84fc688a8212df8e40117b5a9b3e890', 'ajayrai3666@gmail.com', '', '', '9811633743', '@Ashish1', 0, '2018-06-10 08:00:55', '139.5.253.228', 'Active'),
(15, 'Mugdha gupta', '9ada54d14452ffb91002ce872715cd7a', 'muggu15@gmail.com', '', '', '9999992252', 'piyush2005', 0, '2018-06-10 08:08:37', '42.111.3.27', 'Active'),
(16, 'meeti ', '0a284a52b944eb2b3c6e6d790adaf957', 'meetibatra@gmail.com', '', '', '9717391515', 'amiti1dd', 0, '2018-06-10 08:17:51', '47.31.88.55', 'Active'),
(17, 'Nidhi handa', '151a107aaf483f32b1a32afa56114c6c', 'nidhi.handa9@gmail.com', '', '', '9818044763', 'nhanda999', 0, '2018-06-10 08:38:19', '171.61.128.76', 'Active'),
(18, 'Ekta Chitkara ', '12d30a874f568dc49dc6e75e14c43d7c', 'ektac4@gmail.com', '', '', '9915552912', 'ssdn1088', 0, '2018-06-11 09:32:31', '47.31.159.48', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `enquiry_master`
--
ALTER TABLE `enquiry_master`
  ADD PRIMARY KEY (`trial_id`);

--
-- Indexes for table `get_newsletter`
--
ALTER TABLE `get_newsletter`
  ADD PRIMARY KEY (`newsltr_id`);

--
-- Indexes for table `my_numbers`
--
ALTER TABLE `my_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`o_details_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonials_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `user_id_3` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `contact_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `enquiry_master`
--
ALTER TABLE `enquiry_master`
  MODIFY `trial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `get_newsletter`
--
ALTER TABLE `get_newsletter`
  MODIFY `newsltr_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `my_numbers`
--
ALTER TABLE `my_numbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `o_details_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonials_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
