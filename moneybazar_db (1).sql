-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 17, 2019 at 09:59 AM
-- Server version: 5.6.43-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moneybazar_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `plain_pass` varchar(255) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `type` int(10) NOT NULL DEFAULT '1',
  `role` varchar(25) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_by` int(12) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'Active',
  `ip_addr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `fname`, `lname`, `plain_pass`, `phone`, `type`, `role`, `entry_date`, `entry_by`, `status`, `ip_addr`) VALUES
(1, 'admin', '95228f3702f78bbb4cbbd356a01a5e2b', 'anubhav@credencedigital.com', 'admin', '', 'ADMIN!@12', '', 1, '', '2015-09-16 08:57:04', 0, 'Active', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_slug` text NOT NULL,
  `name` varchar(128) NOT NULL,
  `image` varchar(100) NOT NULL,
  `company` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_addr` varchar(128) NOT NULL,
  `status` varchar(128) NOT NULL DEFAULT 'Active',
  `popularity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `title`, `title_slug`, `name`, `image`, `company`, `description`, `meta_title`, `meta_keyword`, `meta_description`, `entry_by`, `entry_date`, `ip_addr`, `status`, `popularity`) VALUES
(4, 'MYSTERY OF PITRU DOSHA IN YOUR LIFEddfdfd', 'mystery-of-pitru-dosha-in-your-lifeddfdfd', 'Sumant And Sumeet Kaul', 'b3c5beaf92fd11ddb9f41f975755d653.png', 'Sumant And Sumeet Kaul', '<p style=\"text-align:justify\"><strong>The most common Karmic trouble seen in almost every house is between daughter-in-law and mother-in-law. Karma Guru ji, can the technique resolve this Karma?</strong></p>\r\n\r\n<p style=\"text-align:justify\">Many people come with the karma of daughter-in-law and mother in law; it&rsquo;s a prevalent karma all over the world. However, people don&rsquo;t realize that it&rsquo;s not an individual karma but it&rsquo;s a family karma. The family travels together with a family karma. Members of the family do not consciously take this karma but the family consciousness decides along with unconscious of the individual to delegate the karma. So whenever there is a mother-in-law daughter-in-law dispute and you can see it in the ancestors also in that family. It means at the great-grandfather level, the mother-in-law died early or had some trauma and couldn&rsquo;t give love to her daughter-in-law and this way the soul has been forgotten. In the family, now, there is a dosha &nbsp;I.e defect regarding not receiving love between mother in law and daughter in law. The new daughter in laws who come in that family unconsciously will take that family karma and out of bonding, sympathy, and love will take the burden of their mother in law&rsquo;s - mother In law to represent them in family because they didn&rsquo;t get love with their daughter in law and to settle that karma the new daughter in law has karma with her mother in law. This script continuously runs until someone in the family consciousness breaks it.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>What can be the underlying cause of migraines and depression?</strong></p>\r\n\r\n<p style=\"text-align:justify\">One lady aborted a child and her next child was having migraines. Her mother started expecting a lot from her because she lost her earlier child and that suppressed grief is stored inside the mother and the present child is identifying with that depression and sadness of lost sibling because that lost sibling hasn&rsquo;t been acknowledged. That&rsquo;s why Kashmir and Palestine and Syria problem cannot be solved unless they acknowledge what&rsquo;s happen to their ancestors and pray for them because the ancestors are giving pain to progeny and it is an endless cycle</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Why do people fall in love, get married and fight and divorce?</strong></p>\r\n\r\n<p style=\"text-align:justify\">One of the major reasons for this is the ancestral defect. Ancestral defect comes from our family members who have not ascended to the highest dimension and we are caught up them through our DNA. For example, Children in the family may behave in an erratic manner become aggressive and angry because of the energetic cord of our ancestors whose desires are left unfulfilled. Our unsatisfied ancestors make us attract business &amp; LIFE partners who are going to betray us and create obstacles. It is affecting the entire world population and the catch is that they do not even know it. Mother sends the child to USA to study and the child then marries a girl and relocates there which deprives the mother of the affection of the child and daughter in law. The child feels happy and attracts a life partner who betrays him and many times have children with sickness</p>\r\n\r\n<p style=\"text-align:justify\"><strong>There are Rituals done for Pitru Dosh eradication. Does it help?</strong></p>\r\n\r\n<p style=\"text-align:justify\">Rituals are just formal processes and not emotional processes. So they might only be mechanically doing it. They do a funeral, but not really release the grief from the DNA AND THIS GRIEF STAYS WITH THE PERSON EATING HIS BODY FROM INSIDE. In cases of inter-caste marriages or family disputes, members are thrown out of family which is a wrong Karma. So, this upsets the order of the family and the parents curse their children and it affects them genetically bringing problems for the children as well as the parents and it stays as a genetic curse even after they die. Only a proper karmic healing can resolve the issue. Brahmin who does the puja ritual himself has ancestral defects. So, the spirit does not listen to that Brahmin as the spirit only listens to mindful and pious yogi.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Karma Guru ji, how can we tackle a child or an adult with attention disorder and extreme anger issues?</strong></p>\r\n\r\n<p style=\"text-align:justify\">In India, mental illness is a taboo. Every physical issue in your life is based on a mental program. For example, a child with attention disorder is aggressive, hyper, anxious or depressed. Even though 50% of the children in the world go through these phases, however, there is some extra drama in their life which makes them have attention disorder. These children exhibit this behavior to attract attention from the mother. They want the mother to acknowledge them. A fetus is the unconscious mind of the mother. The source of this attention seeking behavior comes from the time of conception of the child. Additionally, a miscarriage or an abortion before the conception adds to the fear of the mother for the child that the child will not make it and the energy of the aborted child looms over, Karma Healing facilitates by going to the time and before conception removing all the negative fear and energy which transforms behavior of the child. So, it is not the child who needs healing the most. It is the mother. All the mental illnesses are the result of the unhealed ancestral defect.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>How does my family Karma affect my life?</strong></p>\r\n\r\n<p>Your family karma is like a tree and you are a branch of that tree. A traumatic event in the history of a family can create generational waves impacting future off springs, Divorces, Migrations, Wars or acts of violence, Early deaths, Mothers who died giving birth, Abortions, Exclusions, Dis- eases and Frauds all affect our family karma. One may attract the partner based on the family Karma. Mother-in-law gets daughter-in-law with lot of love and affection to only fight with her later on. This is Karma.</p>\r\n\r\n<p><strong>How can we heal it?</strong></p>\r\n\r\n<p>This can only be healed through special techniques of ancestral defects. Now, most of the readers are married. They will not leave their partner although they might be having marital issues. The beauty of the technique is that the moment you heal the family tree karma, you will observe that the behaviour of your family members and even extended family will completely heal and will become cordial.</p>\r\n\r\n<p>An uncordial relationship of a parent with their father and mother or also between their parents will make the that person behave like father or mother unconsciously because that parent when they were a child were missing the father&#39;s attention and support and this parent starts to look for that support in their children and the child feels burden and ends up losing the childhood at a young age to act like a good father to his mother or father. This spoils the family dynamics and harms relationships in the family. So, it is the mother who has to release the defect. It can only be resolved by following 11 steps of Karma Yoga and releasing the defect in Samadhi state. This defect is continuing in the world for centuries. Karmayog healing is the Original Shiva Technique which people just knows as a mere term but the actual technique was kept a secret for deserving few .In this technique, we see the past as an objective observer and transform the memory of all the present and future Karma in Samadhi State to a joyous or a neutral karma.</p>\r\n', 'Mystery of pitru dosha in your life', 'Mystery of pitru dosha in your life', 'Mystery of pitru dosha in your life', 1, '2018-06-27 09:22:16', '157.38.200.169', 'Active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `contact_id` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_id`, `name`, `email`, `phone`, `message`, `entry_date`) VALUES
(1, 'Jitendra Purviya', 'jitendra.purviya19@gmail.com', '9685243467', 'ssasdsdasd', '2018-11-03 18:29:08'),
(2, 'test', 'kontakt@madgoats.no', '9898989898', 'safsfas', '2019-03-19 17:22:22'),
(3, 'test', 'kontakt@madgoats.no', 'test', 'adasdsa', '2019-03-19 18:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry_master`
--

CREATE TABLE `enquiry_master` (
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `loan_type` varchar(100) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `alt_mobile` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry_master`
--

INSERT INTO `enquiry_master` (`fname`, `lname`, `mobile`, `email`, `address`, `loan_type`, `trial_id`, `alt_mobile`, `amount`, `status`, `entry_date`) VALUES
('Jitendrta', 'thakur', '9685243467', 'jitendra.purviya19@gmail.com', '201', 'Personal Loan', 2, '9685243467', '1200', '1', '2019-06-29 18:48:55'),
('TEST', '1', '9090909090', 'test@email.com', 'TEST', 'Auto/Vehicle Loan', 3, '9090909090', '9090909090', '', '2019-06-29 12:14:44'),
('Raj', 'Kumar', '6289986702', 'kumarrahul009009@gmail.com', 'Ranchi Jharkhand ', 'Personal Loan', 4, '7091335768', '600000', '1', '2019-06-30 01:18:37'),
('Meena ', 'Zode ', '8806068211', 'Poojamvp22@gmail.com', 'Joshi wadi, Bhatkuli rod', 'Personal Loan', 5, '8830394952', '100000', '', '2019-06-30 03:46:51'),
('Amit.', 'Singh', '9415612000', 'Aasusingh123456@gmail.com', 'Sharon. Subansh', 'Personal Loan', 6, '9415612000', '2000000', '1', '2019-07-04 06:19:31'),
('Muskan begum', 'Nadaf', '7349395090', 'addhusk@gmail.com', 'Abbubakar, Majhid, Shahabad', 'Personal Loan', 7, '9607165551', '200000', '', '2019-06-30 07:15:37'),
('Sonia', 'Sharma', '9873121346', 'Soniyasharma199106@gmail.com', '127A,gali no4,east azad nagar', 'Personal Loan', 8, '7042656796', '280000', '', '2019-07-01 07:23:43'),
('Roopa ', 'Devi ', '9026678778', 'Sohamagr122014@gmail.com', 'Jonpur', 'Personal Loan', 9, '9026678778', '600000', '1', '2019-07-03 06:06:43'),
('Roopa ', 'Devi ', '7309990977', 'samgupta4418@gmail.com', 'Varanasi ', 'Personal Loan', 10, '7309990977', '600000', '', '2019-07-01 07:43:38'),
('Aman', 'Jain', '9806169218', 'amanjain1409@gmail.com', 'M-21 bda complex opp Ci Home Mata Mandir', 'Auto/Vehicle Loan', 11, '9806169218', 'asdv', '', '2019-07-01 09:11:00'),
('Ajay', 'Kumar', '9117135446', 'shrifinance690qq@gmail.com', 'xyz', 'Personal Loan', 12, '9117135446', 'fvc', '', '2019-07-01 09:35:34'),
('testss', 'wdvghvjsa', '987897897987', 'email@email.com', 'kjbsachjsdcs', 'Auto/Vehicle Loan', 13, '89798896834', '7867656778', '', '2019-07-01 13:02:33'),
('yadingi', 'rajesh', '960328022', 'sunni7334@gmail.com', 'shamshabad kapugada hyderabad', 'Personal Loan', 14, '9553132153', '50000', '', '2019-07-02 04:59:10'),
('Archana', 'Karmakar', '7787884339', 'archanakarmakar2015@gmail.com', 'Harakrishan Villa,Duplex No-41, Botanda, Sundarpada', 'Personal Loan', 15, '9777877554', '40000', '1', '2019-07-03 06:07:07'),
('Anjali', 'Rathod', '9664822637', 'rathodanju21795@gmail.com', '203 radha swami apt b/h gujrat gas company', 'Personal Loan', 16, '9664822637', '500000', '', '2019-07-02 04:59:42'),
('Boda', 'Santoshkumar', '9963581332', 'Srianjaneyaherbel@gmail.com', 'Chandru thanda khammam', 'Personal Loan', 17, '9346479296', '10000', '', '2019-07-02 05:01:07'),
('Jangrey ', 'Ch sangma ', '8575563032', 'jangsbchisim123@gmail.com', 'Meghalaya dalu barengapara ', 'Home Loan', 18, '8787479382', '70000', '', '2019-07-02 05:02:14'),
('Rajani kanta ', 'Sahoo', '9658146767', 'Rajanikanta96@gmail.com', 'At/po-Dahisada,via-soro,Dist-Balesore,odisha', 'Personal Loan', 19, 'Hii', '150000', '', '2019-07-02 05:10:26'),
('Lakshmi', 'KRISHNAN', '9442236364', 'ram9442236364@gmail.com', 'R.k Nagar', 'Personal Loan', 20, '7448494340', '9442236364', '', '2019-07-02 16:40:12'),
('Gaurav', 'Kohale', '8605161134', 'gauravkohale26@gmail.com', 'At po ladgoan ta katol dist nagpur', 'Personal Loan', 21, '8605161134', '150000', '', '2019-07-02 16:41:08'),
('Harpreet', 'Virk', '8699237374', 'Virk.Harpreet123@gmail.com', '121/1 B Block Acme Heights Appartment', 'Personal Loan', 22, '9592309350', '200000', '', '2019-07-02 16:41:18'),
('Hemant ', 'Kusare', '8849391654', 'hemantkusare66@gmail.com', 'Asha park bahadur vila morbi, Curkit house, Shamakhayadi road morbi', 'Personal Loan', 23, '7874950292', '300000', '1', '2019-07-04 06:22:36'),
('Badrul', 'Maali', '9163179070', 'badrulmaali@gmail.com', '46g/1,Gora Chand Road.kolkata', 'Personal Loan', 24, '6291864776', '50000', '', '2019-07-02 16:42:01'),
('Mahebub abdul bhai shaikh', 'Abdul bhai', '9898858721', 'shaikhmehboobabdulbhai@gmail.co', 'B10tufel park vatvA ahmedabad', 'Personal Loan', 25, '7990069040', '500000', '', '2019-07-02 16:42:31'),
('Bhupat', 'Katosaniya', '9998153519', 'bhupatbhaikatosaniya@gmail.com', 'Chotila Moti Moti moladi ', 'Personal Loan', 26, '9327205292', '50000', '', '2019-07-02 16:43:19'),
('Munniram ', 'Kaswa', '9461967527', 'Kaswamunniram9461@gmail.com ', 'Bikaner ', 'Personal Loan', 27, '8320460788', '50000', '1', '2019-07-04 06:21:20'),
('Ananthajothi ', 'K', '9994966860', 'jo.viji1013@gmail.com ', '2/353,Muniyanvalasai. Sathankulam (po),Ramanathapuram ', 'Personal Loan', 28, '9944843265', '200000', '', '2019-07-02 16:44:36'),
('dipesh ', 'verma', '9713554964', 'dipuvarma3333@gmail.com', 'dewas', 'Personal Loan', 29, '9755199775', '150000', '', '2019-07-02 16:45:06'),
('VINAYAK ', 'Vispute ', '9723904239', 'vvispute28@gmail.com', 'DELADVA gam dindoli udhana Surat ', 'Personal Loan', 30, '7284001405', '300000', '', '2019-07-02 16:45:13'),
('Sukla Rani ', 'Debbarma', '9612031466', 'debbarmasanjit53@gmail.com', 'Rambhadra', 'Personal Loan', 31, '9612031466', '300000', '1', '2019-07-04 06:20:09'),
('Sudeep', 'Chaurasia', '8878164861', 'chaurasiasudeep@gmail.com', '266a, Shirdipuram, Kolar road', 'Personal Loan', 32, '8770839410', '150000', '', '2019-07-02 16:45:40'),
('Vishal', 'Kadam', '8975282254', 'leovishalkadam@gmail.com ', 'Manav park rameshwadi badlapure w', 'Personal Loan', 33, '6253152931', '50000', '', '2019-07-02 16:47:56'),
('RANJANA', 'KAMBLE', '7387899614', '7387899614abhikamble@gmail.com', 'Halkarni chandgad', 'Personal Loan', 34, '7057360895', '50000', '1', '2019-07-04 06:20:48'),
('Dipankar', 'Dey', '9992111207', 'chefdipankardey@gmail.com', 'S-27, firstfloor( front side) Parampuri, Gali no -6, Uttam Nagar . New Delhi -59', 'Personal Loan', 35, '9312148582', '300000', '', '2019-07-02 16:50:25'),
('Abhay         kumar', 'Singh', '9499377939', 'Abhaykumarsingh767473@gmail.com', 'Netajinager,galino_2,linear,bahadurgarh,Disst-jajhar,haryana.pin-124507', 'Home Loan', 36, '8168923908', '2000000', '', '2019-07-02 16:51:21'),
('mahendra ', 'pradhan', '8908259245', 'mahendrapradhan277@gmail.com', 'bhapur khairagadia ,nayagarh odisha-752063', 'Personal Loan', 37, '8480090216', '120000', '1', '2019-07-06 06:46:24'),
('Mehul', 'Ghume', '8879706415', 'vampirecry88@gmail.com', 'A-601, Sunita Anand Palace CHS, Mulund West', 'Personal Loan', 38, '7977918676', '5000', '', '2019-07-02 18:33:58'),
('Adduri vineeth', 'Adduri vineeth', '9493248396', 'Addurivineeth337@gmail.com', 'Chinnaboinapally', 'Personal Loan', 39, '9493248496', '100000', '', '2019-07-02 18:34:37'),
('PRASIT', 'Gurung', '9564521676', 'gprasit6@email.com', 'Sichey', 'Personal Loan', 40, '7797186304', 'Financial problem', '', '2019-07-02 18:35:30'),
('Sibasankar', 'Patro', '9861265777', 'Sibasankrpatra200@gmail.com', 'Nistipur,Ganjam, odisha 761054', 'Education Loan', 41, '9861265777', '50000', '', '2019-07-02 18:39:25'),
('suraj', 'dhanoriya', '9923852851', 'surajdhanwar775@gmail.com', 'near new post office opposite brk no 405 suraj aquarium', 'Personal Loan', 42, '8888529469', '80000', '1', '2019-07-05 06:08:15'),
('Sahadev', 'Gaonkar', '9421949848', 'sahadevgaonkar1@gmail.com', 'Gaonkarwadi, Dariste', 'Personal Loan', 43, '8973897515', '150000', '', '2019-07-02 18:48:37'),
('Mohd ', 'Zaid', '8707770825', 'Princepalak112@gmail.com', '92 ag rajeev nagar f block', 'Personal Loan', 44, '8707770825', '20000', '', '2019-07-02 18:51:41'),
('Prithviraj ', 'Patil ', '9535360522', 'prithvip1996@gmail.com ', '1st gate shukarwar peth tilakwadi Block no s8 ', 'Personal Loan', 45, '7022950206', '200000', '1', '2019-07-05 06:07:38'),
('Rakesh ', 'Dubey', '9503394766', 'rdubey767@gmail.com', '112 jivdani darshan Radhanagar Laxmipark tulinj road Nalasopara East 401209', 'Personal Loan', 46, '7620360619', '10000', '', '2019-07-02 18:57:02'),
('Sudam', 'Gouda', '7752080571', 'Sudamgouda143@gmail.com', 'Abhayapur po.Manikyapur ps.badagada pin 761107', 'Personal Loan', 47, '8249874107', '200000', '1', '2019-07-05 06:07:13'),
('subhashchand ', 'YADAV', '9850951118', 'bfcajay@gmail.com', 'saraswati corner flt no 401 near chinchwad station pune 411019', 'Personal Loan', 48, '8600848881', '80000', '1', '2019-07-06 05:34:40'),
('sandeep', 's', '8095380481', 'sandeepsirnayak@gmail.com', '#128 1st B CROSS, Venkateshware Nilaya', 'Personal Loan', 49, '8095380481', '8095380481', '', '2019-07-02 19:16:21'),
('Jaysukh', 'BhAlu', '9824899197', 'Jayeshbhalu07@gemail.com', 'Jesarroad, Sardarbhavanpase, Bappasitaramsocayty', 'Personal Loan', 50, '99737645350', '300000', '', '2019-07-02 19:17:48'),
('Sahil', 'Malhotra', '7404422252', 'Malhotras535@gimal.com', 'Nj363mitha bzar jalandhar', 'Personal Loan', 51, '9588787775', '100000', '1', '2019-07-05 06:07:24'),
('Afreed', 'Shaikh', '7045546454', 'afreed554@gmail.com', 'Room no.14/Sabir brother chawl, opp.madina dairy, jarimari, Kurla Andheri road, Mumbai-400072', 'Personal Loan', 52, '7045546455', '100000', '', '2019-07-02 19:27:58'),
('Solanki', 'Kanjibhai', '7016988464', 'solankinarshingji@gmail.com', '59 Aazadnagar Kalol, Mansa Rod, ', 'Personal Loan', 53, '7016988464', '50000', '', '2019-07-02 19:31:40'),
('Pallavi', 'Kaushik', '9661410066', 'Pallavikaushik3097@gmail.com', 'Shakespeare sarani', 'Personal Loan', 54, '7003453788', '20000', '', '2019-07-02 19:32:17'),
('Pothunuri ', 'Durga Rao ', '9505844211', 'raju82778@gmail.com', 'Kumari diagnostic, chepalachettu Centre', 'Personal Loan', 55, '9966571999', '200000', '', '2019-07-02 19:36:41'),
('Zonunsanga', 'Zonunsanga', '9862316957', 'Zonunazadeng@gmail.com', 'Aizawl, Mizoram', 'Home Loan', 56, '9366176241', '500000', '', '2019-07-02 19:38:30'),
('Mohammed tabrez ', 'Ahmad ', '9945217682', 'Suraiyatabrez662@gmail.com', '369 Mariyamma temple street Mariyamma temple Saripaliya Bangalore north ', 'Personal Loan', 57, '7019150759', '200000', '1', '2019-07-05 06:05:31'),
('Rahul', 'Ghosh', '9002487468', 'rahulbapl2014@gmail.com', 'Vill Tutranga Po Thakurchak Ps Belda, Dist Paschim Medinipur Pin no 721424', 'Personal Loan', 58, '9002487468', '700000', '', '2019-07-02 19:44:39'),
('Garima', 'Dhawan', '7827537992', 'dhawanginny@gmail.com', 'WZ-58 Dayal Sari Road Uttam Nagar New Delhi', 'Personal Loan', 59, '9958795315', '500000', '', '2019-07-02 19:47:52'),
('bharat', 'gellipogu', '8125050705', 'remallim516@gmail.com', 'ambedkar colony, rentachintala, guntur district', 'Education Loan', 60, '8367429591', '30000', '', '2019-07-02 20:07:38'),
('Anuj', 'goyal', '8077486550', 'anujgl1973@gmail.com', '133 dal mandi mahaveeraganj aligarh', 'Personal Loan', 61, '8077486550', '5000', '', '2019-07-02 20:10:12'),
('Aditya', 'chandel', '8628975425', 'adityachandel93@gmail.com', '#13/11, Rana House, Mandi, H.P. ', 'Personal Loan', 62, '7018473549', '100000', '', '2019-07-02 21:03:06'),
('Jefferson ', 'Thangadurai ', '9566007866', 'tttjefferson@gmail.com', 'Chennai ', 'Personal Loan', 63, '8939818031', '75000', '', '2019-07-02 21:25:12'),
('Gautam', 'Singh', '9682045000', 'gautamsingh.singh@yahoo.com', 'Lucknow', 'Property Loan', 64, '8429741010', '500000', '', '2019-07-02 21:25:49'),
('Pratik', 'Patil', '7208328818', 'patilpratik1728@gmail.com', 'Ram Dev Park,miraroad(east), A/002,ram Kutir Bldg', 'Personal Loan', 65, '7021634676', '200000', '', '2019-07-02 21:27:12'),
('Sunil ', 'Ahirwar ', '9522062966', 'Sunilsuryavanshi851@gmail ', 'Gram Rampura jageer distric vidisha MP. ', 'Auto/Vehicle Loan', 66, '6261797716', '100000', '', '2019-07-02 22:30:24'),
('Aniruddha', 'Saha', '9774814598', 'Aniruddhasaha858@gmail.com', 'Udaipur tripura', 'Personal Loan', 67, '8794043189', '10000', '', '2019-07-02 22:53:07'),
('Raju', 'Sinha ', '9313781572', 'rajusinha715@gmail.com', 'Village  /po tinokhal, Patherkandi', 'Personal Loan', 68, '9313781572', '200000', '', '2019-07-02 23:44:15'),
('Rahul', 'Tyagi', '9873322581', 'Tyagirahul965@gmail.com', '169/7 hans enclave', 'Personal Loan', 69, '701118866', '200000', '', '2019-07-03 01:12:42'),
('Anbalagan', 'Anbalagan', '9025095789', 'anbuvijia@gmail.com', 'theni 625579', 'Personal Loan', 70, '8489494921', '300000', '', '2019-07-03 02:20:40'),
('Ashwani Kumar ', 'Gupta ', '7706006092', 'ashwani444@c.como', '86 sabri mirzapur ', 'Personal Loan', 71, '6392501634', '200000', '1', '2019-07-05 06:07:50'),
('SHIV KUMAR ', 'Mishra ', '7678699848', 'ak1640090@gmail.com', 'KUTHOUNDA, KUTHOUNDA', 'Home Loan', 72, '7678699848', '250000', '1', '2019-07-05 06:06:59'),
('Taheer', 'Shaikh', '9594645212', 'taheernazeer89@gmail.com', 'Room n.1 madina chawl ambedkar chowk titwala ambivali road baneli gaon titwala east kalyan Thane district', 'Personal Loan', 73, '9867248997', '500000', '', '2019-07-03 19:09:58'),
('Abdul shameen', 'Shameen', '9945996876', 'Abdulshameen1@gmail.com ', 'Ashryanager vamnjoor manglore', 'Personal Loan', 74, '9945996876', '2500000', '', '2019-07-03 19:10:40'),
('Ajay  kumar ', 'Pandey ', '6289155057', 'pandeyajaykumar315@gmail.com ', '95/14 Dharmtala Road Ghuuri Howrah ', 'Personal Loan', 75, '7047585237', '50000', '', '2019-07-03 19:27:55'),
('Deepak', 'Besoya', '7838888488', 'addiction.uis@gmail.com', '143,kailash hills,east of kailash,top floor', 'Personal Loan', 76, '9810526005', '100000', '', '2019-07-03 19:42:04'),
('Prem', 'Shirke', '8928279345', 'pshirke20may@gmail.com', '302, sadguru apt, tulinj road, near Saraswati school, nallasopara east', 'Personal Loan', 77, '9167625019', '30000', '', '2019-07-03 19:51:00'),
('Kavitha', 'Ds', '7619636101', 'kavithaamrutha2014@gmail.com', 'Gi Nilaya Chowderderweri Temple Street Madhugiri Madhugiri Tumukur Karnataka', 'Personal Loan', 78, '9902464797', '300000', '', '2019-07-03 19:51:31'),
('Nasrin', 'Ansari', '9619733460', 'nsrramsha786@gmail.com', '20nasheman apartment gr floor Bhiwandi', 'Auto/Vehicle Loan', 79, '9923497947', '8 lakh', '', '2019-07-03 20:07:36'),
('Deepak', 'Mishra', '6394475300', 'deepak69124@gmail.com', 'Tarapur katghara jaunpur', 'Personal Loan', 80, '8960350874', '500000', '', '2019-07-03 20:10:58'),
('Dinesh', 'verma', '8308788070', 'varmasarika437@gmail.com', 'Sharda bhavan, Vrundavan colony,Kandali.Paratwada Dust -Amravati 444805', 'Personal Loan', 81, '8308788070', '50000', '', '2019-07-03 20:30:10'),
('harikrishnareddy', 'gali', '7288914153', 'galiharikrishnareddy28@gmail.com', 'rc puram', 'Personal Loan', 82, '7288914153', '10000', '', '2019-07-03 21:00:24'),
('Ravi Kant', 'Garg', '9936626500', 'ravigarg0000@gmail.com', 'CHOTI BAZAR, KALWAN GANJ JHANDA CHAURAHA BANDA 210001 UTTAR PRADESH', 'Personal Loan', 83, '9140798480', '400000', '', '2019-07-03 21:30:26'),
('Nongthang ', 'Laishram ', '9366426915 ', 'nongthanglaishram5@gmail.com', 'Imphal (Manipur) ', 'Personal Loan', 84, '9366426915 ', '3000', '', '2019-07-03 21:40:49'),
('Anil', 'Singh', '8295766453', 'anilsardarasingh@gmail.com', '#44 shivalik colony village singhawla ambala city', 'Personal Loan', 85, '7889281516', '200000', '', '2019-07-03 21:41:35'),
('S', 'Rajamani', '9025367417', 'raja.mani27@yahoo.com', '1d/4, Maninagar 1st street tuticorin ', 'Personal Loan', 86, '+917904546887', '200000', '', '2019-07-03 22:53:10'),
('PAHILAJ', 'GODHWANI', '9321110111', 'Pahilajgodhwani5000@gmail.com', 'Heera Panna Apartment Flat No.612 B wing', 'Education Loan', 87, '9834461516', '750000', '', '2019-07-03 23:30:23'),
('Najma ', 'Bee', '9516226034', 'Khanchand42429@gmail.com', 'Ganjbasoda', 'Personal Loan', 88, '9516226034', '5000', '', '2019-07-04 02:03:44'),
('Amar', 'Sanga', '7709991429', 'amar.sanga@gmail.com', 'FLAT-408 BAHULE TOWNSHIP, NEAR SAMADHAN COLONY LANE-4, GANGA NAGAR FURSUNGI', 'Personal Loan', 89, '7709991429', '300000', '', '2019-07-04 02:42:29'),
('Rasbihari ', 'Mandal', '9081055192', 'rasb456@gmail.com', '185 Balugram uttarlakshmipur Kaliachak ', 'Personal Loan', 90, '8160973101', '300000', '1', '2019-07-06 05:36:27'),
('Lucky', 'Borang', '8258006700', 'Luckyborang@gmail.com', 'ITANAGAR', 'Personal Loan', 91, '8259891234', '200000', '', '2019-07-04 03:04:31'),
('Mahendra', 'singh', '7357368571', 'msbhati8242@gmail.com', 'Aka bhatiyan mokheri', 'Personal Loan', 92, '7357368571', '100000', '', '2019-07-04 03:04:41'),
('Golden', 'Singh', ' 941544327', 'singhgoldan913@gmail.com', 'Ramuapur bijhouli (khiri)', 'Personal Loan', 93, '7909059392', '500000', '', '2019-07-04 03:16:11'),
('Akshay ', 'Kumar', '7906236142 ', 'Rp872659@gmail.com ', '568 luxman puri meerut', 'Education Loan', 94, '7535959230', '500000', '', '2019-07-04 03:34:55'),
('MR.SHANKAR', 'MANDAL', '9547656725', 'roysuman728@gmail.com', 'RANDA,KESHPUR,PASCHIM MEDINIPUR,PIN 721150', 'Personal Loan', 95, '9547656725', '5,00000', '', '2019-07-04 03:59:09'),
('Kolla padma ', 'Padma kolla ', '9100875103', 'Jashwithkolla@gmail.com ', '23-1-753A pathway kane payta opp Anthony church spar nellore', 'Personal Loan', 96, '7032769008', '100000', '1', '2019-07-06 05:35:52'),
('Rahul ', 'Kumar', '8355071860', 'rk036700.gmail com', 'Padao chandauli', 'Home Loan', 97, '8765800287', '200000', '', '2019-07-04 04:18:00'),
('Anil Kumar', 'Sahu', '9589116139', 'as7721670@gmail.com', 'Khaira khurd po.', 'Personal Loan', 98, '9589116139', '250000', '', '2019-07-04 04:36:55'),
('Babrul', 'Chowdhury', '7002104709', 'babrul.universe@gmail.com', 'Pachim moudonga, Barhawar,Hojai,Assam', 'Personal Loan', 99, '9678657865', '1000000', '1', '2019-07-09 04:56:09'),
('Bhanudas Namdev kovhale ', 'Kovhale ', '9834495144', 'Bhanudaskovhale@gmail.com', 'Bhanudas Namdev kovhale,Taluka-Ausa. District =Latur,Maharashtra state,pin-413520 ', 'Property Loan', 100, 'Bhanudas Namdev kovhale, Taluka-Ausa,District-Latur,Atpost-Haregaon,state =Maharashtra,pin-413520 ', '120000', '1', '2019-07-06 05:36:05'),
('Mukesh', 'Kumar', '8894388988', 'mukeshkumar87212@gmail.com', 'Vpo Raipur sahoran teh and distt una', 'Personal Loan', 101, '7018134779', '50000', '', '2019-07-04 05:14:13'),
('Surbhi', 'Joshi', '9131868454', 'Surbhidwd86@gmail.com ', '68,mig mukherjee nagar dewas m.p', 'Personal Loan', 102, '9131868454', '300000', '', '2019-07-04 05:32:13'),
('Raghuveersingh', 'Raghuveer singh ', '8859318841', 'raghuveersinghdugna@gmail.com', 'Bill, dugnadistfirozabad', 'Personal Loan', 103, '9719967057', '700000', '', '2019-07-04 05:40:40'),
('Doggala ', 'Aravind', '8143563337', 'Doggalaarvind', 'Gudivada ', 'Personal Loan', 104, '9676484988', '20000', '', '2019-07-04 05:49:49'),
('Saniwel', 'Ronghi', '9901377605', 'saniwelronghi@gmail.com', 'MALIDAHAR MIKIR GRAND, JALALPUR', 'Personal Loan', 105, '8415919737', '100000', '', '2019-07-04 06:28:42'),
('Manish ', 'Singla ', '9958883222', 'msingla915@gmail.com', '3355/8 jai mata market tri nagar new Delhi 35', 'Personal Loan', 106, '9958883222', '100000', '', '2019-07-04 06:50:15'),
('Haresh', 'Pawar ', '7798625672', 'pawarharesh493@gmail.com', 'Room.304 gangiri cop housing society nalasopara esat', 'Personal Loan', 107, '7798625672', '12000', '', '2019-07-04 07:35:49'),
('AJAY', 'BARAIK', '7325095234', 'baraikajay100@gmail.com', 'VILL-BANADAG,GETALSUD,ANGARA,RANCHI', 'Personal Loan', 108, '8297141227', '50000', '', '2019-07-04 07:37:04'),
('Tejveer', 'Singh', '9991630094', 'tejveersingh0094 @gmail.com ', 'Gharam Patti pankhiya mohalla hodal distic palwal pin 121106 state haryana', 'Education Loan', 109, '9050456179', '100000', '', '2019-07-04 08:22:00'),
('Bheraram ', 'Ojha', '9799272288', 'Shiventsupplier@gmail.com', 'Ward no.04 near government school jaitpur ', 'Personal Loan', 110, '7615072440', '50000', '', '2019-07-04 08:36:23'),
('Hima ', 'Babu', '9526144375', 'www.hima20@gmail.com', 'Kurumbeparambil house,po kodannur', 'Personal Loan', 111, '9526144375', '50000', '', '2019-07-04 09:09:37'),
('Husain', 'Khan', '9220825887', 'husainkhan.saw@gmail.com', 'Maharashtra thane city kalyan', 'Personal Loan', 112, '8097150370', '100000', '', '2019-07-04 10:18:46'),
('Vishnu', 'M J', '9847584377', 'vishnu4377@gmail.com', 'Sreesankaram santhumoola malayinkeezhu trivandrum', 'Personal Loan', 113, '9747659170', '500000', '', '2019-07-04 10:24:01'),
('APURBA', 'DEKA', '6000315914', 'apurbadeka8484@gmail.com', 'vill bala post office bala dist nalbari state assam', 'Home Loan', 114, '8486021857', '500000', '', '2019-07-04 10:50:40'),
('Sabirhusen', 'Uka', '9638879799', 'ukasabir@gmail.com', '4/2375 salabatpura momnawad', 'Personal Loan', 115, '9924103690', '25000', '1', '2019-07-08 05:25:12'),
('Kamala Kanta ', 'Dash', '8851299479', 'kamaldadh095@gmail.com', 'At/po- mahu , Kendrapara,odisha', 'Personal Loan', 116, '9711246645', '150000', '', '2019-07-04 11:46:03'),
('Deepa', 'K', '9645815161', 'www.divyavijayan23@gmail.com', 'Vijaya bhavan company vila veedu company mukku vellanad p. O', 'Personal Loan', 117, '8921211328', '200000', '1', '2019-07-08 05:24:56'),
('Abhinandan', 'Mahale', '8805723123', 'amahale1991@gmail.com', 'Nashik', 'Personal Loan', 118, '8805724123', 'Com', '', '2019-07-04 12:22:32'),
('Husain', 'wala', '7976084538', 'Husainrw5253@gmail.com', 'Hakimi Traders shop no 11tipta', 'Personal Loan', 119, '7976102760', '50000', '', '2019-07-04 12:36:22'),
('Lokesh ', 'Mahala ', '9785318992', 'lokesh.mahala1997@gmail.com', '51/189,rajat Path, Mansarovar, jaipur ', 'Personal Loan', 120, '8426030021', '30000', '', '2019-07-04 12:54:04'),
('Sneha', 'Dubey ', '9670531903', 'snehadubey@yahoo.com', '348 viramkhand 5Gomtinagar Lucknow  ', 'Personal Loan', 121, '8975601293', '50000', '', '2019-07-04 12:56:06'),
('Rezaul', 'Islam', '6000173424', 'rajaahmed779@gmail.com', 'Lanka Hojai assam', 'Personal Loan', 122, '9085826365', '250000', '', '2019-07-04 13:01:09'),
('chandrasekhar', 'gupta', '6295262346', 'biki713201@gmail.com', 'irani para near burn company station market durgapur 1', 'Personal Loan', 123, '7074953821', '50000', '', '2019-07-04 13:32:26'),
('Vipulbhai ', 'Undkat ', '8735926885', 'Undkatvipul2@gmail.com ', 'Surat', 'Personal Loan', 124, '8980740231', '70000', '', '2019-07-04 13:46:58'),
('ANTHONY ', 'BHAGYARAJ', '9945638607', 'bagyaraj72@gmail.com', '157.yarandahalli.near vinayaka temple', 'Personal Loan', 125, '8431176553', '20000', '', '2019-07-04 13:56:28'),
('Ayesha ', 'Syed', '9164229585', 'Smasif23@yahoo.com', '9/1 b. G road', 'Personal Loan', 126, '9620639385', '200000', '', '2019-07-04 14:41:04'),
('Pawan', 'Anand', '9803258266', 'pawananand76@gmail.com', '1415 Phase 2 Urban Estate Dugri', 'Personal Loan', 127, '7986211620', '200000', '', '2019-07-04 14:47:50'),
('Nikhil', 'Babina', '8208120258', 'Nikhil.babina001@gmail.com', '15/117/1 Bairo wado dando caranzalem panjim goa', 'Personal Loan', 128, '7798105298', '100000', '', '2019-07-04 14:50:49'),
('Namrata ', 'Mane', '9158862859', 'Namrata.mane.com', 'Kadu plot Jalgaon road', 'Personal Loan', 129, '9765875556', '30000', '1', '2019-07-09 04:55:57'),
('rajneesh kumar', 'singh', '8052790490', 'raju.1985007@gmail.com', '636/15, 15A budh vihar takrohi indira nagar', 'Personal Loan', 130, '7007998912', '50000', '', '2019-07-04 14:59:16'),
('Deepak Kumar ', 'Agrawal ', '9200000044', 'agrawaldeepak111@yahoo.com', 'Raipur ', 'Personal Loan', 131, '9098579389', '1000000', '', '2019-07-04 15:06:05'),
('ram', 'naresh', '7007524274', 'ramnareshbharati61982@gmail.com', 'anpara sonbhadra', 'Personal Loan', 132, '6307484599', '50000', '', '2019-07-04 15:12:03'),
('Prabesh kumar', 'Shaw', '9830992524', 'prabesh08@yahoo.co.in', '285 Vivekananda Road ', 'Personal Loan', 133, '9830992524', '1500000', '', '2019-07-04 15:15:21'),
('RANA', 'KARMAKAR', '7908263879', 'ranarsushmita@gmail.com', 'Vill+p.o: sajnapara. P.s: DHUPGURI. Dist: jalpaiguri. Pin : 735212', 'Personal Loan', 134, '9679161645', '20000', '', '2019-07-04 15:18:02'),
('sagar', 'bavane', '7028793651', 'sagarbawane555@gmail.com', 'alka nagari society cidco aurangabad ', 'Personal Loan', 135, '7083299402', '150000', '', '2019-07-04 15:23:50'),
('Nunavath', 'Krishna', '9866165595', 'nknaik6@gmail.com', 'Lainthanda mandal gudur dist mahabubabad', 'Personal Loan', 136, '8978996202', '500000', '1', '2019-07-09 04:56:21'),
('PINTU ', 'CHAKRABORTY ', '9862693119', 'pcbagartala.78@gmail.com', 'North Indranagar, NANDANNAGAR ROAD, Kunjaban, 799006, AGARTALA, West Tripura ', 'Personal Loan', 137, '8794456237', '500000', '', '2019-07-04 15:25:10'),
('Devendra singh ', 'Yadav ', '9589589598', '1991 devendra yadsv', 'Gya jeet pura chheda teh.mohangarh distric.tikamgarh mp', 'Personal Loan', 138, '7620561602', '150000', '', '2019-07-04 15:26:03'),
('Akram ', 'Mansuri', '8619192683', 'akrammansuri04@gmail.com', '1418 danmal jinka ahata rangpur road kota jn', 'Personal Loan', 139, '9799227282', '100000', '', '2019-07-04 15:30:02'),
('Deepak ', 'Damar', '6261770592', 'deepakdamar329@gmail.com', 'Sinduriya, tehsil-sardarpur, District-Dhar,  madhya pradesh', 'Personal Loan', 140, '6261770592', '2,00,000', '1', '2019-07-09 05:41:51'),
('Likhith ', 'H A', '7676913651', 'aclikhith1993@yahoo.com', '#368/a, 3rd floor, 10th cross, 1st N Block, Rajajinagar, Bangalore ', 'Personal Loan', 141, '7760414684', '100000', '', '2019-07-04 15:43:45'),
('LOGANATHAN', 'BALAIYAN', '9894751681', 'loganathanbpl@gmail.com', '216 JAYARAJ NAGAR KEERANUR KOLLUMANGUDI POST', 'Personal Loan', 142, '9087279124', '50000', '', '2019-07-04 15:43:59'),
('Kapil', 'Kumar', '9354306503', 'Kk3001500@gmail.com', 'Khansa 37', 'Personal Loan', 143, '6204957641', '20000', '', '2019-07-04 15:44:55'),
('KUMAR', 'SHATRUGHNA', '7061879589', 'ntpl0612@gmail.com', 'vill-kanhwara,, p.o-haripur b.o.', 'Property Loan', 144, '7970893999', '100000', '', '2019-07-04 15:50:43'),
('Rajendra', 'Perei', '9566183013', 'rajendraperei1990@gmail.com', '2and Floor Microsoft Prsteg Polygon Teynapet', 'Personal Loan', 145, '9940400930', '25000', '', '2019-07-04 15:51:06'),
('?????', '????????', '7389577129', 'anshulgoswamijimaharaj@gimel.com', '???????? ??????.???.', 'Personal Loan', 146, '7389577129', '20000', '', '2019-07-04 15:53:45'),
('Sunkara ', 'Sreekanth ', '9515085668', 'Royalsreek15@gmail.com ', '2/12,bradipet,guntur', 'Education Loan', 147, '7036357920', '5000', '', '2019-07-04 16:55:09'),
('Chandrshekhar ', 'Devghare', '8788103590', 'Sdevghare071@gmail.com', 'Nagpur khaparkheda', 'Personal Loan', 148, '7030148803', '35000', '', '2019-07-04 16:58:19'),
('Triloki Nath', 'Tiwari', '8922805368', 'tnt32354@gmail.com', 'Vill. Bhadua Tarhar, Post Firojpur Tarhar, Distt. Gonda', 'Personal Loan', 149, '9120329816', '500000', '', '2019-07-04 17:11:19'),
('Onkar ', 'Singh', '8805355249', 'singhomkar482@gmail.com', 'Shadi glori  pl. 45/46 gat  199 room no_A/10 dharmik nagar satpur nashik  Maharashtra 422012', 'Personal Loan', 150, '7972838905', '500000', '', '2019-07-04 17:15:00'),
('Praneeth', 'Chowdary', '9908814769', 'praneeth404@gmail.com', 'Plot no 180,Mayuri nagar,miyapur', 'Personal Loan', 151, '9908814769', '5000', '', '2019-07-04 17:29:30'),
('Kvelu', 'Kvelu', '9944684727', 'Pazhamudircholaivelu@gmailcom', 'Sovripal', 'Personal Loan', 152, '9952221160', '200000', '', '2019-07-04 17:32:45'),
('samual', 'sarkar', '7478426030', 'samualsarkar1987@gmail.com', 'vill+p.o-kamdebkati.dist-north 24porgana.pin-743438', 'Personal Loan', 153, '7478426030', '50000', '', '2019-07-04 17:44:10'),
('Ashok ', 'Roy', '8334860625', 'Ashokroy002@gmail.com', 'Narendrapur ', 'Personal Loan', 154, '9051212104', '10000', '', '2019-07-04 17:48:08'),
('Dipika', 'Tyagi', '8920774502', 'Dipikatyagi161@gmail.com', 'Ghaziabad ', 'Personal Loan', 155, '8920774502', '60000', '', '2019-07-04 17:50:57'),
('Avijeet', 'Chowdhury', '9329009824', 'ultradurg@gmail.com', 'MIG 222 Padmanabhpur Durg', 'Personal Loan', 156, '9109194499', '150000', '', '2019-07-04 17:53:09'),
('Rakibul', 'Mallick', '7865081810', 'mallickrakibul044@gmail.com', 'Majhkanali , Papurdihi , Bankura Sadar , Bankura', 'Education Loan', 157, '8250134828', '800000', '', '2019-07-04 17:59:18'),
('Anil', 'Kumar', '9810709601', 'Karanvij 96@yahoo.com', '3467dariba pan Swami ram tiat nagar', 'Personal Loan', 158, '9650479601', '200000', '', '2019-07-04 17:59:38'),
('C', 'saravanan', '9789400668', 'saran.mecad@gmail.com', '3/2,  ragvendhra garden,  thottpalayam,  karuparanapalayam,  mylampatti (post),  coimbatore-', 'Personal Loan', 159, '9344880466', '50000', '', '2019-07-04 18:00:56'),
('Jalaluddin', 'Sk', '7432929844', 'jalaluddinsk370@gmail.com', ', , -CHHOTOMAHADIPUR, DEBIPUR(ACHINTALA)', 'Auto/Vehicle Loan', 160, '7001412140', '100000', '', '2019-07-04 18:12:14'),
('Kautik ', 'Raut', '9579548710', 'kautikraut5@gmail,com', 'Umari meghe', 'Personal Loan', 161, '9112833472', '50000', '1', '2019-07-15 06:42:09'),
('Peddiboina', 'Mallikarjuna rao', '9000796331', 'mallinaga7985@gmail.com', 'Marriwada', 'Personal Loan', 162, '9000796331', '200000', '', '2019-07-04 19:11:22'),
('ANDE', 'RAJESH', '9030595352', 'rajeshkalyan913@gmail.com', '18-15-10 Salipet Tenali', 'Personal Loan', 163, '7989184303', '250000', '', '2019-07-04 21:18:06'),
('Pawan', 'Wadhwa', '7400285282', 'Pawan.k.wadhwa999@gmail.com', 'C 804 ROOM NO 03, Hai baba dham', 'Personal Loan', 164, '8692814824', '100000', '', '2019-07-04 21:45:21'),
('Somanadhan', 'Rakesh Kumar', '9679572525', 'srakeshkumar 1947@gmail.com', 'Andaman &Nicobar', 'Personal Loan', 165, '9531840277', '300000', '', '2019-07-04 23:24:14'),
('Anandhan', 'Anandhan', '9159838183', 'perumalananthan1976@gmail.com', '1/687senthil NAGAR sick0', 'Personal Loan', 166, '8925431338', '5000', '', '2019-07-04 23:37:45'),
('Vijay Kumar soni', 'Soni', '9575620612', 'vijaysoni.soni1977@gmail.com', 'Vivek Vihar Colony Dabra m.p', 'Personal Loan', 167, '7566949678', '130000', '', '2019-07-05 02:14:28'),
('Jagatjeet', 'Singh', '7993341502', 'dashingjagat@gmail.com', 'H.no 11-37, mro office colony,  beside mro office,  Parawada mandalam,  parawada,  vishakapatnam, H.no 11-37,  mro office colony,  beside mro office, parawada mandalam, parawada,  vishakapatnam', 'Personal Loan', 168, '7993341502', '20000', '', '2019-07-05 02:16:59'),
('Brajkishor', 'sarswat', '9690729056', 'Brajkishorsarswat9690@gmail.com', 'Nagla neem semra khandauli agra', 'Personal Loan', 169, '9690729056', '50000', '', '2019-07-05 02:57:00'),
('Ravi', 'Suryanarayana', '9972113942', 'raviraj0610@gmail.com', '651,1st Main, Bhaskar Nagar, Alahally, Konanakunte,Bangalore 560062', 'Personal Loan', 170, '8068154700', '30000', '', '2019-07-05 03:12:45'),
('Atma ', 'Ram ', '9785494000', 'mainaatma11@gmail.com', 'Ward no13 dabli bas chugta 2 p b n', 'Personal Loan', 171, '9660121516', '30000', '', '2019-07-05 03:13:08'),
('Nakul ', 'Wadhwa ', '9953201103', 'goldlineventures@gmail.com', '10 east avenue road east Punjabi Bagh ', 'Personal Loan', 172, '9953201104', '500000', '', '2019-07-05 03:43:25'),
('Sandeep', 'Jangid', '8005578957', 'Sandeepjangir9516@gmail.com', 'Kh-7 murlipura jaipur Rajasthan', 'Personal Loan', 173, '8233089192', '1,00,000', '', '2019-07-05 03:47:26'),
('Satish', 'Dixit', '9880263517', 'satish7000@gmail.com', 'No 112 maruti layout 2 nd main marenahalli vijaynagar bangalore 40', 'Personal Loan', 174, '9731419222', '300000', '', '2019-07-05 03:54:02'),
('Gurvinder singh', 'So', '9779132601', 'Soni253040', 'Chet singh nagar st no 1 hause no 3262 gill road', 'Personal Loan', 175, '9779132601', '100000', '', '2019-07-05 04:12:47'),
('Mangu', 'Narsimha rao', '9014725563', 'bjy.mnrao@gmail.com', ' Bhongir ', 'Personal Loan', 176, '9014725563', '10000', '', '2019-07-05 04:13:24'),
('Vipin', 'Kumar', '8570949493', 'Vipinkumar2515@gmail.com', 'Pingli Chowk Kaithal Road karnal haryana', 'Personal Loan', 177, '8053441548', '50000', '', '2019-07-05 04:19:35'),
('Sandip ', 'Kumar ', '6386947761', 'Email', '370914498197', 'Personal Loan', 178, '6386947761', '200000', '', '2019-07-05 04:19:51'),
('Aparna', 'B', '9663685849', 'appuseenu814@gmail.com', 'Banglore peenya2nd stage  thigaralapalya', 'Personal Loan', 179, '9663685849', '600000', '', '2019-07-05 04:42:08'),
('Santosh', 'Choudhary', '9890598414', 'Santoshrajnandni143@gmail.com', '#10 apratim apartment near Kulkarni hospital Warje jakat naka', 'Personal Loan', 180, '7447474709', '300000', '', '2019-07-05 05:08:47'),
('Sadanand', 'Mutkule', '8888050054', 'Sagar8888sagar@gmail.com', '1056 juni police line murarji petha solapur', 'Personal Loan', 181, '7028288391', '150000', '', '2019-07-05 05:09:02'),
('RAJESH', 'S', '9751822299', 'Rajeshfire3@gmail.com', '46 KARUPPANAN PILLAI STREET NEAR POWER HOUSE OOTY 643001', 'Personal Loan', 182, '6382531341', '300000', '', '2019-07-05 05:15:50'),
('sunny', 'dewani', '8888529469', 'sunnydewani1111@gmail.com', 'near khan garage ppposite sunny builders', 'Personal Loan', 183, '9922334467', '50000', '', '2019-07-05 12:05:02'),
('Mahesh ', 'Sain', '7568750117', 'maheshsain589@gmail.com', 'KHAWASPURA JODHPUR ', 'Personal Loan', 184, '9166567261', '100000', '', '2019-07-05 18:36:12'),
('Arun', 'Kumar', '7892164112', 'Arunkumar14741@gmail.com', 'No8 4th cross hmt layout, Chowdiah block Rt nagar', 'Personal Loan', 185, '8123580676', '100000', '', '2019-07-05 18:36:13'),
('Salman ', 'Shaikh', '9137084519', 'salman829184@gmail.com', 'Pl lokhande marge chembur ', 'Personal Loan', 186, '8291848040', '50000', '1', '2019-07-12 05:22:20'),
('VEMALI', 'Mahesh', '8142749040', 'vemalimahesh120@gmail.com', '1-5-57, NEW MARUTHI NAGAR', 'Personal Loan', 187, '6300016528', '90000', '', '2019-07-05 18:36:48'),
('Sumanta', 'das', '8013661353', 'das.sumanta.das@gmail.com', 'No1 govt colony malda', 'Personal Loan', 188, '974902672', '200000', '', '2019-07-05 18:38:03'),
('Laxmidhar', 'Nahak', '8114923728', 'nareshdipu00@gmail.com', 'Nayagarh', 'Personal Loan', 189, '8117027311', '100000', '', '2019-07-05 18:38:39'),
('Kimaya', 'karale', '7208524529', 'Kkarale2@gmail.com', 'Kopar gaon kopar road', 'Personal Loan', 190, '9082605215', '15000', '', '2019-07-05 18:38:42'),
('vikram ', 'singh', '8320655489', 'v4rvickyrawat6598@outlook.com', 'karwar karnataka ', 'Personal Loan', 191, '8320655489', '3000', '', '2019-07-05 18:40:16'),
('Ghanshyam', 'Yadav', '8827111159', 'Gkybs12@gmail.com', 'WORD NO. 3 PANDHI, PANDHI POST PARAGAON', 'Personal Loan', 192, '9098818787', '50000', '1', '2019-07-12 05:31:16'),
('Pavan', 'Singh', '8143094123', 'Ps7486892@gmail.com', 'Mallapally hyd', 'Personal Loan', 193, '8143094123', '50000', '', '2019-07-05 18:41:02'),
('Jyoshna ', 'Roy', '6000200677', 'shubhmangalassam@gmail.com', 'Barpeta Road ', 'Personal Loan', 194, '6900689199', '2500000', '', '2019-07-05 18:41:04'),
('Chandan', 'Bhairati', '9767052676', 'chandan052676@gmail.com', 'Venubai apt, navare park, mr ayyappa mandir, ambernath west', 'Personal Loan', 195, '9975164916', '60000', '1', '2019-07-12 05:23:27'),
('NISHANT', 'Mohapatra', '9008768222', 'nishant.syn@gmail.com', 'Vandhana SS Lake View ApartmentBangalore 560068', 'Personal Loan', 196, '', '500000', '', '2019-07-05 18:42:16'),
('ASIM', 'ROYCHOUDHURY', '8777215393', 'asdemi2017@gmail.com', 'A/2 Anweshan , near badamtala, PO Andul Mouri Duillya, Dist Howrah, pin code 711302.', 'Personal Loan', 197, '7047028602', 'Rs. 25000.00', '', '2019-07-05 18:43:08'),
('Mohit', 'Yadav', '9910927026', 'mohityadav4121995@gmail.com', 'F-210 top floor karampura', 'Personal Loan', 198, '8447451839', '200000', '', '2019-07-05 18:43:29'),
('ABBAS ', 'Gazi', '9933530882', 'Abbasgazi351@gamil.com', 'Vill+post Keyatala ps Baruipur dis s24pgs', 'Personal Loan', 199, '9933530882', '200000', '', '2019-07-05 18:45:23'),
('Sher khan', 'Sher khan', '8777032146', 'Sherkhansherkhan4017 @gmail...', 'H127shyam Lal lane garden rsich kolkata24', 'Home Loan', 200, '8777032146', '500000', '', '2019-07-05 18:45:41'),
('Akash', 'Waghmode', '7350173969', 'akashw1515@gmail.com', 'Pune', 'Personal Loan', 201, '9096215831', '20000', '', '2019-07-06 18:50:21'),
('Ajay ', 'Singh', '7291905263', 'uk.7staritsolution@gmail.com', '152-A village Tekhand New delhi110020', 'Personal Loan', 202, '7210289274', '200000', '1', '2019-07-15 05:03:41'),
('Amarnath', 'Bhandari', '8240177215', 'amarnathbhandari2018@gmail.com', '4/1/2 GHOSH PARA LANE, SALKIA, HOWRAH -711106', 'Personal Loan', 203, '6289749851', '50000', '', '2019-07-06 18:57:38'),
('INDRA PRASAD ', 'SARMA', '9954535676', 'i.sarma63@gmail.com', 'Udalguri assam', 'Property Loan', 204, '+91 70027 57913', '500000', '', '2019-07-06 19:04:06'),
('Varun', 'Sharma', '8851824663', 'Ninja.sharma111@gmail.com', '54 gopal park gali no 3 Krishna nagar', 'Personal Loan', 205, '7217867790', '40000', '', '2019-07-06 19:30:31'),
('Abdu', 'Rahiman', '9539326344', 'Abduseevayi@gmail.com ', 'Puthiyangadi, ettamel, p.o madayi, kannur, kerala', 'Personal Loan', 206, '9895825344', '300000', '', '2019-07-06 19:34:25'),
('Lipika', 'Patnaik ', '7377550467', 'liza7377550467@gmail.com', 'Lipika patnaik, Scientific nagar, New bus stand road, Berhampur (Ganjam), Pin-760002', 'Personal Loan', 207, '9124444695', '100000', '', '2019-07-06 19:44:26'),
('Bs Salong', 'Debbarma', '6009282644', 'bssalong711@gmail.com', '110037 Mahipalpur south west Delhi', 'Personal Loan', 208, '8131955527', '4000', '', '2019-07-06 19:47:29'),
('Sam', 'Simon', '9637291145', 'carmelenterprises60@gmail.com', '2F/6, ward no.6, Goulloy, Nuvem', 'Personal Loan', 209, ',7507275547', '300000', '', '2019-07-06 19:49:43'),
('gousekarim', 'syed', '9703063494', 'gousekarim.11@gmail.com', '4-69,sivalayam street,pedanandipadu', 'Personal Loan', 210, '8500544888', '10000', '', '2019-07-06 19:52:42'),
('Raj kumar ', 'Raj kumar ', '6396010261', 'Rajkumar9740442933@gmail.com ', 'Old Faridabad Sector 14 house number 1015', 'Auto/Vehicle Loan', 211, '7291044261', '200000', '', '2019-07-06 19:53:27'),
('Bhupesh', 'kumar', '8396064666', 'www.bhupeshbhambani1111@gmail.com', 'Bhiwani', 'Personal Loan', 212, '9996763057', '70000', '', '2019-07-06 20:11:41'),
('Sanjay', 'Saini', '7742901840', 'sanjaysaini0710@gmail.com', 'Focus energy limited plot no.56 CVS colony near preeya hotal', 'Personal Loan', 213, '9610251450', '500000', '', '2019-07-06 20:12:25'),
('varun', 'luthra', '9582441254', 'vl9582441254@gmail.com', '5/9 lal quater lohia nagar ghazibad', 'Personal Loan', 214, '9555377887', '100000', '', '2019-07-06 20:20:34'),
('SK', 'ABdulqadir', '8297221036', 'QAdirskabdul8291@gmail.com', 'Hydarabad', 'Personal Loan', 215, '8522990036', '50000+', '', '2019-07-06 20:35:57'),
('Avinash', 'Kumar', '8368480080', 'AVI.k9015@gmail.cim', 'G-43 Jain road near DWARKa mor metro station', 'Personal Loan', 216, '9334412614', '100000', '', '2019-07-06 20:45:31'),
('Bikash', 'Mahato', '9007451753', 'vikx.mahato@gmail.com', '18sri Mani Bagan Lane Salkia', 'Personal Loan', 217, '8017572517', '100000', '', '2019-07-06 20:49:20'),
('Kishan', 'Singh', '7237 927 484', 'aaj9236tak@gmail.com', 'Unnao', 'Personal Loan', 218, '7237 927 484', '50000', '', '2019-07-06 21:01:31'),
('salman', 'ahmadi', '7979032553', 'salmanahmadi104@gmail.com', 'kalyani nadia A11/45A west bengal', 'Auto/Vehicle Loan', 219, '9308115108', '100000', '', '2019-07-06 21:18:11'),
('PALAYAM ', 'RAM MOHAN ', '8123108641', 'palayamrammohantkm12111985@gmail.com', '26/B,PERUMAL KOIL GARDEN, VANNIYAMPATHY STREET, RAJA ANNAMALAI PURAM, MANDAVELI, CHENNAI-600028', 'Education Loan', 220, '7550165886', '125000', '1', '2019-07-15 05:04:06'),
('Shubham', 'Dhanwani', '9893333538', 'Shubhamdhanwani16@gmail.com', 'Samta colony raipur c. G. ', 'Personal Loan', 221, '7772087990', '2000', '', '2019-07-06 21:47:09'),
('Dinesh ', 'Desai', '9712788797', 'Dineshdesai7871@gmail.com ', 'Block no 378/2040ghb bapunagar ahemdabad ', 'Personal Loan', 222, '7698186180', '25000', '', '2019-07-06 21:53:04'),
('Prakash ', 'behera', '9603373501', 'Prakashprakashbehera3489@gma', '1-109/35syno5bishapatinagerkongdapur', 'Education Loan', 223, '7981313829', '5000', '', '2019-07-06 22:09:55'),
('Ravi', 'singh', '7887372154', 'sabhi7978@gmail.com', 'Ganesh nagar church road manpada thane west', 'Personal Loan', 224, '9594468767', '10000', '', '2019-07-06 22:29:48'),
('BALVANT ', 'SHARMA ', '8814082951', 'avibalwant016@gmail.com ', '1025 ceher khurd bhiwani haryana', 'Personal Loan', 225, '8708190869', '600000', '', '2019-07-06 22:40:38'),
('Kanchibhatla', 'Kamalakar', '6302353893', 'kamalakar.kar@gmail.com', 'House no 1_29/267 jai jawan colony kapra ecil Hyderabad', 'Personal Loan', 226, '6301617704', '50000', '1', '2019-07-15 06:41:33'),
('Mohammed mohsin shaikh', 'Hussain shaikh', '8433888853', 'mohsinshaikh227.ms@gmail.com', 'room no 504 chohan avenue A wing new link road goregaon west, 504', 'Personal Loan', 227, '9769889227', '20000', '', '2019-07-06 23:01:58'),
('Ashfaq', 'Sorathia ', '7208813690', 'Ashfaq12344@gmail.com', '37 pinjara street crawford mkt', 'Personal Loan', 228, '8779390189', '200000', '', '2019-07-06 23:22:47'),
('Rakhi', 'Davi', '8630559532', 'rk9522069@gmail', '6/5prakashnagar,shaganj,agra', 'Personal Loan', 229, '8630559532', '50000', '', '2019-07-07 00:22:22'),
('Babu ', 'Babu ', '8080929523 ', 'babuahirwar1992@gmail.com', 'Pata 2 Gram chunha gram panchayat bhasunda tahsil ajaigarh District Panna MP. India ', 'Personal Loan', 230, '8080929523 ', '100000 ', '1', '2019-07-15 05:03:27'),
('Hymareddy', 'T', '9182966217', 'thymareddy@gmail.com', 'Nunusuralla Tuggali mandal kurnool ', 'Personal Loan', 231, '7285956125', '15000', '', '2019-07-07 00:47:18'),
('DINESH KUMAR ', 'DINESH KUMAR ', '7523986959', 'dineshkumar752398@gmail.com', 'Sidhauli', 'Education Loan', 232, '7523986959', '300000/-', '', '2019-07-07 01:03:24'),
('jeetu', 'Trivedi', '7880372917', 'jeetutrivedi7880@gmail.com', 'lalipur huasainganj Fatehpur 212651', 'Personal Loan', 233, '9161987708', '200000', '', '2019-07-07 01:57:48'),
('Uma', 'Das', '9284625602', 'Umashankerdas5746@gmail.com', 'Chaibasa ', 'Home Loan', 234, '7033408945', '100000', '', '2019-07-07 01:58:34'),
('Pujan', 'Roy', '8116110545', 'pujan9255@gmail.com', 'Dinhata village', 'Education Loan', 235, '8116110545', '3000', '', '2019-07-07 02:09:01'),
('Kanhaiya', 'rungta', '8808001144', 'rungta.kanha@gmail.com', 'Suvassh nagar padrauna', 'Personal Loan', 236, '9792505776', '100000', '', '2019-07-07 02:17:03'),
('Neeraj', 'Choudhary', '09588014845', 'nk30468@gmail.com', '16 Luhar pada Ghanoli ', 'Personal Loan', 237, '6377516094', '50000', '', '2019-07-07 02:17:54'),
('Gobinath', 'V', '9500691343', 'achievergobi@gmail.com', '19 Ganapathy flats NGO colony adambakkam', 'Personal Loan', 238, '8610607772', '500000', '', '2019-07-07 02:28:54'),
('Sangita', 'Dugaje', '9511519000', 'ganeshdugaje@gmail.co ', 'Dindori Nashik ', 'Property Loan', 239, '9527475425', '50000', '', '2019-07-07 02:33:35'),
('RAJEEVA', 'M', '8105501545', 'rajeevam467@gmail.com', 'JP Nagar 6th phese sarakki garden #10 13cross 35th mine', 'Personal Loan', 240, '9686120886', '50000', '', '2019-07-07 02:34:59'),
('Ramesh Chandra Arya ', 'arya', '8368722595', 'ramesharya.pm@gmail.com', 'P195pllingi vill sarojini nagar', 'Personal Loan', 241, '9873207765', '500000', '', '2019-07-07 02:46:32'),
('SHIBU', 'RAJAK', '7488436187', 'Shiburajak9433@gimil.com ', 'Aditypur MAJHI tola PANCWATI colony', 'Personal Loan', 242, '7541047964', '40000', '', '2019-07-07 02:55:35'),
('ullangula', 'gopikrishna', '9642918504', 'gopikrishnaullangula', 'abbinanigunta palem', 'Personal Loan', 243, '8340947987', '50000', '', '2019-07-07 03:02:16'),
('Shivaji', 'Rakde', '9545512721', 'Shivajirakde20@gmail.com', 'At kasod post talni tq sillod dist aurangabad ', 'Personal Loan', 244, '9423814516', '500000', '', '2019-07-07 03:05:21'),
('Aditya kumar', 'Aditya kumar', '7351934418', 'adityakumarak6339351@gmail.com', 'Araown road sirsaganj', 'Education Loan', 245, '7351934418', '1000000', '', '2019-07-07 03:20:20'),
('Priyanka', 'Priyanka', '8755419637', 'priyankaarisedata@gmail.com', 'New haridwar colony ranipur More haridwar', 'Personal Loan', 246, '7251035548', '100000', '', '2019-07-07 03:38:00'),
('mohammad', 'shafi', '9399363777', 'spur ', '9-111 sanjay gandhi nagar shapoor nagar quthbullapur ida jeedimetla k.v. rangareddy andhra parades 500055', 'Property Loan', 247, '9399363777', '1 lakh', '', '2019-07-07 03:39:02'),
('BUDDHMAN SINGH', 'PAW', '9131572659', 'Seenucut0@gmail.com', 'Shahdol, Shahdol area', 'Education Loan', 248, '7477049161', '7000', '', '2019-07-07 03:46:29'),
('Navsath begam', 'K', '9344997841', 'Moorthekrishna96@gmail.com', 'Chennai', 'Personal Loan', 249, '9344362391', '50000', '', '2019-07-07 04:03:02'),
('Sonu', 'Kumar', '9499456697', 'Mehandasonu39 @gmail.com', 'Hansi new subhash nager', 'Personal Loan', 250, '9499456697', '50000', '', '2019-07-07 04:03:46'),
('Pankaj', 'Chetry', '7086631199', 'thapapankaj444@gmail.com', 'Ouguri Nepali Gaon, Pathalial', 'Personal Loan', 251, '7002376025', '10000', '', '2019-07-07 04:45:46'),
('Nazneen', 'Saiyad', '7016739515', 'nazosaiyad189@gmail.com', 'Mota saiyadwada, thasra, gujarat', 'Personal Loan', 252, '7878221589', '45000', '', '2019-07-07 04:49:35'),
('Samuel', 'Digal', '8848574900', 'Samuelk.digal143@gmail.com', 'At/po:-Biringia, kurtamagarh, Kandhamal,Odiaha', 'Personal Loan', 253, '8763180969', '50000', '', '2019-07-07 04:51:20'),
('Chinmaya', 'routray', '9438639463', 'chinmayaroutray2019@gmail.com', 'Birajapur rambag jajpur, Birajapur rambag jajpur, Birajapur rambag jajpur', 'Education Loan', 254, '9658727022', '50000', '', '2019-07-07 04:55:55'),
('Aarthy', 'Rajkumar', '7010171696', 'aarthyramanathan17@Gmail. com', 'Block 4 door no 82 perumbakkam housing board', 'Personal Loan', 255, '8015741160', '40000', '', '2019-07-07 05:17:04'),
('Sugunendran', 'K', '8943945959', 'Suguhbr@gmail.com', 'AMBATTAMOOLA HOUSE , PO PONDI,KASRAGOD,KERALA', 'Personal Loan', 256, '9148012551', '25000', '', '2019-07-07 05:29:55'),
('Arvind ', 'Kumar ', '8299168728', 'arvindkumar09612@gmail.com', 'Raebareli ', 'Education Loan', 257, '8423833605', '50000', '', '2019-07-07 05:32:03'),
('Santhosh Kumar D ', 'D', '7353498060', 'santhoshkumard13@gmail.com', '#69 4th cross jayadevanagara metagalli mysore ', 'Personal Loan', 258, '9980946834', '200000', '', '2019-07-07 06:00:59'),
('Manoj', 'Pandey', '9415619412', 'Mp6761747@gmlcom', 'Vli new basti jalale pati post vulanpur pac Varanasi', 'Personal Loan', 259, '9120383200', '50000', '', '2019-07-07 06:03:00'),
('Priya Ranjan ', 'Mohanty', '7608003160', 'satyaprakashjhr@gmail.com', 'Nanpur Balichandrapur', 'Personal Loan', 260, '8269259343', '100000', '', '2019-07-07 06:37:34'),
('Saravanamuthu', 'mani', '8015292658', 'msmsaravana@gmail.com', '73,kamatchiamman kovil st,, Sindhamani,ramanathapuram', 'Personal Loan', 261, '8838382846', '10000', '', '2019-07-07 08:00:04'),
('Tapati', 'karmakar', '8617255258', 'ayan0409@gmail.com', '113A/5, Sarat ghoshgarden road', 'Personal Loan', 262, '9831890205', '50000', '', '2019-07-07 08:34:03'),
('Samreen', 'Ku', '9027090635', 'rihanmhommed6760@gmail.com', 'Jaishing pura khadar Tamoli dharmshala mathura (up) 281003', 'Education Loan', 263, '8810107971', '50000', '', '2019-07-07 08:50:06'),
('Jani Niranjan', 'Narhariprasad', '7600606342', 'janiniranjannarhariprasad@gmail.com', '41; payalpark Haripura Maninagar (East) Ahmedabad', 'Personal Loan', 264, '7600606342', 'Prasanal loan', '', '2019-07-07 08:57:34'),
('Priyanka', 'kasana', '7838604191', 'Kasanapriyanka12@gmail.com', 'Gaziyabad bhopura DLF', 'Personal Loan', 265, '9810410920', '50000', '1', '2019-07-12 07:09:44'),
('Vinay', 'Kumar', '7302990447', 'vinaykumar11121992@gmail.com', 'Adda sati Manikpur mode etawah', 'Personal Loan', 266, '7351143089', '25000', '1', '2019-07-15 04:15:30'),
('Rajeev ', 'Verma ', '9628577700', 'mohiraj1@gmail.com', 'Sinzai, behind sumer nursing home, Shahjahanpur ', 'Personal Loan', 267, '0000000000', '50000', '', '2019-07-07 10:10:25'),
('Vitthal', 'Kale', '8359053193', 'kalrvitthal041@gmail.com', 'Ekambi post Ujani dist Latur', 'Personal Loan', 268, '8329053193', '500000', '', '2019-07-07 10:12:19'),
('Yamini', 'Nagarajan', '9790599212', 'shobanabi1125@gmail.com', 'No 27 Krishna Nagar 4th Cross Street Iyyapanthangal Chennai-600056', 'Personal Loan', 269, '8072949709', '5000', '', '2019-07-07 10:16:03'),
('Parveen', 'kumar', '9518147765', 'parveenkumar200691@gmail.com', 'Panipat', 'Personal Loan', 270, '9050089724', '40000', '', '2019-07-07 10:25:53'),
('Sunitha', 'Pramodh singh ', '8489630683', 'Sunithasilvia1993@gmail.com', '2/499 Srinivasa 1st Cross street balaiah Garden madipakkam chennai 600091', 'Personal Loan', 271, '9566206087', '50000', '', '2019-07-07 10:38:40'),
('Basapi', 'Terangpi', '8638742776', 'hanryterangpi@gmail.com', 'Rongkethe near PHE Colony Diphu', 'Personal Loan', 272, '8638696370', '50000', '', '2019-07-07 10:54:15'),
('Sandeep', 'Thosre', '9926776618', 'Sandeepthosre123@gmail.com', '183 anand nagar shivnagar bhopal', 'Personal Loan', 273, '9926776618', '50000', '', '2019-07-07 11:28:54'),
('Sandeep', 'Thosre', '9303711484', 'Tejuthosre545@gmail.com', '183 anand nagar shivnagar bhopal', 'Personal Loan', 274, '9303711484', '100000', '', '2019-07-07 11:32:20'),
('Manish', 'Jadhav', '9699242715', 'maddyjazz44@gmail.com', 'Daurnagar Karanja Road Uran, House no. 1307', 'Personal Loan', 275, '9326571182', '50000', '', '2019-07-07 11:46:55'),
('Sanjaya ', 'Behera ', '8658790927', 'sanjayabehera036@gmail.com', 'At-Kumbharia. PS-BHANDARI POKHARI. DIST-BHADRAK ', 'Personal Loan', 276, '6372563462', '100000', '1', '2019-07-15 06:38:37'),
('Mithun ', 'Sarkar ', '9732724093', 'manassram@yahoo.co.in', 'Bagula Puratan Para, PO Bagula PS Hanskhali ', 'Personal Loan', 277, '8617451324', '9000', '', '2019-07-07 12:18:57'),
('Lakshmi', 'Bai d', '9739665771', 'laksmibaid16@gmail.com', '#24 7th cross 2nd B main road sunkadakatte pipeline road Bangalore 91', 'Personal Loan', 278, '9739665771', '500000', '', '2019-07-07 12:37:15'),
('Rameshm', 'Mallappa', '9738754977', 'ramesmr993@gmail.com', '122 1st cross 4th main beml layout rajarajeswarinagar', 'Personal Loan', 279, '9741536568', '1500000', '', '2019-07-07 13:39:48'),
('Vinod', 'Pal', '8573845320', 'palv3289@gmail.com', '130/232 A bagahi anshik TP Nagar Kanpur', 'Personal Loan', 280, '9026057390', '30000', '', '2019-07-07 13:48:52'),
('subodh', 'kumar', '8448532975', 'Subodhkumar07778@gmail.com', '338.5 ashok mhoolla nangloi', 'Personal Loan', 281, '8766335519', '100000', '', '2019-07-07 14:02:21'),
('Saurabh', 'Singh ', 'Saurabhyadavji@gmail.com ', 'saurabhyadavji78@gmail.com', 'Villess  Rajpur kalan post ofiss Rajpur Kalan pin code 243302', 'Auto/Vehicle Loan', 282, '7830003858', '60000', '', '2019-07-07 14:12:21'),
('Somesh ', 'Santra ', '9830896921', 'santrasomesh@gmail.com', 'Style, Village Gobindapur', 'Personal Loan', 283, '9830896921', '300000', '', '2019-07-09 10:35:03');
INSERT INTO `enquiry_master` (`fname`, `lname`, `mobile`, `email`, `address`, `loan_type`, `trial_id`, `alt_mobile`, `amount`, `status`, `entry_date`) VALUES
('Munireddy', 'Reddy', '9535148849', 'Munireddy ashraya badavane', 'Munireddy s/o anjappa ashtrays badavane chintamani-563125', 'Personal Loan', 284, '9686047469', '500000', '', '2019-07-09 10:39:44'),
('Avinash', 'Kandgule', '8530349842', 'avinashkandgule011@gmail.com', 'At.Bhangewadi.at.ausa.dist.latur', 'Auto/Vehicle Loan', 285, '9579764118', '200000', '', '2019-07-09 11:06:36'),
('Salman', 'Ansari', '9084178684', 'Salmanansari90841@gmail.com', 'Ahata isaiyaan tahsel sardahna meerut', 'Personal Loan', 286, '9084178684', '200000', '', '2019-07-09 11:33:23'),
('MALLIKARJUNA', 'THODETI', '9440771841', 'mallikarjunathodeti@gmail.com', 'Nayunipalle Cuddapah Varikunta Andhra Pradesh 516217', 'Personal Loan', 287, '7095542338', '400000', '', '2019-07-09 11:51:22'),
('Gurpreet ', 'Singh', '9041127292', 'Jai babe di 0001@ Gmail.com', 'Choti havali Ropar ', 'Personal Loan', 288, '9041127292', '300000', '1', '2019-07-15 06:39:47'),
('Siddu', 'Biradar', '8861624698', 'siddubiradar11.sb@gmail.com', 'shivashkti nagar kalburgi', 'Auto/Vehicle Loan', 289, '8861609077', '150000', '', '2019-07-09 12:41:14'),
('virmati', 'virmati', '7678267284', 'rakeshkumarokf5@gmail', 'H no.320 sanjay colony jcb chowk ballabgard faridabad', 'Personal Loan', 290, '7678267284', '700000', '1', '2019-07-15 04:39:35'),
('Aneesfathima', 'Aneesfathima', '9087787424', 'rosanasraf00529@gmail.com', 'No. 2Amooventharnagar.kuniyamuthur', 'Personal Loan', 291, '9087787424', '10000', '', '2019-07-09 13:15:36'),
('Nolon ', 'Narzary ', '9678027917', 'nolonnarzary80271@gmail.com', 'Maoriapara', 'Home Loan', 292, '9678027917', '5.00000', '', '2019-07-09 13:54:15'),
('Nakul ', 'Tassa ', '6900765544', 'dtassa359@email.com', '1no mohmari duliajan asssm ', 'Personal Loan', 293, '6900765544', '300000', '1', '2019-07-15 06:40:06'),
('Seetaram ', 'Regar', '7568268857', 'Babulalregar9001@gmail.com', 'Barood 311025  hamiragarh bhilwara rajasthan', 'Personal Loan', 294, '6375626504', '200000', '', '2019-07-09 14:08:54'),
('Khalid ', 'Shaikh', '9325488096', 'Khalidshaikh39@gmail.com', 'House no 614 chandrawado fatorda Margao Goa ', 'Personal Loan', 295, '9325488096', '500000', '', '2019-07-09 14:30:13'),
('Govind', 'Saini', '7500856949', 'chaudharyronit890@yahoo.com', '157 vijay nagar', 'Personal Loan', 296, '7017859573', '300000', '', '2019-07-09 14:41:48'),
('MOHD SHAFI', 'shafi', '8923094089', 'mohdshafi92707@gmail.com', 'Murtaza pur bulaki urf peada', 'Personal Loan', 297, '7078175859', '200000', '', '2019-07-09 14:46:09'),
('Rahul', 'Halder', '9933024811', 'rahulhalder8540@gmail.com', 'Joyram pur', 'Auto/Vehicle Loan', 298, '8016315252', '4000', '', '2019-07-09 15:06:03'),
('Anup', 'Patil', '9893816190', 'anuppatil1111@gmail.com', 'Near Binaki Buddha Vihar, Zenda Chowk', 'Personal Loan', 299, '7887441940', '10', '', '2019-07-09 15:06:08'),
('Dipak kumar ', 'Pandey', '9458689774', 'dpandey4313@gmail.com', 'Village. Jamouli  post. Kanchausi   disstric. Auraiya', 'Personal Loan', 300, '7505428803', '150000', '', '2019-07-09 15:37:44'),
('G SRIDHAR', 'Sridhar', '7397410047', 'sridharg618@gmail.com', '47,Anna Nagar , Kanchipuram', 'Personal Loan', 301, '7397331174', '50000', '', '2019-07-09 15:43:20'),
('Shaikh ', 'Nadeem ', '9175013339', 'nadeemshaikh0901@gmail.com', 'Aurangabad ', 'Personal Loan', 302, '7507395146', '20000', '', '2019-07-09 15:49:14'),
('Jignashaben ', 'Parmar', '8000853111', 'sabbatic.infra@gmail.com', '9, Krishna rowhouse Palanpur Jakatnaka surat ', 'Personal Loan', 303, '9726191238', '300000', '', '2019-07-09 15:53:19'),
('Kuldeep', 'kumar', '9915411472', 'Ds99154@gmel com..', 'Vpo Hamira', 'Personal Loan', 304, '9814407984', '100000', '', '2019-07-09 15:59:08'),
('Sandeep Singh', 'Singh', '9888777616', 'sandeepsinghpatialvi5@gmail.com', ' #30village Ablowal mistriya muhlla post off sidhuwal teh dist Patiala', 'Personal Loan', 305, '9888523906', '100000', '', '2019-07-09 16:03:03'),
('Alankar', 'Singh', '9720553948', 'youremailsinghalankar9720553948@gmail.com', '5 k kaysthan Puranpur pilibhit', 'Personal Loan', 306, '9452131929', '150000', '', '2019-07-09 16:07:06'),
('Shaik', 'Asweena ', '8008823356', 'shaikasweena94910@gmail.com', '1/547-B12 yanadi colony', 'Personal Loan', 307, '9398596624', '200000', '', '2019-07-09 16:08:06'),
('Ankush', 'Rode', '9119474991', 'Ankush.015080@gmail.com', 'Kaudgaon jalna', 'Personal Loan', 308, '9145576246', '50000', '', '2019-07-09 16:09:35'),
('Rasmiranjan', 'pradhan', '7077718690', 'rasmiranjanpradhan59@gmail.com', 'At-mahula,po-angarapada,dist-khurda', 'Personal Loan', 309, '9937356500', '20000', '', '2019-07-09 16:20:03'),
('Avtar', 'Singh', '7508713826', 'avtarsingh13826@gmail.con', '#2232/1 Manimajra Chandigarh', 'Personal Loan', 310, '7696637628', '50000', '', '2019-07-09 16:23:18'),
('SHIBABRATA', 'DAS', '9331423056', 'shibabrata1111@gmail.com', 'P15 Bandipur Road kolkata 700093', 'Personal Loan', 311, '9051076864', '60000', '1', '2019-07-15 04:14:58'),
('Chitendra ', 'Baral', '9556958843', 'CHITENDRABARAL@GMIL.COM', 'bhubaneswar', 'Education Loan', 312, '8917423590', '200000', '', '2019-07-09 16:31:08'),
('K raghavendra ', 'Kodanda Rama ', '9035310807 ', 'krv.rc35@gmail.com', '#952 Shanthinagar 10th dhargajogalli doddballapur 561203 ', 'Personal Loan', 313, '9842572182', '15000 ', '', '2019-07-09 16:32:45'),
('Shaik ', 'Jakir ', '9963450903', 'Jakirskaik@1989gmail.com ', '3-1-74/a', 'Personal Loan', 314, '7013178800', '200000', '1', '2019-07-15 04:14:29'),
('Dharavath', 'Lachiram', '9701404652', 'dharavathlachiram844@gamil.com', 'Bojjyagudamthanda\'shanthinagar, Bojjyagudamthanda\'shanthinagar, Bojjyagudamthanda\'shanthinagar', 'Personal Loan', 315, '+919701404652', '50000', '', '2019-07-09 16:40:31'),
('Sunita', 'gupta', '8777821152', 'gsneha2001@gmail.com', '302 A.P.C ROAD, IDEAL HEIGHTS, BLOCK D, FLAT 4D, SEALDHA', 'Personal Loan', 316, '9330222310', '300000', '', '2019-07-09 16:44:14'),
('AVDHESH KUMAR KHRWARE', 'KHWARE', '9455840623', 'awadhesh9455840@gmail.com', 'Bhadurpur post Devkali Ballia', 'Personal Loan', 317, '8299811788', '800000', '', '2019-07-09 16:46:21'),
('mohd ishaque', 'shaikh', '8097813328', 'ishaqueshaikh960@Gmail.com', '301/3rd sudiksha apartments e wing sanjay nagar gaon devroad mumbra dist thane', 'Personal Loan', 318, '9594595761', '50000', '', '2019-07-09 16:48:08'),
('Tarun ', 'Singh', '9536579861', 'tarunrajput1996tsm@gmail.com', 'VILL.DEDUL GAON, PO. JHATKANDI, BEDUL GAON,PAURI GARHWAL', 'Personal Loan', 319, '9411507552', '100000', '', '2019-07-09 16:51:18'),
('Sandeep ', 'Kumar ', '9125444490', 'Sk821705@gmail.com ', 'Kushinagar ', 'Personal Loan', 320, '7897424490', '100000', '', '2019-07-09 17:00:09'),
('Reeshoo ', 'Pratap singh', '9058946004', 'Reeshoopratapsingh415@gmail.com', 'Nagala koom', 'Personal Loan', 321, '7500518028', '5000', '1', '2019-07-15 04:14:47'),
('Chirag', 'Parmar', '8154849975', 'chirusa2951@gmail.com', 'Jain society behind HDFC bank, Opp nagarpalika bajar road', 'Education Loan', 322, '08154849975', '13000', '', '2019-07-09 17:03:16'),
('Shatrughan', 'Singh', '9601006206', 'Panditshatrughansingh@gmail.com', 'Ahmadabad', 'Personal Loan', 323, '9601006206', '200000', '', '2019-07-09 17:07:15'),
('Paresh', 'Soni', '9998450424', 'Pareshsoni849@gmail.com', 'B 207 pooja avenue, Waghodia dabhoi ring road', 'Personal Loan', 324, '7990824430', '20000', '', '2019-07-09 17:10:18'),
('SHIVRAM', 'DAHARE', '9303629193', 'ramshiv88776@gmail.com', 'New santoshi para camp 2 Bhilai cg', 'Personal Loan', 325, '9303629193', '70000', '', '2019-07-09 17:11:51'),
('Mohd ', 'Waseem', '8096013545', 'mdwaseem152@gmail.com', 'Quba colony Shaheen nagar  Hyderabad', 'Personal Loan', 326, '8686310737', '50000', '', '2019-07-09 17:19:42'),
('Dhirendra ', 'Singh', '8851545523', 'Dhirendra.singh90@gmail.com', 'Q, 1/27b Mohan garden uttam nagar New Delhi ', 'Personal Loan', 327, '9650417068', '70000', '', '2019-07-09 17:20:11'),
('Amit', 'Chowdhary', '8820273057', 'amitamm03@gmail.com', '58/1 DAYAl Banerjee road', 'Personal Loan', 328, '8820021172', '250000', '1', '2019-07-15 04:14:14'),
('Mukti', 'Basumatary', '7896797919', 'Muktibasumatary123@gmail.com', 'Vill-Ganesh Tangia Post:Gohpur Dist-Biswanath (Assam)', 'Personal Loan', 329, '7896797919', '5,00.000', '', '2019-07-09 17:21:50'),
('Jenifer', 'Paul', '7033697828', 'jenny.r.paul@gmail.com', 'Basanti Apartment A3/1 tank road Kadma Jamshedpur', 'Personal Loan', 330, '7033697828', '150000', '', '2019-07-09 17:22:44'),
('Jaskaran', 'Singh', '8427945139', 'js866113@gmail.com', 'Bhamiana kalan Tajpur roud near Hundal chonk ludhiana', 'Personal Loan', 331, '7888856434', '300000', '', '2019-07-09 17:28:08'),
('sachin ', 'kumar', '6396317902', 'Kashyapgurukul0@gmail.co', 'Bhikkampur jeetpur laksar haridwar', 'Personal Loan', 332, '9837900268', '34000', '', '2019-07-09 17:37:38'),
('subodh ', 'kumar', '8709054589', 'singh.subodh5590@gmail.com', '303 Geetanjali boys pg', 'Personal Loan', 333, '9431510193', '40000', '', '2019-07-09 17:42:55'),
('Pritan', 'Chakraborty', '8416016260', 'pritamudp9@gmail.com', 'Udaipur', 'Education Loan', 334, '8415024121', '900000', '', '2019-07-09 17:46:55'),
('Payal', 'Roy', '8981037841', 'payalroy178@gmail.com', '788/1 sahid khudiram bose sarani, South dum dum', 'Personal Loan', 335, '7980762260', '180000', '', '2019-07-09 17:51:18'),
('Ganesh ', 'Avhad', '9823126787', 'avhadganesh.ga84@gmail.com', 'ROW NO 5 PLOT NO 54 SARVE NO 100/A SAMRTH ROW HOUSES JADHAV SANKUL CHINCHOLE SHIWAR AMBAD LINK LOAD SATPUR NASHIK PIN 422010', 'Personal Loan', 336, '9423664263', '100000', '', '2019-07-09 17:54:42'),
('Nitesh', 'Kumar', '9555220294', 'nktiwari0007@gmail.com', 'W-190 janki. Vihar prem nagar 2 kirari delhi 110086', 'Personal Loan', 337, '9971536648', '300000', '', '2019-07-09 18:10:07'),
('Kumar smitabhanu ', 'Tung', '7008977535', 'kumar4me@ymail.com', 'Kaudia,sanmundhabani', 'Personal Loan', 338, '8763522477', '20000', '', '2019-07-09 18:14:38'),
('k', 'pradhan', '8093995568', 'kaleswarpradhan182@email.com', 'At.palli', 'Auto/Vehicle Loan', 339, '7978215162', '10000', '', '2019-07-09 18:31:31'),
('SHIV', 'KUMAR ', '8607000411', 'Shivranga1999@gmail.com ', 'V.P.O.-SEENK, TEH.-ISHRANA, DISTT.-PANIPAT', 'Personal Loan', 340, '8607666631', '50000', '', '2019-07-09 18:40:15'),
('Pakeerugari', 'Naveen sai', '798912100', 'Naveensaipakeerugari@gmail.com', '18 -306 gajula street venkatagiri', 'Personal Loan', 341, '9652326241', '50000', '', '2019-07-09 18:56:48'),
('Vijay', 'Gupta', '7066484629', 'Vijaycgupta@gmail.com', 'A/02 sai krupa jyoti society shirdi nagar achole road nalasopara east', 'Personal Loan', 342, '9029322390', '20000', '', '2019-07-09 19:01:48'),
('Lavleen', 'Sharma', '9058251613', 'chanchalshivnit@gmail.com', 'State uttar prdesh distt bulandshshr khurja jn 203132', 'Personal Loan', 343, '09058251613', '500000', '', '2019-07-09 19:31:29'),
('Bipla', 'Mahanta ', '9749887481', 'Mahanta130@gmail.com ', 'West bengol cooch behar ', 'Personal Loan', 344, '9002315423', '35000', '', '2019-07-09 22:01:10'),
('Sakthivel', 'Ramasamy', '9715149007', 'sakthivel.ramasamy89@gmail.com', '344, kas nagar, railway colony post, erode', 'Personal Loan', 345, '9965877744', '200000', '', '2019-07-10 01:35:26'),
('Sanjeev', 'Kumar', '7454944554', 'yadavjisky7454944554@gmail.com', 'Tejganj jasmai mainpuri up', 'Education Loan', 346, '7454944554', '25000', '', '2019-07-10 02:07:17'),
('Virendre Singh ', 'Virendre Singh ', '9518892543', 'vs601222@gmail.com', 'Kuruksatre ', 'Personal Loan', 347, '9518892543', '400000', '', '2019-07-10 02:20:25'),
('Sreekurthi', 'Raveendrareddy', '8179243387', 'Sreekurthiraveendrareddy', 'Chowdepalli(v),o.d.c(m),anantapur(d),a.t.p(s)', 'Personal Loan', 348, '8179243387', '20000', '', '2019-07-10 03:36:14'),
('Shaikmohammed idrish', 'Idrish', '7702916515', 'Zeenath plumbing services 123@gmail.com', 'Sai Nagar flat number 86 RIDALABAZAR', 'Personal Loan', 349, '6304597574', '100000', '', '2019-07-10 03:36:47'),
('Neeraj', 'KALYAN', '8920881444', 'shobhakalyan1999@gmail.com', 'Gao Sihi Secter 8 Near Valmiki Mandir FARIDABAd', 'Personal Loan', 350, '8920881444', '1,50,000', '', '2019-07-10 03:41:41'),
('Ashvin', 'Sahare', '9156856294', 'priyankasahare71@gmail.com', 'Rajesh mate ke Ghar ke pass new coloney pachamari mohalla sadar nagpur', 'Personal Loan', 351, '9156856294', '50000', '', '2019-07-10 04:30:41'),
('sundram', 'kumar', '6239826756', 'Sundramkumar1234@gimal.com', 'Susta sakra muzaffarpur', 'Auto/Vehicle Loan', 352, '6239826756', '50000', '1', '2019-07-15 07:15:47'),
('Sarat', 'Nath', '9749878058', 'saratnath75@gmail.com', 'Vill.hridaypur post:kalabari bagan dist:jalpaiguri west bengal', 'Personal Loan', 353, '7063861895', '10 laks lone', '', '2019-07-10 06:48:20'),
('SUNIL', 'Gope', '9724226973', 'sunilgope484@gmail.com', 'Tola kandesai KHARPOSH', 'Personal Loan', 354, '7490865480', '500000', '', '2019-07-10 07:17:22'),
('Sandip', 'Shinde', '9682330953', 'Shindesl251@gmail.com', 'Khopoli , katrang , pin 410203, State :- MH ', 'Personal Loan', 355, '7030938899', '10000', '', '2019-07-10 07:29:30'),
('SUSANT KUMAR', 'SAHOO', '9692062034', 's.kumar.mitu1978@gmail.com', 'PLOT NO.LIG.K.4.1048,KALINGA VIHAR .BHUBANESWAR751019', 'Personal Loan', 356, '9439878951', '200000', '', '2019-07-10 07:43:16'),
('Rafeekh', 'Parakkal paramba', '9895685744', 'rafeequebhilai007@gmail.com', 'Hemi villa.mannur valavu.po.mannur, Kozhikkode.kerala.', 'Personal Loan', 357, '989568574', '100000', '', '2019-07-10 08:04:34'),
('Dampanababoyina rambabu ', 'Dampanababoyina rambabu ', '8885424271', 'Ram690482@email.com ', '12-10-650 warasiguda secunderabad hyderabad 500061', 'Personal Loan', 358, '88i5424271', '10000', '', '2019-07-10 08:05:21'),
('Nishant', 'Kumar', '9000791124', 'nishantkmr6@gmail.com', '3-6-89/4 nehru nagar west marredpally Secunderabad', 'Personal Loan', 359, '8919315175', '50000', '', '2019-07-10 08:07:34'),
('Biraj', 'Mazumder', '7648024374', 'birajmazumder483@gmail.com', 'S/O WARD- 01 PV 38 INDRAPRASTH KANKER PVCHATTISGARH, PAKHANJUR', 'Personal Loan', 360, '7587707804', '100000', '', '2019-07-10 08:44:28'),
('Sunil', 'kumar', '8960908427', 'Kr89609084@gmail.com', 'Sirwara road', 'Personal Loan', 361, '8382984105', '25000', '1', '2019-07-17 05:55:50'),
('Pradeep kumar', 'Hardayal singh', '7477209537', 'Pradeepkhanna522@gmail.com', 'Gram mustri post baghoura tahesil mehghaon bhind', 'Education Loan', 362, '7477209537', '25000', '', '2019-07-10 08:59:33'),
('Sanjana ', 'Ohri ', '7217813274', 'Sanjanaohri79@gmail ', 'H36 Sant nagar extn tilak Nagar ', 'Personal Loan', 363, '8860490021', '10000', '1', '2019-07-16 05:48:29'),
('Hemalatha', 'Perumal', '9884150486', 'nidharshana1524@gmail.com', 'No:2/6,oilmill road, parasunath nagar, Thundalam, Chennai-600077', 'Personal Loan', 364, '8825840889', '50000', '', '2019-07-10 09:14:01'),
('Kapil', 'Setia', '9660025842', 'kapilsetia001@gmail.com', '113, pooja colony', 'Personal Loan', 365, '8000012791', '2500000', '', '2019-07-10 09:19:06'),
('dipankar', 'das', '8402908645', 'dipankardas777555@gmail.com', 'palasbari ward no 3 guwahati assam pin 781128', 'Personal Loan', 366, '7002548076', '50000', '', '2019-07-10 09:22:33'),
('ANUP KUMAR', 'DAS', '8637545973', 'anup34447@gmail.com', 'RUPSINGH JOTE, GOSSAINPUR, PO. BAGDOGRA, DIST- DARJEELING(WB), PIN-734014', 'Personal Loan', 367, '7584996710', '20000', '', '2019-07-10 09:48:38'),
('Vandana', 'Ahirwar', '6263962654', 'abhishek8889952914@gmail.com', 'Niwari Tikamgarh', 'Personal Loan', 368, '6263962654', '100000', '', '2019-07-10 09:52:54'),
('Lokesh', 'Sharma', '9160190092', 'lksharma226@gmail.com', 'Naval dockyard vizag Andhra Pradesh 530014', 'Personal Loan', 369, '9671492981', '1000000', '', '2019-07-10 09:54:42'),
('Priya', 'rao', '9887537270', 'prao27720@gmail.com', 'A-46, Shiv nagar-A colony', 'Education Loan', 370, '9887537270', '800,000', '', '2019-07-10 09:59:55'),
('ANISA', 'BEGUM', '9952064881', 'anisabegum306@gmail.com', '10/19 kathbada1stlane Chenna', 'Personal Loan', 371, '9952064881', '10000', '1', '2019-07-16 05:48:48'),
('Gajananad', 'Gope', '6204847169', 'gajanandgope1996@gmail.com', 'Vill-tirildih post office-gitilata po-potka ', 'Personal Loan', 372, '7321079994', '50000', '', '2019-07-10 10:07:00'),
('Ajay Kumar', 'Sharma', '9918066126', 'as8257978@gmail.com', 'Ram gang pakka talab fathepur', 'Personal Loan', 373, '9918066126', '50000', '', '2019-07-10 10:16:17'),
('Abrar ', 'Zargar', '7889622148', 'Ma7415513@gmail.com', 'Bhaderwah', 'Education Loan', 374, '9596701855', '10000', '', '2019-07-10 10:22:41'),
('ANJALI', 'MALIK', '9466526002', 'supreetghangas001@gmail.com', 'V.P.o Bandh, 354', 'Personal Loan', 375, '9896484358', '2150', '', '2019-07-10 10:32:17'),
('Umashankar ', 'Ghogare ', '9011892714', 'ghogareshankar5@gmail.com', 'Kasar galli kakramba osmanabad tuljapur ', 'Personal Loan', 376, '8380911842', '30000', '', '2019-07-10 10:35:50'),
('Moloy', 'Kundu', '9609991739', 'moloy.chottu@gmail.com', 'No.2 govt.colony,malda', 'Personal Loan', 377, '8145737377', '6000', '', '2019-07-10 10:39:30'),
('lakhan kumar', 'Khatwa', ',9462564128', 'lakhan.md3353@gmail.com', 'Kumari Gate outside ka jabardasti Nagaur', 'Personal Loan', 378, '9462564118', '100000', '', '2019-07-10 10:40:43'),
('Devanand', 'Ingade', '7039556954', 'pradnyaingle1707@gimal.com', 'Flat No 203trabkshvr Darshan Chaitanya Sankul Katrap Badlapur', 'Personal Loan', 379, '9137344900', '50000', '', '2019-07-10 10:41:33'),
('Nitin', 'Ahire', '8482906043', 'nitina212@gmail.com', 'F2/3 Cathay colony rest camp road devlali camp nashik', 'Personal Loan', 380, '7498414517', '20000', '', '2019-07-10 10:43:00'),
('Amit', 'Shrivastav', '7632098457', 'amitshrivastav6565@gmail.com', 'Harikartar colony street no3 house no-3353 Ludhiana punjab141003', 'Personal Loan', 381, '6284657350', '10000', '1', '2019-07-15 06:02:29'),
('tanmoy', 'patra', '9804702474', 'tanmoy626@gmail.com', 'nabadwi,nadia,west bengal', 'Personal Loan', 382, '9776837708', '10000', '1', '2019-07-15 06:02:46'),
('Sandeep ', 'Somani ', '8059844166', 'Sandeepsomani303@gmail.com ', '730 A indar puri mohhla near shiv chowk ', 'Personal Loan', 383, '8059844166', '500000', '', '2019-07-10 10:51:52'),
('Amitkumar', 'Vyawahare', '7517352495', 'amitvyawahare1988@gmail.com', 'At Post Pipla D. B', 'Personal Loan', 384, '9545897951', '50000', '', '2019-07-10 10:59:04'),
('Indranil', 'Acharyya', '8240163371', 'indranilacharyya46@gmail.com', 'P-396 Purna das road, Golpark', 'Personal Loan', 385, '7890769901', '20,000', '', '2019-07-10 10:59:26'),
('Ajad', 'Singh', '9878132862', 'ajadsinghbhojgi@gmail.com', '2911 sector 20c chandigarh', 'Personal Loan', 386, '7888838237', '300000', '', '2019-07-10 11:02:24'),
('Ashish', 'Sharma', '7046963224', 'ashishsharma7046963224@gmail.com', 'Dehra, Siyana', 'Personal Loan', 387, '9027762488', '20000', '', '2019-07-10 11:02:34'),
('Umesh ', 'Kumar', '7895469791', 'umeshkumar865012222@gmail.com', 'Bekeir shapur bereiily', 'Personal Loan', 388, 'Bareilly', '20000', '1', '2019-07-15 09:20:07'),
(' Shravan', 'Gawali', '8975442437', 'Svngavali88@gmail.com', 'Sundarpur tal niphad dist nashik', 'Personal Loan', 389, '9011078606', '10000', '', '2019-07-10 11:08:56'),
('Aqeelur', 'Asif', '9366969560', 'asifmechanical2010@gmail.com', 'Sivasagar', 'Personal Loan', 390, '7005702035', '70000', '', '2019-07-10 11:15:14'),
('Sikhamoni', 'Saikia', '8011496138', 'Sikhasaikia126@gmail..com ', 'Ratanpur Near Dhemaji collage', 'Personal Loan', 391, '8471923221', '100000', '', '2019-07-10 11:23:39'),
('Mandeep', 'Shukla', '9599540150', 'mandeep.shukla@gmail.com', '373 shahpur jat new delhi 110049', 'Personal Loan', 392, '9667382181', '50000', '', '2019-07-10 11:38:09'),
('  Bhrigu Kumar', 'Saiki6', '9101419351', 'saikiab626@gmail.com', 'murhadol salaguri jamugurihat sonitpur assa', 'Personal Loan', 393, '9435068350', '300000', '', '2019-07-10 11:42:24'),
('Karma Tshering ', 'bhutia ', '9083110494', 'Karmaspurs91@gmail.com', 'Rumtek east sikkim', 'Personal Loan', 394, '9083110494', '60000', '', '2019-07-10 11:43:20'),
('Lilaram g', 'purohit ', '9351645089', 'raygurlalitrajpurohit@gmail.com', 'gumanaram PUROHIT 270 Juthli Jeran Tesil Bagoda Jila Jalor rajasthan ', 'Personal Loan', 395, '9351645089', '100000', '', '2019-07-10 11:45:16'),
('Manish', 'Nama', '7742611197', 'namamanish56@gmail.com', 'Balaji ke Mandir ke pass khera rasool pur,', 'Personal Loan', 396, '07568857849', '10000', '', '2019-07-10 11:58:00'),
('Mohd Farman', 'Ali', '8447932170', 'Mohdfarman9639229960@gmail.com', 'Hasanpur Amroha', 'Education Loan', 397, '7037183748', '500000', '', '2019-07-10 12:00:22'),
('Shashikant', 'Sable', '9881725251', 'shashikant194455@gmail.com', 'Plot no 88 Gujrat colony near Kavita apt kothrud Pune', 'Personal Loan', 398, '8308675691', '10000', '', '2019-07-10 12:00:35'),
('Vivek', 'Singh', '6307690200', 'viveksingh0209.lko@gmail.com', 'Hash nagar faizullaganj lucknow', 'Personal Loan', 399, '9838490592', '25000', '', '2019-07-10 12:01:28'),
('suraj', 'kumar', '8953783258', 'surajkaithal99@gmail.com', 'sukhi.ka.purwa.khaga.fatehpur ', 'Property Loan', 400, '9795984162', '500000', '', '2019-07-10 12:14:29'),
('Priyanka', 'Rauthan', '8860012843', 'rauthan.priya21@gmail.com', 'D-37 street no.6 rajnagar part 2 palam colony', 'Personal Loan', 401, '7042886781', '50000', '', '2019-07-10 12:18:42'),
('Depara', 'Anand', '7075757052', 'deparaanand@gmail.com', 'S/o depara peruku naidu marupalli village srungavarapukota mondel', 'Personal Loan', 402, '317623185374', '5000', '', '2019-07-10 12:30:19'),
('Jakhir', 'Hussain', '9961872927', 'Jaki5281@gmail.com ', '40.236.t.mj qutras  thayineri payyanur ', 'Personal Loan', 403, '8921118608', '500000', '', '2019-07-10 12:36:25'),
('Bibeka', 'Panigrahi ', '9437514637', 'bibeka74@gmail.com', 'Jagannathpur Bhadrak Orissa ', 'Personal Loan', 404, '6371689350', '15000', '', '2019-07-10 12:53:42'),
('Sunil kumar', 'Sunil Kumar', '9250573456', 'sunilkumarsunilyadav72@gemil.com', 'B-73balbir Bihar suleman nagar  sec20 rohini kirari', 'Personal Loan', 405, '9250573456', '1,000000', '', '2019-07-10 12:54:55'),
('nilesh', 'gajjar', '8600592436', 'buntynil@gmail.com', 'h-305, gayatridham phase-2, plesantpark, miraroad east.', 'Personal Loan', 406, '8451826242', '50000', '', '2019-07-10 12:55:58'),
('Shubham', 'Agarwalla', '9749373795', 'Shubhamagarwal420420@gmail.com', 'Rajganj bazar,sukhani 735134', 'Personal Loan', 407, '7076319262', '50000', '1', '2019-07-16 05:49:15'),
('Lilan kumar ', 'Dash', '9438854988', 'dashlilankumar@gmail.com', 'Samraiepur, gelpur, dist-bhadrak, orissa ', 'Personal Loan', 408, '9583834988', '500000', '1', '2019-07-16 06:47:26'),
('Gajendra ', 'Kumar ', '9928588534', 'Gjchouhan1979@Gmail.com ', 'Mahilla ka bas sadri ', 'Personal Loan', 409, '9680893926', '30000', '', '2019-07-10 13:03:51'),
('Bodicherla', 'Siva prasad', '9581129470', 'bodicherlasivaprasad@gmail.com', '5-16, b c girls hostel street, sangam', 'Personal Loan', 410, '9441443084', '100000', '', '2019-07-10 13:05:31'),
('Sudalaieswaran ', 'Eswaran ', '9384744154 ', 'sudalaieswaraneswaran293@gmail.com', '57 PATHRA KALIAMMAN KOVIL STREET KOVILPATTI ', 'Personal Loan', 411, '9344810688 ', '10000', '', '2019-07-10 13:06:34'),
('Santosh', 'Darjee', '7602343053 ', 'santoshdarjee093@gmail.com', 'Siliguri ', 'Personal Loan', 412, '7602343053', '35000 ', '', '2019-07-10 13:09:19'),
('Srimannrayana ', 'Mortha ', '9640320791 ', 'Shivabujji4353@gmail.com ', 'Amalapuram ', 'Auto/Vehicle Loan', 413, '9177987395', '250000', '', '2019-07-10 13:10:36'),
('Ravinder ', 'Kumar ', '8607000880 ', 'Rvndrwagley57@gmail.com ', 'YamunaNagar ', 'Personal Loan', 414, '9728600064', '250000', '', '2019-07-10 13:13:14'),
('Pradip', 'Garodia', '9864256606', 'pkgarodia09@gmail.com', 'Golaghat, Assam', 'Personal Loan', 415, '8473908064', '200000,', '', '2019-07-10 13:13:42'),
('ANIL', 'PADWAL', '9209251023', 'anilpadwal33@gmail.com', 'At,post - malegaone ,Tel-Shahapur , Dist - Thane', 'Education Loan', 416, '7030222146', '40000', '1', '2019-07-16 05:49:28'),
('Tummala', 'Mahesh', '9591088359', 'Mahesh.chinn8899@gmail.com ', 'Sanjai gandhi colony, sk fast foods,', 'Personal Loan', 417, '9704499236', '1020000', '', '2019-07-10 13:28:19'),
('Bhupendra kumar', 'Bhavesh', '7050747195', 'ar1113547@gmail.com', 'Subhash colony ballabhgarh', 'Personal Loan', 418, '7050747195', '15000', '', '2019-07-10 13:35:16'),
('Showkat Ahmad', 'Mir', '9622811204', 'sameerkhan11540@gmail.com', 'Wandevalgam', 'Personal Loan', 419, '8899104914', '100000', '', '2019-07-10 13:40:33'),
('Sapnil', 'Sugunan', '9619245958', 'sapnilsugunan111@gmail.com', '266 Kamala niwas HSG SOCIETY GANESH NAGAR PANCHKUTIR POWAI MUMBAI 400076', 'Personal Loan', 420, '998720', '3000', '', '2019-07-10 13:43:25'),
('Kisanaram', 'Ram', '9043863310', 'kisanaram014@gmail.com', 'Satyapriya Compex Sitsra, ', 'Personal Loan', 421, '9043863310', '100000', '', '2019-07-10 13:55:05'),
('Dharmsing', 'Rathod', '9922655965', 'dharmsingrathod1@gmail.com', 'Devala.naik.tanda to.kinwat.ds.nanded, 9, 9', 'Personal Loan', 422, '9922655965', '3500', '', '2019-07-10 13:57:59'),
('Jonaid ', 'Ahmed', '7003391161', 'Sparklingjonaid@gmail.com ', '29/7 a giri babu lane', 'Personal Loan', 423, '9007420416', '100000', '', '2019-07-10 13:58:51'),
('Manveet', 'Bibra', '9872416020', 'star15175@gmail.com', 'H.no 1089 Sector 4 Panchkula Haryana', 'Personal Loan', 424, '8528050730', '200000', '', '2019-07-10 14:03:27'),
('Rotam', 'Lal', '8628959853', 'Rotamlal77@gmail.com', 'Rohru, Himachal Pradesh, India', 'Personal Loan', 425, '8894585436', '50000', '', '2019-07-10 14:06:59'),
('Vaghari', 'Dipakkumar prahladbhai', '9081576130', 'vagharidipakpooja@gmail.com', 'Jun aspa ..taluka..vadngar...dist ..Mehsana...384335', 'Personal Loan', 426, '9510169336', '5000', '', '2019-07-10 14:11:39'),
('Ekta ', 'Joshi ', '8126979389', 'ektajoahi40@gmail.com', 'Jeevanwala Fatehpur tanda Majri grant Haridwar dehradun highway Doiwala dehradun Uttarakhand ', 'Personal Loan', 427, '8445871140', '100000', '', '2019-07-10 14:14:11'),
('Pinky', 'Prasad', '8097376648', 'Prasad.pinki25@gmail.com', 'D 310, MARUTI APARTMENT NAVGHAR ROAD BHAYANDER EAST', 'Personal Loan', 428, '8169893242', '50000', '', '2019-07-10 14:20:22'),
('Jigneshkumar V', 'Kothari', '9725171707 ', '4everjignesh@gmail.com ', '301-sunrise, point, parivar char rasta, waghodia road, Vadodara 390025 Gujrat', 'Personal Loan', 429, '8160585479', '50000', '', '2019-07-10 14:22:35'),
('VIKASH', 'Sharma ', '6369751427', 'Vikashsharma62282@gemil.com ', 'No7 pvj nagar vanagram main road ambathur ', 'Personal Loan', 430, '9957262208', '50000', '', '2019-07-10 14:24:34'),
('Devangbhai', 'Raval', '8320282517', 'devangraval30@rediffmail.com', 'Amaranth Park Main Road ', 'Personal Loan', 431, '9428792006', '100000', '', '2019-07-10 14:25:18'),
('Nandhini', 'Nagaraj', '7639270382', 'nandimbatrichy@gmail.com', 'Sannadhi street poonamalle ', 'Personal Loan', 432, '9843441543', '25000', '', '2019-07-10 14:28:20'),
('Kishor', 'Motkar', '8806717141', 'Kishor.motkar137@gmail.com', 'Flat no 102, krishna kunj apartment, near iskon Temple, Akurdi Railway station, Pune', 'Personal Loan', 433, '9762344135', '5000', '', '2019-07-10 14:31:18'),
('Vishal', 'Rawat', '7570033492', 'vk5542307@gmail.com', 'Post Goila chinhut Lucknow', 'Personal Loan', 434, '07570033492', '100000', '', '2019-07-10 14:39:07'),
('Rohit', 'khurana', '8930887999', 'roifakhurana@gmail.com', '163 mohalla mashad', 'Personal Loan', 435, '8685886996', '50000', '', '2019-07-10 14:45:21'),
('Rajnandini', 'Nandeshwar', '9987620493', 'rajnandininandeshwar143123100@gmail.com', 'Thane', 'Personal Loan', 436, '8928636378', '5000', '', '2019-07-10 14:47:01'),
('Dillip ', 'Rana', '7325819953', 'dilliprana18@gmail.com', 'Village:kandhumunda,po:kathaumal,via:gaisilat,Distric:Bargarh,state:Odisha,pin:768037', 'Personal Loan', 437, '6371147691', '100000', '', '2019-07-10 14:51:46'),
('Tretiya ', 'Hirva', '9924206129', 'savantretiya24@gmail.com', 'Kothariya road Rajkot,Gujarat', 'Personal Loan', 438, '9924206129', '80000', '', '2019-07-10 14:59:27'),
('Pabitra', 'Sabar', '9418874556', 'www.pabitra@786gmail.com', 'Binnaguri Cantt', 'Personal Loan', 439, '9734700450', '100000', '', '2019-07-10 15:05:49'),
('Sarguna', 'Sarguna', '8754921916', 'niceguna555@gmail.com', '3/101 ad street keerampur namakkal ', 'Personal Loan', 440, '8489577258', '10000', '', '2019-07-10 15:13:43'),
('Ashok', 'nagar', '9829983838', 'ashoknagar466@hotmail.com', 'Village- Haripura, post - pipliya, teh- pachpahar, dist jhalawar pin code - 326501', 'Property Loan', 441, '9928233268', '10000000', '', '2019-07-10 15:17:41'),
('Rajeev ', 'Kumar ', '9805871778', 'Rajeevkumarsharmaraju968@gmail.com ', 'Baddi ', 'Personal Loan', 442, '7018493433', '50 000 ', '', '2019-07-10 15:24:44'),
('Vandana', 'Chauhan', '7302225624', 'mayanksahni42@gmail.com', 'Dehradun', 'Personal Loan', 443, '9634883223', '70000', '', '2019-07-10 15:25:25'),
('PULLELA ', 'ALEKHYA ', '9738258883', 'alekhyaswathi4@gmail.com', 'House no. -750, chowdeswari layout, near tulsi theatre, Marathalli,Bangalore, 560037', 'Personal Loan', 444, '9620128176', '70000', '', '2019-07-10 15:25:43'),
('M', 'Ashok kumar', '6309598219', 'Ashhu426@gmail.com', 'Hyderabad', 'Auto/Vehicle Loan', 445, '6305026631', '100000', '', '2019-07-10 15:28:43'),
('Satpal', 'Singh', '9315155456', 'Spm000630@gmail.com', '213 din nikas mohalla mandnaka hathin', 'Education Loan', 446, '8814977630', '50000', '', '2019-07-10 15:30:10'),
('Santosh', 'Wakle', '9325621548', 'santoshwakle92@gmail.com', 'plot.29. Ramakrishn Nagar peth Road Saptshrungi Mata Devi Mandir Mage Makhamalabad Nashik Maharashtra', 'Personal Loan', 447, '7769916941', '50000', '', '2019-07-10 15:37:10'),
('Takasenla', 'Imsong', '9378067852', 'asen.imsjamir94@gmail.com', 'Mokokchung District ,Nagaland', 'Personal Loan', 448, '9366459001', '90000', '', '2019-07-10 15:48:51'),
('Vivek', 'Jadhav', '8551054545', 'vj456vj@gmail.com', 'Chikhali,tal-haveli Dist-pune.', 'Personal Loan', 449, '9921491951', '100000', '', '2019-07-10 15:55:36'),
('Syed', 'Uvez', '9739800963', 'syeduvez1100@gmail.com', 'ajimir agency', 'Personal Loan', 450, '9900206532', '200000', '', '2019-07-10 15:55:56'),
('Dipshikha ', 'Doley', '9678503020', 'dipshikhadoley36@gmail.com', 'Dhakuakhana, Dist-Lakhimpur, pin-787055', 'Personal Loan', 451, '9101173545', '100000', '', '2019-07-10 15:59:44'),
('Lavanya ', 'Kamasani ', '9177829787', 'Lavanyakamasani219@gmail.com', 'Samskruthi college of engineering', 'Personal Loan', 452, '8971769787', '20000', '', '2019-07-10 16:19:39'),
('Thongam', 'Medini', '8974182136', 'medinithongam@gmail.com', 'Mongshangei awang leikai school leirak ', 'Personal Loan', 453, '8837044676', '50000', '', '2019-07-10 16:27:59'),
('Vivek', 'Joshi', '8218767129', 'mascot.joshi@gmail.com', 'Sai Vihar near sai mandir', 'Personal Loan', 454, '8218767129', '2000', '', '2019-07-10 17:14:18'),
('Beena ', 'Sharma ', '8171940557', 'rubyrudrapooja@gmail.com ', 'Shivasha estate colony satoha asgarpur mathura ', 'Personal Loan', 455, '8433429085', '100000', '', '2019-07-10 17:14:35'),
('JAYA RAJAGURU', 'PEETER', '8508887166', 'happygana329@gmail.com', 'W/o:rajaguru.no1/95,kularkadu.thanikkottagam.nagapattinam.tamilnadu.614716', 'Personal Loan', 456, '8508887166', '2000', '', '2019-07-10 17:16:37'),
('Manpreet kaur', 'Kaur', '8607406466', 'Nancybeauty37@gmail.com', 'Bcw surajpur haryana', 'Property Loan', 457, '8950661433', '500000', '', '2019-07-10 17:19:16'),
('P', 'Kamalesh', '9346222210', 'kamal0471@gmail.com', '130,srinivasapuram,renigunta road,tirupati,517501', 'Personal Loan', 458, '7095674720', '25000', '', '2019-07-10 17:23:25'),
('Dipankar ', 'Dolai ', '9083280990', 'dipankar1991dolai@gmail.com', 'Bhangabandh, Debra, Paschim Medinipur, 721211', 'Personal Loan', 459, '8617370314', '100000', '', '2019-07-10 17:26:12'),
('ASHOKKUMAR', 'SANMUGAM', '9080026885', 'ashokmeena2232@gmail.com', '3/366/1, ITMAPURAM, PERIYAMANJAVADI, PAPPIREDDIPATTI, DHARMAPURI', 'Personal Loan', 460, '9787288049', '10000', '', '2019-07-10 17:35:51'),
('Sajan singh', 'Sitoly', '7879584632', 'sajansitoliya105@gmail.com', 'Kannoad road malviya nagar ashta', 'Personal Loan', 461, '7879584632', '5000', '', '2019-07-10 17:36:05'),
('Vikas ', 'Verma', '9565257084', 'vv2129700@gmail.com', 'Indiranagar Lucknow', 'Personal Loan', 462, '9565257084', '100000', '', '2019-07-10 17:50:35'),
('Rahat', 'Ahmad', '9607266169', 'Kasba nanouta jila Saharanpur', 'Kasba nanouta jila Saharanpur up', 'Personal Loan', 463, '9607266169', '100000', '', '2019-07-10 17:51:56'),
('pranav', 'bansal', '9105828785', 'bansal.pranav996@gmail.com', 'muzaffarnagar', 'Personal Loan', 464, '9105828785', '150000', '', '2019-07-10 18:10:07'),
('Gulab chand', 'Nagar', '9754354147', 'Gulabnagar31@gmail.com', 'Odpur post bhatkhedi Dist Rajgarh ', 'Personal Loan', 465, '9754354147', '50000', '1', '2019-07-17 05:53:54'),
('Krishna', 'Nath', '7975007330', 'krishnakantanath1@gmail.com', 'Rampur Sorbhog Barpeta Assam', 'Personal Loan', 466, '8135826015', '150000', '', '2019-07-10 18:13:46'),
('Raj', 'Rani', '9465183616', 'bhupindersingh040491@gmail.com', 'Jammu Basti Ward no 2,street balwinder mc', 'Personal Loan', 467, '9465183616', '100000', '1', '2019-07-17 06:08:09'),
('Vishal', 'Patel', '9016225272', 'vishal@travelholictours.com', 'f16 patel nagar near saroli bridge surat olpad road jahangirpura', 'Personal Loan', 468, '09016225272', '15000', '1', '2019-07-17 05:54:24'),
('Chetan', 'Dholkia', '9427743166', 'chetansoni67587@gmail.com', 'Sanghvi sreet main bazar lathi dist Amreli', 'Personal Loan', 469, '9106494649', '200000', '1', '2019-07-17 05:55:36'),
('Ankush ', 'Sahu', '9506613549', 'ankushsahu87@gmail.com', '73/a 88 khuldabad allahabad uttar pardesh', 'Home Loan', 470, '7905820958', '50 lakh', '', '2019-07-10 18:26:32'),
('Naveed,Pasha', 'Naveed,Pasha', '9986636654', 'naveedpashanavredpaha@gmail.com', 'Bengaluru ru', 'Personal Loan', 471, '9986636654', '10,000', '1', '2019-07-17 05:53:39'),
('Sachin', 'hendre', '7558410400', 'sachinhendre101@gmail.com', 'pimple gurav', 'Personal Loan', 472, '7796538328', '25000', '', '2019-07-10 18:40:14'),
('Kadambari', 'hendre', '7796538328', 'Kadambarihendre@gmail.com', 'pimple gurav katepuram chowk vinayaknagar lane 2 sai apartment', 'Personal Loan', 473, '7558410400', '25000', '', '2019-07-10 18:47:37'),
('Mohd Khalid', 'Mohd Khalid', '8630585517', 'buckleworld@gmail.com', 'Sarai Rahman tar Wali Gali', 'Personal Loan', 474, '9897896254', '50000', '', '2019-07-10 21:41:12'),
('Vishal', 'Gupta', '8318304950', 'Gurshaiganj kannauj', 'Midway Nagar gurshaiganj kannauj up 209722', 'Personal Loan', 475, '8318304950', '200000', '', '2019-07-10 22:15:21'),
('Moumita Chatterjee ', 'Chatterjee ', '9875396643', 'Somnathhk@gmail.com', 'Bonopulak apparment jyangra bottola', 'Personal Loan', 476, '9748985898', '200000', '', '2019-07-10 22:29:54'),
('Pawan ', 'Nagar', '8053538744', 'pkn8053538744@gmail.com', 'D-926 Harinagar Palwal, Harinagar Palwal haryana', 'Personal Loan', 477, '9773507094', '50000', '', '2019-07-10 23:27:03'),
('Mousumi', 'Roy Chowdhury', '9038558258', 'mousumiroychowdhury925@gmail.com', '15 kashibati shitalatala lane po-makhla dist-hooghly', 'Personal Loan', 478, '8585894952', '5000', '', '2019-07-11 00:17:19'),
('Pushpa', 'Srinivasan', '9597135241', '9597135241s@gmail.com', '4/26 mariamman koil street', 'Personal Loan', 479, '9597135241', '25,000', '', '2019-07-11 00:21:11'),
('Tripuresh', 'Pathak', '9307538090', 'tripuresh.pathak100@gmail.com', '75A,76 Bhabha Nagar Sanigawa road Ramadevi Kanpur 208021', 'Personal Loan', 480, '9450569695', '210000', '1', '2019-07-17 07:53:34'),
('Nathanial', 'Franklin', '9872067849', 'nathanialfranklin099@gmail.com', 'House No 40/12', 'Personal Loan', 481, '7009382033', '100000', '', '2019-07-11 00:48:18'),
('Ramu', 'Mandloi', '9770486705', 'Titi', 'Titi', 'Personal Loan', 482, '88431820007436', 'Dj', '', '2019-07-11 00:53:21'),
('Gunna singh', 'Gunna singh', '7320905546', 'Jharkhand garhwa gram raro', 'Garhwa', 'Home Loan', 483, '7320905546', '100000', '', '2019-07-11 01:05:55'),
('Bashir Uddin', 'Ahmed', '9435301177', 'asim4uddin@gmail.com', 'Faizabad Cant, Faizabad Cantonment', 'Personal Loan', 484, '6393469666', '50', '', '2019-07-11 01:08:48'),
('Mahamed Dadapeer ', 'Dadapeeer ', '8095218310', 'Dadapeeermahamed@gmail.com', '414 D group employees layout 1th block ', 'Personal Loan', 485, '9113067441', '300000 ', '', '2019-07-11 01:19:21'),
('Sonu', 'Mavi', '8923624821', 'sonumavikaili@gmail.com', 'Kalli Ram pur Mera', 'Personal Loan', 486, '8923624821', '1473001700012147', '', '2019-07-11 01:39:47'),
('Bhanudas', 'Mane', '9665156355', 'bhanudasmane1982@gmail.com', 'Sanjay Nagar Masur Tal Karad Dist Satara, Sanjay Nagar Masur Tal Karad Dist Satara', 'Personal Loan', 487, '9665156355', '100000', '', '2019-07-11 01:54:07'),
('RAHUL', 'DWIVEDI', '8209063535', 'rahulrd35@gmail.com', '87A CHETAN NAGAR', 'Personal Loan', 488, '7024026177', '100000', '', '2019-07-11 01:59:30'),
('Santosh kumar', 'Santosh kumar', '6201708728', 'Santoshkumar052128@gmail.com', 'Kolkata', 'Personal Loan', 489, '20000', '20000', '', '2019-07-11 02:06:10'),
('Rajagopal. ', 'Gopal', '9544652859', 'deepuptply@gmail.com', 'Vezhathinkal chirayil, Avalookunnu. P. O, South aryad,  Alappuzha', 'Personal Loan', 490, '9544652859', '50000', '', '2019-07-11 02:16:39'),
('Bhajanlal ', 'Kushwah ', '8941818451', 'yoursingh6@gmail.com', 'Raipur khair aligarh ', 'Personal Loan', 491, '8941818451', '100000', '1', '2019-07-17 07:02:06'),
('Vadla', 'Chandrashekar', '970276006', 'Shankarchary692@gmail', 'Undekode college narva man dal mahabubnagar district in Telangana  pin code509353', 'Personal Loan', 492, '6300706218', '30000', '', '2019-07-11 02:34:09'),
('Ratnakar ', 'Agham', '9970516065', 'ratnakaragham@gmail.com', 'Ro n17 khanderao nagare mala trambak road pimpalgoan Bahula Nashik 422012', 'Personal Loan', 493, '8390099046', '40000', '', '2019-07-11 02:36:15'),
('Mari', 'Kani', '9952222935', 'pmarikani6274@gmail.com', 'West Pandian colony street', 'Personal Loan', 494, '8072406611', '100000', '', '2019-07-11 02:36:33'),
('Rakesh', 'kumar', '9729094071', 'Rakesh.mittal67@gmail.com', 'House no.214/a,gali chopra wali', 'Personal Loan', 495, '7015714059', '100000', '', '2019-07-11 02:44:03'),
('Mahendra', 'Kumar', '7903164609', 'mahendra01187@gmail.com', 'Plot No 553, Chira Chas', 'Personal Loan', 496, '6299150046', '5000', '', '2019-07-11 03:01:24'),
('Jatinder', 'Kumar ', '7625985082', 'Jatinderkumar5475@gmail.com', 'S/O: Dalvir Singh, jassian colny,jassian, CEntral post OFFICE,Ludhiana, Punjab, 141008', 'Personal Loan', 497, '8360783431', '50000', '', '2019-07-11 03:09:50'),
('Rakesh', 'Jawa', '9837001501', 'jawarakesh@protonmail.com', 'B730 Lajpat Nagar Moradabad', 'Personal Loan', 498, '9456893777', '100000', '', '2019-07-11 03:13:10'),
('Raviroshan ', 'Oraon', '6207857416', 'balumathjh@gmail.com', 'Vill- Rajwar ,Holang,Balumath', 'Education Loan', 499, '6207857416', '20000', '', '2019-07-11 03:17:14'),
('mamata', 'sahu', '8658219312', 'bhaghirathisagu66@gmil.com', 'bijepr  siepali    tengra', 'Auto/Vehicle Loan', 500, '8658219312', '60000', '', '2019-07-11 03:19:24'),
('Ajay', 'Ahuja', '8770690355', 'ajayahuja383@gmail.com', 'Near Shani mandir gudhiyari raipur ', 'Personal Loan', 501, '7772867598', '40000', '', '2019-07-11 03:19:42'),
('Akash ', 'Kumar', '8006930310 ', 'Ak0091670@gmail.com ', 'Vpo titauli dist shamli', 'Home Loan', 502, '8006930310 ', '3000000', '', '2019-07-11 03:24:54'),
('Anilkumar', 'Sahameer ', '9967274711', 'Anilmandawa83@yahoo.com', 'Village-Hunumanpura ,post bakara distc - Jhunjhunu , Rajasthan ', 'Home Loan', 503, '9967274711', '100000', '', '2019-07-11 03:28:18'),
('Nagin', 'Nat', '6354789737', 'naginnat05678@gmail.com', 'Rohaniya Banswara rajasthan', 'Personal Loan', 504, '6354789737', '50000', '', '2019-07-11 03:33:21'),
('Ujjval', 'Bhardwaj', '8923990584', 'ujjvalsharma0501@gmail.com', 'Bikaner', 'Personal Loan', 505, '9660030530', '150000', '', '2019-07-11 03:34:31'),
('Prudhvi', 'Vannekuti ', '7095245715', 'vannekutiprudhvi@gmail.com', 'Sheelanagar, mother enclave, opposite Lutheran church,flat 004', 'Personal Loan', 506, '6303662600', '25000', '', '2019-07-11 03:37:00'),
('Shanker', 'Ray', '9871784808', 'sahilrai160501@gmail.com', 'Rana park siraspur Delhi  110042', 'Personal Loan', 507, '09871784808', '500000', '', '2019-07-11 03:39:11'),
('PERVEZ', 'QUADIR', '9892031836', 'pervezmq@gmail.com', '38/A Ekbalpore Road Calcutta ', 'Personal Loan', 508, '8356862611', '500000', '', '2019-07-11 03:45:13'),
('Vinu bhai', 'Padhiyar', '8511288812', 'Swayam139@gmail.com', 'At post. Luna , ta .Padra ,Dust.Vadodara. Borvalu faliyu ', 'Personal Loan', 509, '8511288812', '400000', '', '2019-07-11 03:46:45'),
('Shubham', 'Uniyal', '8755048141', 'happyuniyal123@gmail.com', 'Annand nagar balawala dehradun', 'Personal Loan', 510, '9410178514', '10000', '', '2019-07-11 03:55:15'),
('Umesh', 'Singh', '6391842255', 'singhumesh532@gmail.com', 'C 15ashokpuram colony daffi Varanasi', 'Education Loan', 511, '8765405323', '15000', '', '2019-07-11 03:57:46'),
('Ashok', 'sharma', '7727885845', 'ashok.sharma5782@gmail.com', 'Rajasthan patrika jhalana dungri jaipur, E-5', 'Personal Loan', 512, '7727885845', '30000', '', '2019-07-11 04:08:00'),
('Sambhana', 'Rambabu', '9848185559', 'sambhana.rambabu@,Yahoo.com', '74-12/6-8', 'Personal Loan', 513, '8185962896', '50000', '', '2019-07-11 04:08:14'),
('DEVENDRA ', 'Shrivastava ', '8982010257', 'dev22shrivastava@gmail.com', '462010', 'Personal Loan', 514, '9340716732', '40000', '', '2019-07-11 04:28:34'),
('Vasantha ', 'Devi ', '9942565842 ', 'vijayvijay89601@gmail.com', 'Vasantha Devi w/o lakshmanan no 60 Vadamugam vellode  perundurai tk erode still pin 638112 ', 'Personal Loan', 515, '9965231626', '10000', '1', '2019-07-15 06:39:18'),
('Mohammed', 'Alam', '9867418710', 'Dularealam32@gmail.com', '119 room no. Naya Nagar sion mahim link Rd near raheja hospital mahim west 400016', 'Personal Loan', 516, '9372272194', '300000', '', '2019-07-11 04:35:13'),
('yatesh kumar ', 'agrawal', '9634284950', 'yatesh.agrawal.234@gmail.com', 'Radha krishna vatica, ganesh tila , jaisingh pura mathura', 'Personal Loan', 517, '6397496416', '100000', '', '2019-07-11 04:38:02'),
('Jamruddin', 'Siddiq', '8302905861', 'sahilbhaiahmd@gmail.com', 'Distik badau ', 'Personal Loan', 518, '8859668211', '50000', '1', '2019-07-15 06:48:06'),
('Nanjundaswamy D', 'Nanjundaswamy D', '9741171967', 'Nanjumanju@gmail.com', 'Arepura v, Gundlupet to, chamarajanagara Dist.. 571109', 'Personal Loan', 519, '9741171967', '50000', '', '2019-07-11 04:46:13'),
('Neha', 'Ahire', '8928824448', 'nehaahire143@gmail.com', 'Sector-19, Khoperkhairne, Plot No-001, Room No-0897', 'Personal Loan', 520, '8433502324', '50000', '1', '2019-07-15 06:41:06'),
('Rahul', 'Jain', '8980191173', 'Jain87876@gmail.com', 'Pipli bazar nawa city', 'Personal Loan', 521, '8239684093', '150000', '', '2019-07-11 05:01:50'),
('vijendra', 'singh', '9521556198', 'vijendrasinghbgru@gmail.com', 'vill-parwan post-parwan teh.-phagi, jaipur', 'Personal Loan', 522, '8000988227', '20000', '', '2019-07-11 05:04:51'),
('amit', 'ghodke', '9421662911', 'amit302001@gmail.com', 'laxminagari apt flat no 5 neminath nagar vishrambag sangli', 'Personal Loan', 523, '9284640047', '50000', '', '2019-07-11 05:06:43'),
('?????? ????? ', '  sundrchhodri ', '9111979348', 'Ujjean ', '????????????', 'Personal Loan', 524, '9111979348', '200000', '1', '2019-07-16 06:42:30'),
('Mohmmad', 'Idresi', '8356085361', 'kalamahamd0786@gmail.com', 'Sir jj marg duncun road400008', 'Personal Loan', 525, '7408286549', '20000', '', '2019-07-11 05:21:10'),
('Mangam.sivaramakrishna', 'm.siva', '9014020683', 'Sivam6884@gmail.com', 'mangam.sivaramakrishna.s/omadhavarao.torredu(post).Rajahmundryrural.egdt.pin533293', 'Auto/Vehicle Loan', 526, '9966206073', '2,00000', '1', '2019-07-15 06:41:56'),
('soorya', 'kumar', '9048846237', 'sooryakumarais121@gmail.com', 'manchumala vandiperiyar panchayat', 'Personal Loan', 527, '9048846237', '10000', '', '2019-07-11 05:26:59'),
('Mukeem', 'Khan', '7798515558', 'mukeemkhan9561@gmail.com', 'Makhdoom ashraf nagar opp verna electronic city cortalim goa', 'Personal Loan', 528, '07798515558', '200000', '', '2019-07-11 05:29:39'),
('Rushikesh ', 'Gawai', '9145289450', 'rushikeshgawai71@gmail.com', 'At.gourkheda pos.chandikapur ttq.daryapur dist.amravti', 'Personal Loan', 529, '9145289450', '5000', '', '2019-07-11 05:31:08'),
('Padama ', 'Devi', '7525932346', 'aniketprajapatistp11@gmail.com', 'Chungi choki sitapur, Munsigang road chungi choki', 'Personal Loan', 530, '07525932346', '5000', '', '2019-07-11 05:32:07'),
('Ninga', 'Nna', '7619335914', 'ninganna32487@gmail.com', 'Kammagondanahalli', 'Personal Loan', 531, '9901683190', '15000', '', '2019-07-11 05:40:26'),
('SUBHASHIS', 'NATH', '6000791464', 'subhashisnath169@gmail.com', 'Vill_ Bhangarpar House no _221 street s.k road bhangarpar p.o_ bhangarpar', 'Personal Loan', 532, '7636841765', '5000', '', '2019-07-11 06:02:24'),
('Ashraf', 'Khan', '7668533168', 'Khanhena887@gmail.com', '129/111shahganj', 'Personal Loan', 533, '7905196442', '500000', '', '2019-07-11 06:04:14'),
('Prashant', 'pradhan', '9049126916', 'priyaprashantenterpriese@gamil.com', 'Housing board colony flat no m90  blud no 15', 'Home Loan', 534, '9049126916', '500000', '', '2019-07-11 06:05:05'),
('Settipalli', 'Saroja', '9177225057', 'Settipallinagendra@gmail.com', '13-121.veerabadhranagar.challlavaripalem.ankireddypalem.rural.guntur.522005.', 'Personal Loan', 535, '9177225057', '50000', '', '2019-07-11 06:22:57'),
('Satendra ', 'kumar ', '6393148487', 'kumarsatendra0732@gmail.com', 'Gram Jamahura post Sikandrabod district Lakhimpur Kheri satet Uttar Pradesh ', 'Personal Loan', 536, '6393148487', '1000000', '', '2019-07-11 06:25:11'),
('Pankaj ', 'Kumar ', '8931857685', 'Pankajvns001@gmail.com', 'Home no, 3/276 rampur ramnagar Varanasi ', 'Personal Loan', 537, '8858415331', '75000', '', '2019-07-11 06:25:39'),
('juraij ', 'mulla', 'juraij3713@gmail.com', 'juraij3713@gmail.com', 'Chouthai galli bhairavnath mandir koregaon', 'Personal Loan', 538, '9637171713', '50000', '', '2019-07-11 06:26:09'),
('Vanitha', 'Somu', '9972958469', 'vanithabhoomika@gmail.com', 'E 20 hampapura manchnaykanahalli ramanagar bangalore', 'Personal Loan', 539, '7618747805', '400000', '', '2019-07-11 06:29:25'),
('BAIRAGI', 'SWAIN', '9658514258', 'bairagi9990bs@gmail.com', 'Nehru bangala sector 21 Paradeep', 'Personal Loan', 540, '9438323638', '300000.00', '', '2019-07-11 06:32:26'),
('Savalla', 'Srikanth', '9676184063', 'srikant.savalla@gmail.com', '3-3-211 sawaran street karimnagar telangana', 'Personal Loan', 541, '6303357776', '50000', '', '2019-07-11 06:34:47'),
('Ganga ', 'Ram', '8094220469', 'Sainiganga1997@gmail.com', 'Bijwari, bhusawar, bharatpur, rajasthan', 'Personal Loan', 542, '8094220469', '50000', '', '2019-07-11 06:34:47'),
('randeep', 'kour', '9779009010', 'randeepkour1993@gmail.com', 'amloh fatehgarh sahib Punjab', 'Personal Loan', 543, '8284810052', '20000', '1', '2019-07-16 06:43:20'),
('Hiren', 'Kansara', '9328053569', 'hetanshikansara1712@gmail.com', 'A404 sanand heights sanand', 'Personal Loan', 544, '7990179903', '700000', '', '2019-07-11 06:39:14'),
('Dharamraj ', 'Chaudhari ', '9549431664', 'dharamrajChaudhari542@Gmail.com', 'Saroli mod dooni tonk rajasthan ', 'Home Loan', 545, '9549431664', '800000', '', '2019-07-11 06:43:54'),
('Jitendra', 'Mohan ', '8393801231', 'Jitendramohan12@gmail.Com', 'Tehir ', 'Personal Loan', 546, '8393801231', '80,000', '', '2019-07-11 07:10:08'),
('ZAFAR', 'IQBAL', '7753033040', 'zafarhar89@gmail.com', 'Kapish vihar colony uttardhauna faizabad road Lucknow', 'Personal Loan', 547, '9839848976', '100000', '', '2019-07-11 07:12:22'),
('Anamika', 'Das', '8902947838', 'anamikad582@gmail.com', '6A Deshapriya park East sukriti apt ', 'Education Loan', 548, '8902947838', '50000', '1', '2019-07-16 06:49:25'),
('jeedigunta', 'sathishbabu', '8978233085 ', 'check ', '19-1-352-2a.vinayakanagar.oldtown.anantapurandhrapradesh', 'Personal Loan', 549, '9000171239', '30000', '', '2019-07-11 07:19:17'),
('SANTHANAM', 'SAN THANAM', '8870318112', 'spreety845@gmail.com', '39, N. G. O COLONY, 6TH STREET, 1ST EXTENTION', 'Personal Loan', 550, '08870318112', '10000', '', '2019-07-11 07:26:52'),
('Anmolpreet ', 'Kaur ', '9855476103', 'anmolpreetkaur2121@gmail.com', 'V.p.o-Lakhan ke Padda city KAPURTHALA ', 'Education Loan', 551, '9855476103', '60000', '', '2019-07-11 07:28:59'),
('Mohit', 'Chhabra', '9026441598', 'mohitchhabra19821@gmail.com', '113/1 chander nagar alambagh Lucknow', 'Personal Loan', 552, '9696673075', '50000', '1', '2019-07-16 06:43:02'),
('Sayyad', 'Husein', '9413773048', 'Sayyadpathan4@gmail.com', 'Sipahiya ka Mohalla Devgarh', 'Personal Loan', 553, '9413773048', '20000', '', '2019-07-11 07:43:32'),
('Tummalapalli', 'Veera Venkata Satya Madhava rao', '8185805769', 'casinova.madhu@gmail.com', 'Yanam', 'Personal Loan', 554, '7995270789', '10000', '', '2019-07-11 07:50:45'),
('Diptanu ', 'Deb', '9774222794', 'diptanudeb012@gmail.com ', 'Unnayan sangha maddha para', 'Personal Loan', 555, '9774222794 ', 'Five lack only', '', '2019-07-11 07:58:06'),
('Kanhiya', 'Lal', '9555244848', 'kanhiyalaldl12@gmail.com', 'Khandsa', 'Property Loan', 556, '9555965645', '200000', '', '2019-07-11 08:09:38'),
('A latha', 'Latha', '9032775348', 'Abalaji12345678@gmail.com', 'Police line street', 'Education Loan', 557, '09032775348', '50000', '', '2019-07-11 08:12:18'),
('yogesh', 'singh', '7014388175', 'ysdaiya@gmail.com', 'ramdev temple behind gali no. 2 suthala jodhpur', 'Personal Loan', 558, '8502904553', '20000', '', '2019-07-11 08:21:42'),
('Rakesh', 'Dayabhai', '8980280863', 'rathodrakesh482@gmail.com', '46,ranuja dham soc,puna Bombay market road surat', 'Personal Loan', 559, '7046241203', '10000', '', '2019-07-11 08:23:23'),
('Utpal', 'Bora', '+448474086919', 'm.utpalbora111@gmail.com', 'Barpathori sutar gaon', 'Personal Loan', 560, '9957994824', '150000', '', '2019-07-11 08:26:16'),
('Akash', 'Mishra', '7219439559', 'akashmishra402@gmail.com', 'Ramabai Tiwari,,plot no-114 khadgaon road Vikas Nagar wadi Nagpur', 'Personal Loan', 561, '72194395599', '200000', '1', '2019-07-16 06:44:24');
INSERT INTO `enquiry_master` (`fname`, `lname`, `mobile`, `email`, `address`, `loan_type`, `trial_id`, `alt_mobile`, `amount`, `status`, `entry_date`) VALUES
('Ajay', 'Koli', '8087858021', 'ajaykoli5919@gmail.com', 'Sr no 71 shrinath nagar colony no 2 ghorpadigaon pune', 'Personal Loan', 562, '6364897452', '70000', '', '2019-07-11 08:33:32'),
('Gandavarapu', 'Bharat', '9182362389', 'firesafety963@gmail.com', '20-513,pallimitta,mulapeta,, Nellore -524003', 'Personal Loan', 563, '7093521332', '20000', '1', '2019-07-16 06:48:53'),
('Labchandra reang', 'Reang labchandra', '9862070736', 'Labchandra reang dist- santir barzar south tripura', 'Vill- najirai para kalashi mukh dist- santir barzar south tripura', 'Home Loan', 564, '9862070736', '200000', '', '2019-07-11 08:42:06'),
('Raju ', 'Mondal ', '8017518115', 'raju.benchmark@gmail.com ', '28/3 Khanpur Sahid Nagar Naktala Kolkata -700047', 'Personal Loan', 565, '8017518111', '1000000', '', '2019-07-11 08:56:58'),
('Pampana lakshmi', 'Kantham', '9985448067', 'ramlucky9d@gmail', 'Vijayawada', 'Personal Loan', 566, '9121705003', '50000', '', '2019-07-11 09:02:01'),
('JAGANNATH', 'PAL', '8944040461', 'jagannathpalmi@gmail.com', 'Kamarda bazaar,khejuri,purba medinipur.', 'Auto/Vehicle Loan', 567, '9126614588', '100000', '', '2019-07-11 09:16:30'),
('Vijay', 'Jaiswal', '8820154033', 'vijayjaiswal82074@gmail.com', '62b/p j. N mukherjee road ghusuri howrah', 'Personal Loan', 568, '8820154033', '100000', '', '2019-07-11 09:19:39'),
('Bhawan', 'Jha', '7834910060', 'bhawanjha139@gmial.com', 'Wz  746/6 Dhani Vill Palm, Corier', 'Personal Loan', 569, '7834910060', '60000', '', '2019-07-11 09:19:52'),
('Amit', 'Singh', '8793243370', 'as392438@gmail.com', 'Nagpur ', 'Personal Loan', 570, '8793243370', '60000', '', '2019-07-11 09:21:01'),
('Firoj Musa Tadavi', 'Tadavi ', '8888227154', 'firojtadavi28@gmail.com', 'Savkhede sim tal yawal dist jalgaon', 'Personal Loan', 571, '8806388118', '100000', '', '2019-07-11 09:22:05'),
('Tokato', 'Awomi', '8416054451', 'awomytokato@gmail.com', 'Phuye Old Village', 'Personal Loan', 572, '9366624082', '50000', '', '2019-07-11 09:25:02'),
('Raj', 'Kumar', '9760866107', 'Rajkumarkarhal@gmail.com', 'VILL bhidahar post karhal mainpuri', 'Property Loan', 573, '7830208789', '50000', '', '2019-07-11 09:31:07'),
('Pawan Kumar', 'Sharms', '9467802611', 'pawantiwari@010879gmal.com', 'Mandi adampur', 'Personal Loan', 574, '9467802611', '200000', '', '2019-07-11 09:37:06'),
('Jagtar', 'singh', '9478501074', 'jagtarsingh6134@gmail.com', 'ho no17sura colony po sura nussi', 'Personal Loan', 575, '9478501074', 'Bisnos', '', '2019-07-11 09:41:55'),
('Badrinath', 'Kuber', '8329467728', 'badrikuber@gmail.com', 'N-11 C-1/9 deepnagar hudco Aurangabad', 'Personal Loan', 576, '7558757997', '50000', '', '2019-07-11 09:46:21'),
('Ramkrishn', 'Rawat', '9522230729', 'Ramkrishnrawat @gmail com', 'Villge bigaudi', 'Personal Loan', 577, 'Al', 'Buisnis', '', '2019-07-11 09:52:05'),
('Neeraj', 'aggarwal', '9817150054', 'world123famous@gmail.com', 'Ecomart near research aid palampur.distt kangra himachal pardesh', 'Personal Loan', 578, '9736022555', '100000', '', '2019-07-11 09:55:32'),
('Manju', 'Sharma', '7889756375', 'panditsarasshastri123@gmail.com', 'Manju Sharma  s/o babu ram r/o garan pain tec kalakote Dec rajouri satat Jammu and Kashmir', 'Personal Loan', 579, '07889756375', '400000', '', '2019-07-11 10:02:31'),
('David', 'Rai', '8207223691', 'davidraichamling@gmail.com', 'Kalimpong', 'Personal Loan', 580, '8310907787', '20000', '', '2019-07-11 10:08:14'),
('Lingaraju', 'Muniyappa', '9606773552', 'lingarajukish7180@gmail.com', 'No 87, abbigere Kalony, behind mariyamma temple, chikkabanavara post, Bangalore', 'Personal Loan', 581, '9738807180', '50000', '', '2019-07-11 10:13:33'),
('Jitender ', 'Kumar', '8210532888', 'jituramro34@gmail.com', 'Rims campus b block qr.no-01 bariyatu ranchi jharkhand', 'Personal Loan', 582, '8986652210', '20000', '', '2019-07-11 10:15:11'),
('Suraj Kumar', 'Maravi', '6260552776', 'SSurajkumarMaravi@gmail.com', 'Karnala Shivpur Korba', 'Personal Loan', 583, '9575035174', '2000', '', '2019-07-11 10:15:43'),
('KN Somanadh', 'Narayanan', '7025534101', 'Somanadhkanjar002@gmail.com', 'KN Somanadh  Karukappallil Kanjar Thodupuzha', 'Personal Loan', 584, 'no', '25000', '', '2019-07-11 10:17:49'),
('Bakki', 'Kiran kumar', '9951062619', 'kirankumarhitech@gmail.com ', '1-39 jaibeem peta tallarevu ', 'Personal Loan', 585, '9703102627', '50000', '', '2019-07-11 10:18:56'),
('sonal', 'hadse', '9819979215', 'sonalmore786@gmail.com', 'mumbai', 'Personal Loan', 586, '9594964178', '300000', '', '2019-07-11 10:37:15'),
('Bhikulal', 'Pawar', '7218293396', 'bhikulalpawar@gmail.com', 'Nanded, Shivneri Nagar Sarkari Grah Nirman,Airport Sangvi bk,', 'Personal Loan', 587, '8830683917', '30000', '', '2019-07-11 10:44:29'),
('Neelam ', 'Mukati ', '9584654918', 'neelam25mukati@gmail.com', '3086 patti Bazar mhow 453441 Distt Indore Madhya Pradesh ', 'Personal Loan', 588, '8319800379 ', '80000', '', '2019-07-11 10:52:48'),
('Palak', 'Khan', '9084419505', 'palakkhanhdr12345@gmail.com', 'Shanker Aashram, Jwalapur', 'Personal Loan', 589, '7351821130', '100000', '', '2019-07-11 10:53:07'),
('Anayatullah', 'Naik', '7051174342', 'anayatullahsubhani18@gmail.com', 'ARIPINCHLLA, Tehsil KHARI', 'Personal Loan', 590, '07051174342', '3000', '', '2019-07-11 11:08:57'),
('Rahul', 'Rahul', '9643583626', 'singhrahul44888@gmail.com', '26, Girdharpur mathura', 'Personal Loan', 591, '7466963934', '150000', '', '2019-07-11 11:13:23'),
('Ranjeet Kumar ', 'Kumar ', '6393124080', 'rranjeetkashyap39421@gmail.om', 'Kharan purva majra Chkoti kalan ', 'Home Loan', 592, '6393134080', '500000', '', '2019-07-11 11:13:43'),
('Raj kamal', 'G', '8970495190', 'rajkamalrajkamal950@Gmail.com', 'No 1 12th cross pillaganahalli gottikere ', 'Personal Loan', 593, '8553083754', '50000', '', '2019-07-11 11:14:21'),
('Krrish', 'Akhauri', '9065540992', 'vishesh0105@gmail.com', 'Ward no 20 Rajeev nagar Patna Bihar', 'Personal Loan', 594, '7718957983', '10000', '', '2019-07-11 11:21:51'),
('Chetan', 'Gupta', '7240128587', 'Lavigupta557@gmail.com', 'Rastogi Bali sugar Bali ', 'Personal Loan', 595, '7240128587', '100000', '', '2019-07-11 11:22:05'),
('Umesh', 'Yede', '9834232716', 'Umeshyede267@', 'Ratndeep vasahat galli no 5,,kabnur,,Kolhapur,,', 'Personal Loan', 596, '9545620969', '15000', '', '2019-07-11 11:27:46'),
('Vikash', 'Jangra', '8529364277', 'Vikashjangra00000@gmail.com', 'Vpo barwa techil siwani disst bhiwani', 'Personal Loan', 597, '9671376926', '50000', '', '2019-07-11 11:30:16'),
('Vijayan', 'M', '9444312558', 'mvsriads@gmail.com', '39/24 Vinayagapuram 1 St Street Arumbakkam', 'Personal Loan', 598, '9840988859', '200000', '', '2019-07-11 11:35:16'),
('Anurag', 'Tiwari', '9305888007', 'anurag.neer@gmail.com', 'B 34/140 A5 B2, GAYATRI NAGAR, SHUKLAPURA, SARAINANDAN, VARANASI-221010', 'Personal Loan', 599, '9005525553', '100000', '', '2019-07-11 11:41:02'),
('RAKESH', 'NEGI', '9478141048', 'rakeshmohra88@gmail.com', 'Indira nager lucknow', 'Personal Loan', 600, '07986714645', '7986714645', '', '2019-07-11 11:41:58'),
('Suparna', 'Sardar', '7596910612', 'Sardarsuparna158@gmail.com', '7,bhabananda road kalighat,kolkata,west Bengal ', 'Personal Loan', 601, '9330205047', '50000', '', '2019-07-11 11:45:01'),
('Raghunatha', 'Thippeswamy', '9902947154', 'raghunath0700@gmail.com', '152 Natesh building near shaneshwara temple Thirupalya Bommasandra  Bangalore560099', 'Personal Loan', 602, '9945634554', '600000', '', '2019-07-11 11:47:08'),
('Debakanta ', 'Behera ', '7008135197 ', 'dipu1234goutam@gmail.com', 'Bhubeneswar, ORISSA ', 'Personal Loan', 603, '9937569827 ', '50000', '', '2019-07-11 11:48:13'),
('Dinesh Kumar ', 'Shukla', '7380591821', 'golushuklaji@gmail.com', 'Vill/post behta bujurge kanpur nagar', 'Education Loan', 604, '8009926051', '1000000', '', '2019-07-11 11:51:36'),
('Abdull ', 'Hakim', '9982847354', 'Anajir844@gmail.com ', 'Charnye siwandi', 'Personal Loan', 605, '8890062358', '30000', '', '2019-07-11 11:56:14'),
('Madhusudan', 'Kadam', '7620228028', 'kadammadhu815@gmail.com', 'Katarni', 'Personal Loan', 606, '7620228028', '100000', '', '2019-07-11 12:02:04'),
('Azam', 'Bhatti', '9814052045', 'bhattiazam7@gmail.com', 'Mohalla ilyas khan in.side delhi gate malerkotla', 'Personal Loan', 607, '7888383718', '100000', '', '2019-07-11 12:06:06'),
('SHAIKH EJAAZ', 'PAPPANKHAN', '7621929343', 'ajazkhan56372@gmail.com', 'Juhapura ambar tawar pas Kadri party plot se street sawera tea, A one nagar', 'Personal Loan', 608, '9898805873', '500000', '', '2019-07-11 12:06:55'),
('Mohd', 'Sajid', '8076101795', 'sajid98100@gmail.com', 'Rqz-13 NIHAL VIHAR NANGLOI Delhi 110041', 'Personal Loan', 609, '7503497176', '200000', '', '2019-07-11 12:11:29'),
('USHA', 'USHA', '6377840975', 'rrajk8058@gmail.com', '626455064028', 'Education Loan', 610, '6377840975', '100000', '', '2019-07-11 12:21:34'),
('Jyoti', 'Patil', '7083962897', 'Jyotipatil708396@gmail.com', 'Chinmaya residency sadashiv nagar belgaum karnataka', 'Personal Loan', 611, '7083962897', '20000', '', '2019-07-11 12:21:44'),
('Gowhar ', 'Manzoor ', '7006045758', 'Gowharmanzoor50@gmail.com ', 'Srinagar jammu and kashmir ', 'Personal Loan', 612, '9086373283', '30000', '', '2019-07-11 12:24:27'),
('GURUSIDDAPPA', 'HONNALLI', '9481280119', 'guru.honnalli@rediff.com', '63 ADARSHNAGAR,HUBLI-580032 KARNATAKA', 'Personal Loan', 613, '9481280119', '200000', '', '2019-07-11 12:24:30'),
('Biswajit', 'Chakraborty', '8101992094', 'bchakraborty311@gmail.com', 'Khanakul  Ghaghorpur Hooghly', 'Home Loan', 614, '+918101992094', '300000', '', '2019-07-11 12:25:32'),
('Abhishek', 'Bhat', '09916133253', 'bhattun@gmail.com', '1475 Hebbal.Mysore .karnataka ', 'Personal Loan', 615, '9742779858', '500000', '', '2019-07-11 12:27:15'),
('SAMPATH ', 'KUMAR', '9848733549', 'rvsk1999@gmail.com', 'Gudur manda,l Nellore,AP', 'Personal Loan', 616, '9494288549', '300000', '', '2019-07-11 12:29:49'),
('Chinna Mariya', 'Selvam', '9344480457', 'maryashylu1993@gmail.com', '70/1, RG Nagar, Rathnapuri', 'Personal Loan', 617, '9487558663', '2000', '', '2019-07-11 12:30:27'),
('Janagi', 'Elumalai', '8220038265', 'elumalaisamy19@gmail.com', '110 east street. c pudupettai. ', 'Personal Loan', 618, '9080078362', '20000', '', '2019-07-11 12:37:13'),
('Rishit ', 'Marsonia', '7405086188 ', 'Rishitmarsonia007@gmail.com', '150 feet ring road rajkot ', 'Personal Loan', 619, '7405086188', '100000', '', '2019-07-11 12:40:30'),
('rakesh ', 'boruah', '8638893212', 'rinkuboruah40@gmail.com', 'diphu', 'Personal Loan', 620, '8472024478', '20000', '', '2019-07-11 12:52:34'),
('Chandan', 'Panda', '8480635495', 'Pandachandan59@yahoo.co.', 'Udala mayurbhanj Odisha ', 'Home Loan', 621, '9437238850', '8000000', '', '2019-07-11 12:56:02'),
('Gagandeep', 'Juneja', '8566889922', 'Juneja_lovely13@yahoo.co.in', 'Block-23,447 new Kidwai nagar.  Ludhiana. Punjab', 'Personal Loan', 622, '9781781878', '200000', '', '2019-07-11 12:57:25'),
('Rajvinder', 'Kaur', '9877303295', 'rajvinderkaur76824@gmail.com', 'H.no.262b green avenue kala sangia road Jalandhar', 'Personal Loan', 623, 'No', '180000', '', '2019-07-11 12:57:42'),
('Sukhwinder', 'Singh', '9781727300', 'sukhwinder@emil.com', 'Vpo.rachhin.dit.ludhiana.tec.raikot', 'Home Loan', 624, '9781727300', '10.00000', '', '2019-07-11 12:58:27'),
('Arvind', 'Arvind', '9050316945', 'arvindbhagwanpur01@gmail.com', 'Bhagwanpur, Radaur, Radaur', 'Personal Loan', 625, '8708955005', '50000', '', '2019-07-11 13:07:24'),
('sandeep ', 'kumar', '9588345756', 'sk0343566666@gmail.com ', 'Butana kundu gohana sonipat haryana', 'Education Loan', 626, ',9588345756', '200000', '', '2019-07-11 13:12:39'),
('Nur', 'Islam', '9957784342', 'islamnur445@gmail.com', 'Vill.... Sonapur Baligate, Dist... Kamrup Metro, State.... Assam, Pin.... 782402', 'Personal Loan', 627, '7002723463', '100000', '', '2019-07-11 13:14:53'),
('Srimonta', 'das', '8906136101', 'srimontadas3@gmail.com', 'Westbengal Burdwan, Durgapur CMERI/ soner tori J /16', 'Personal Loan', 628, '9614878533', '50000', '', '2019-07-11 13:15:33'),
('Nipun', 'Ahlawat', '7906945060', 'nipun.ahlawat015@gmail.com', 'D18 balwant enclave roorkee road meerut', 'Personal Loan', 629, '8899309247', '5000', '', '2019-07-11 13:23:31'),
('abhinav', 'Kumar', '9006842927', 'abhinavgolu107@gmail.com', 'raghunathpur, motihari ,bihar, india', 'Personal Loan', 630, '9006842927', '14000', '', '2019-07-11 13:23:46'),
('Utakrashth ', 'Kant', '7248303401', 'kantutakrashth@gmail.com', 'Vill Bighepur Post Ballapur Disterct Auraiya State UP Pincode 206121', 'Personal Loan', 631, '7248303401', '150000', '', '2019-07-11 13:25:42'),
('Pandav', 'Alpesh', '8000124738', 'alpeshpandav1992@gmail.com', '401/Krushnanayn Complex Shree Raam Nagar Rashi Cirucal Katargam', 'Personal Loan', 632, '08000124738', '200000', '', '2019-07-11 13:30:58'),
('MOHAN LALU D', 'DIVAKARAN ', '9605395076', 'mohanlalud@gmail.com', 'INFANT JESUS COTTAGE MANAKKARA, SASTHAMCOTTA PO KOLLAM KERALA 690621', 'Personal Loan', 633, '9605395076', '200000', '', '2019-07-11 13:31:31'),
('Durgesh ', 'Maurya ', '9795290426', 'Durgeshmaurya31@gmail.com', 'janaura pakka talab faizabad ', 'Education Loan', 634, '9695070414', '50000', '', '2019-07-11 13:32:18'),
('Balbir Singh ', 'Balbir', '9813037789', 'Balbir kharal 73@Gmail. Com ', 'Vill.kharal  pos.badsui t.guhala  dis. Kaithal  pin.136034', 'Home Loan', 635, '9813037789', '50,000', '', '2019-07-11 13:34:03'),
('Sankar', 'Saha', '7005515713', 'sankarsaha087@gmail.com', 'Agartala', 'Personal Loan', 636, '8415061503', '50000', '', '2019-07-11 13:34:16'),
('Alok ', 'Tiwari', '9936065849', 'aloktiwari875@gmail.com', '226023', 'Personal Loan', 637, '8354073277', '10000', '', '2019-07-11 13:37:39'),
('Hemant ', 'VAISHNAV ', '7016037542', 'Hemantpooja25@gmail.com', '301,302 chamunda nagar society vejalpur ', 'Personal Loan', 638, '8980575878', '10000', '', '2019-07-11 13:40:17'),
('Pavan', 'Dodda', '6281574061', 'hsherlock05@gmail.com', 'Sri rama giri by pass road near rto office', 'Education Loan', 639, '8179754908', '10000', '', '2019-07-11 13:40:48'),
('Karunajothi ', 'Kannan', '9629681167 ', 'Karunajothi.15@gmail.com', 'No 27 pakkumarasalai devanampattinam cuddalore ', 'Personal Loan', 640, '9790608137', '5000', '', '2019-07-11 13:45:21'),
('Boddepalli Anil', 'Kumar', '9121688763 ', 'anilgr8t@gmail.com', 'Dlf madien heights tower c-51 5th floor ', 'Personal Loan', 641, '7483285092', '25000', '', '2019-07-11 13:48:18'),
('Venkatesh jn', 'Venkatesh jn', '9740025733', 'Venkatesh jn8@gmailcom', 'Chikkaballapur', 'Personal Loan', 642, '9148060325', '100000', '', '2019-07-11 13:48:47'),
('Nayyum', 'Pathan', '7020733739', 'Nayyumk2@gamil.com', 'Bodkin Taluka  paithan    distric aurangabad mahashtra', 'Education Loan', 643, '9604462394', '100000', '', '2019-07-11 13:51:49'),
('RAJENDRA', 'Bhardwaj', '9672779963', 'rajendrabhardwaj322@gmail.com', 'C53 , shastri nagar bhilwara', 'Personal Loan', 644, '8107057123', '500000', '', '2019-07-11 13:57:17'),
('Govind', 'Bhandari', '9624700181', 'gopib822@gmail.com', 'Raman bhai ki building room no 03 amli near gayatri temple silvassa ', 'Personal Loan', 645, '9724451266', '150000', '', '2019-07-11 14:03:23'),
('shajitha', 'banu', '9566698170', 'shajithabanu59417@gmail.com', '476, mosque street, venkatesapuram', 'Personal Loan', 646, '8807816139', '65000', '', '2019-07-11 14:03:25'),
('Sagar', 'Dixit', '9689001805', 'sagardixit8547@gmail.com', 'Ap ladegaon tal khatav dist Satara Maharashtra', 'Personal Loan', 647, '7350318364', '50000', '', '2019-07-11 14:03:38'),
('Priyankar', 'Goon', '9051387850', 'priyo.rox@gmail.com', 'Madhyamgram', 'Personal Loan', 648, '9263308872', '100000', '', '2019-07-11 14:04:59'),
('rushikesh', 'patil', '8488030910', 'rushikesh.patil20691@gmail.com', 'Pandvai Sugar Colony, Bharuch', 'Personal Loan', 649, '9054309790', '50000', '', '2019-07-11 14:07:59'),
('Ritesh', 'Patre', '8962201837', 'riteshkumarpatre95@gmail.com', 'Tikrapara dayalband bilaspur', 'Personal Loan', 650, '7898323158', '500000', '', '2019-07-11 14:12:31'),
('Anil', 'Dongre', '6268677177', 'anildongre694@gmail.com', 'Tansaramal umranala', 'Personal Loan', 651, '6268677177', '460000', '', '2019-07-11 14:12:55'),
('Varanasi', 'Shiddu', '8639555268', '1981shiddu@gmail.com', 'Shiddu Dr no 10/08/04 kashpa street markeat road', 'Personal Loan', 652, '8341299835', '20000', '', '2019-07-11 14:17:13'),
('ARVIND KUMAR', 'MAGARDE', '9584562040', 'arvind.magrde@rediffmail.com', 'tyfd 43a near railway raning room, amla', 'Personal Loan', 653, '8770998769', '50000', '', '2019-07-11 14:20:49'),
('vrushant', 'shah', '8655789101', 'vrushants3@gmail.com', 's/o deepak shah chikle building ganesh nagar tilak chauk kalyan west thane maharashtra', 'Personal Loan', 654, '9326615739', '10000', '', '2019-07-11 14:22:05'),
('Sujit Kumar ', 'Nayak ', '9178982460', 'ajitnayakkumar4511@gmil.com', 'At/po katharagada so kishor chandra nayak near_shiv mandir gouda Street dist_koraput odisha', 'Education Loan', 655, '09178982460', '500000', '', '2019-07-11 14:26:26'),
('saheb ', 'mondal', '9609594337', 'msaheb288@gmail.com', 'palash danga, telua, khandaghosh, burdwan', 'Home Loan', 656, '9609594337', '30000', '', '2019-07-11 14:27:58'),
('Arokia', 'Jenifer ', '7339454312', 'arokiajenifer333@gmail.com', 'Colachel,  Kanyakumari District', 'Personal Loan', 657, '7339454312', '50000', '', '2019-07-11 14:35:02'),
('Khirud', 'Pator', '09706973340', 'ashman.nag@gmail.com', 'C/o Mukut Gogoi, M/s Yearn Store,  A. T. Road, Dikom Bazar, Chabua, Dibrugarh (Assam)', 'Personal Loan', 658, '8638137743 ', '15000', '', '2019-07-11 14:36:57'),
('Ramadevi', 'Ryali', '9985011996', 'hiramya143@gmail.com', '1-1-44 / 2 kondayapallem ', 'Personal Loan', 659, '9985011996', '30000', '', '2019-07-11 14:37:11'),
('Manisha ', 'Kainthola', '7836969190', 'Mkainthola123@gmail.com', '1485 maruti kunj Gurgaon ', 'Personal Loan', 660, '9319443204', '20000', '', '2019-07-11 14:37:54'),
('Kabin ', 'Boro', '9101168426', 'borokabin48@gmail.com ', 'Vill: Khandikar P.O Dhepargaon P.S : Tamulpur Dist : Baksa State Assam PIN: 781354', 'Personal Loan', 661, '9613060727', '100000', '', '2019-07-11 14:38:45'),
('Rakesh', 'Prasad', '9304774360', 'rv216867@gmail.com', 'Gwala basti telco jamshedpur ', 'Personal Loan', 662, '7209626070', '50000', '', '2019-07-11 14:41:22'),
('PREMKUMAR ', 'M', '8526875635', 'prem1461994@gmail.com', 'Ward 8', 'Personal Loan', 663, '9159872370', '8000', '', '2019-07-11 14:45:23'),
('Karanum joythi', 'K joythi', '9949376148', 'kali.karanm@gmail.com', '2-182, jayakrishna puram barma colany tolusurupalle Tekkali srikakulamAndhra pradesh- 532201', 'Personal Loan', 664, '9949376148', '30000', '', '2019-07-11 14:46:09'),
('Jaya', 'Durga', '8695495961', 'durgaplanner@gmail.com', '16mariyamman koil St drumarajpuram alangayam Tamil', 'Personal Loan', 665, '9360294044', '200000', '', '2019-07-11 14:50:49'),
('Aman', 'Negi', '8954232509', 'localarzee@gmail.com', 'Vivekanand gram Jogiwala Dehradun ', 'Personal Loan', 666, '9458901505', '5000', '', '2019-07-11 14:57:38'),
('Magan', 'Nigwal', '6239945464', 'magansinghnigam30@gmai.com', 'Vi.okhla po.katkut  te.barwaha', 'Personal Loan', 667, '9644426008', '1500000', '', '2019-07-11 15:04:14'),
('Magan', 'Nigwal', '9644426008', 'magansinghnigam30@gmail.com', 'Vi.okhla po.katkut  te.barwaha', 'Personal Loan', 668, '9644426008', '1500000', '', '2019-07-11 15:07:25'),
('Ranjeet ', 'Kumar', '7903688141', 'ranjeetraj65205@gmail.com', 'Vill_sabano tola tari, Post_sabano p.s._hussainabad,dis_palamau', 'Auto/Vehicle Loan', 669, '7779812438', '500000', '', '2019-07-11 15:07:39'),
('Akshaykumar', 'Patel', '9537247959', 'Patelakshay211in@gmail.com', '237, Patel Faliya, Hansapore, Navsari. 396 445', 'Personal Loan', 670, '9327947396', '110000', '', '2019-07-11 15:10:54'),
('Darla Vanya ', 'Teja', '9676015877', 'Rickyvanya1@gmail.com', 'Giddalur ', 'Personal Loan', 671, '9000503232', '3000', '', '2019-07-11 15:11:24'),
('G.karthikayyan', 'Karthik', '6382270039', 'karthivkzone@gmail.com', 'Bangalore', 'Personal Loan', 672, '9620372676', '100000', '', '2019-07-11 15:12:07'),
('Chenna', 'Shivakumar', '9347833218', 'shivakumarche71@gmail.com', 'H no 2-70/1 chevella village alladurg mandal medak district ', 'Personal Loan', 673, '9666651094', '50000', '', '2019-07-11 15:13:15'),
('Vishal', 'Athwal', '9816565586', 'vishalathwal@gmail.com', 'Mansar salogra', 'Personal Loan', 674, '8894004263', '100000', '', '2019-07-11 15:16:55'),
('Katara Mukeshbhai', 'Katara Hadyabhai', '9099764017', 'Mukeshbhai689@gmail.com', 'At.madhva po.vangad ta.fatepura di.dahod', 'Personal Loan', 675, '9099764017', '5 lakh', '', '2019-07-11 15:17:29'),
('Surjeet', 'Singh', '8826931943', 'Surjeetsingh0421@gmail.com', 'Sunjay colony sehatpur ', 'Home Loan', 676, '7703934849', '200000', '', '2019-07-11 15:18:51'),
('Shivansh ', 'Singh', '8429996554', 'shivanshsingh8306@gmail.com', 'Barabanki', 'Education Loan', 677, '9305894412', '500', '', '2019-07-11 15:21:16'),
('Sushma ', 'S', '7259260043', 'sushmatalawar500@gmail.com', '29,5th b cross,8th block,nagarbhavi 2nd stage', 'Personal Loan', 678, '9964788717', '500000', '', '2019-07-11 15:25:03'),
('Velmurugan', 'Velmurugan ', '9442320464', 'maheswaran1412@gmail.com', 'Kandakulam', 'Personal Loan', 679, '6380239931', '200000', '', '2019-07-11 15:34:14'),
('Uriyel ', 'Narzari', '6001353005', 'uriyelnarzari1234@gmail.com', 'Cycle factory mojit gali ', 'Personal Loan', 680, '9101888429', '150000', '', '2019-07-11 15:35:11'),
('Sonu', 'kumar', '7827310040', 'sksonu0405@gmail.com', 'A - 350, Jaitpur Extension Part - 1, near gurudwara, Badarpur', 'Personal Loan', 681, '8826283897', '100000', '', '2019-07-11 15:36:11'),
('Venkatesan', 'M', '7871964024', 'venkatcr1977@gmail.com', '89.krishna nagar ', 'Personal Loan', 682, '9500122548', '200000', '', '2019-07-11 15:40:28'),
('Shital ', 'Rathod ', '7620787155', 'shital.ade12@rediffmail.com ', 'Dayand colini Umarkhed Dist yavatmal ', 'Personal Loan', 683, '9763892111', '1000000', '', '2019-07-11 15:43:39'),
('Ankit', 'Khtiwada', '8126071918', 'Ceragemalmora@gmail.com', 'Sainik nagar kasampur meerut cantt', 'Personal Loan', 684, '8439525231', '300000', '', '2019-07-11 15:46:56'),
('Anita Bose', 'Thakkar', '8961588555', 'dasanita76@gmail.com', 'E-398,Green  Field city.S Shibrampur Mahestala.', 'Personal Loan', 685, '08961588555', '3,00,000', '', '2019-07-11 15:57:29'),
('Poonam', 'Devi', '7906267511', 'ankushraghav1991@yahoo.com ', 'Village-Sadarpur Sec-45 Noida', 'Personal Loan', 686, '9811864115', '200000', '', '2019-07-11 15:59:06'),
('Kritesh', 'Jat', '7024333110', 'Avirock787@gmail.com', '12 choti gwal toli street no1', 'Personal Loan', 687, '9826708288', '100000', '', '2019-07-11 15:59:09'),
('Rajesh', 'Kumar', '9518217463', 'rajeshjat518@gmail.com', 'Vpo duloth jat', 'Personal Loan', 688, '9466123768', '30000', '', '2019-07-11 15:59:30'),
('Sumit', 'Raghav', '8368797244', 'Raghavsumit153@gmail.com ', 'Village-Sadarpur Sec-45 Noida', 'Personal Loan', 689, '8860229710', '150000', '', '2019-07-11 16:01:31'),
('Shabbir Hasan', 'shabbir hasan', '6386733044', 'hasanshabbir055@gmail.com', 'mufti mohal jaunpur', 'Personal Loan', 690, '6386733044', '500000', '', '2019-07-11 16:04:08'),
('Manisha', 'Bairwa', '9974302606', 'manisha.bairwa009@gmail.com', '12 maa jwala apna Ghar colony near Panchayat samiti Abu road sirohi Rajasthan', 'Personal Loan', 691, '9974302606', '30000', '', '2019-07-11 16:05:20'),
('Sathya', 'Narayanan', '9080772167', 'narayanan20chennai@gmail.com', '31/44, Selvaperumalkovilstreet  otteri', 'Personal Loan', 692, '09150157370', '200000', '', '2019-07-11 16:06:38'),
('Ranjeet', 'Kumar', '9971034484', 'rk8955278@gmail.com', 'Azamgarh U.P. 276201', 'Personal Loan', 693, '9354716052', '500000', '', '2019-07-11 16:12:36'),
('Sarita', 'vaiswar', '7065754018', 'saritabaiswar@gmail.com', 'B-252 kotla mayur vihar phase 1', 'Personal Loan', 694, '6306748623', '100000', '', '2019-07-11 16:13:18'),
('Keshri', 'Netam', '7000096060', 'Tarunagrawal45@gmail.com', 'Main road rajkamma', 'Personal Loan', 695, '9669088889', '300000', '', '2019-07-11 16:14:55'),
('Mahipal', 'Ram', '9982393966', 'Mahipalrambajiya@gmail.com', 'Khinyas,degana,nagaur', 'Personal Loan', 696, '9413383966', '60000', '', '2019-07-11 16:19:02'),
('Sambit', 'Pradhan', '6370313381', 'sambitpradhan2129@gmail.com', 'Adaspur', 'Personal Loan', 697, '737724923', '50000', '', '2019-07-11 16:25:25'),
('SHANKAR', 'KURCHAPELLI', '9010358123', 'shankarkurchapelly.@gmail.com', 'H.NO 2-9-1050. Waddepally.hanamakoda.warangal.pin.506370', 'Personal Loan', 698, '9885655512', 'Fifteen housand', '', '2019-07-11 16:26:18'),
('Pawan', 'Yadav', '8858549795', 'yadavpawan7654@gmail.com', 'Shop no.299 icici bank Ltd.kandeli, Itwara bazar narsinghpur mp', 'Personal Loan', 699, '8858549795', '10000', '', '2019-07-11 16:27:58'),
('Sudheer', 'Bhatt', '7900142721', 'gm.midwayresort@gmail.com', 'Tisriyara basar binak khal tehri garhwal uttarakhand', 'Personal Loan', 700, '9552920729', '200000', '', '2019-07-11 16:29:05'),
('Ankit', 'Srivastava', '9097719804', 'ankits.ida@gmail.com', 'Manikunj Apartment, Road No-21', 'Personal Loan', 701, '08227889091', '100000', '', '2019-07-11 16:31:22'),
('Prashant', 'Sarkate', '9112438790', 'prashantsarkate1990@gmail.com', 'Abedakar nagar aurngabad', 'Personal Loan', 702, '9561526021', '10000', '', '2019-07-11 16:32:55'),
('Samyak ', 'Chopra', '9977287292', 'samyak667chopra@gmail.com', '328 tilak marg', 'Personal Loan', 703, '8770092624', '300000', '', '2019-07-11 16:33:26'),
('Prem ', 'Kanti', '9810258477', 'mann.prem143@gmail.com', 'Hoimu', 'Personal Loan', 704, '98700958821', '300000', '', '2019-07-11 16:35:17'),
('Puran', 'Sharam', '8627098605', 'puransharam1@gmail.com', 'Manikaran, Manikaran', 'Personal Loan', 705, '8219329098', '50000', '', '2019-07-11 16:39:01'),
('Atur rongcheicho', 'Terang', '8486324737', 'arongcheichoterang@gmail.com', 'Rukasen near Atur kimi school diphu', 'Personal Loan', 706, '8638882920', '250000', '', '2019-07-11 16:41:38'),
('Adil', 'Wasim', '9335006517', 'adilwasim52@gmail.com', 'Kanpur', 'Personal Loan', 707, '8299363287', '500000', '1', '2019-07-17 05:54:09'),
('Prakash ', 'Kharwar', '6203368431', 'rajp02400@gmail.com', 'Jarad', 'Personal Loan', 708, '7070134902', '10000', '', '2019-07-11 16:46:13'),
('Savitaben ', 'Nileshbhai', '7016768494', 'Nileshchauhan9803@gmail.com', 'A-205 suman darshan LIG Aavaso Singapore cozway Road surat', 'Personal Loan', 709, '9904871026', '20000', '', '2019-07-11 16:47:03'),
('ABHIJIT', 'MUKHERJEE', '8391097093', 'abhijtmukherjee724@gmail.com', 'Kamarpukur', 'Personal Loan', 710, '', '', '', '2019-07-11 16:51:44'),
('Pooja', 'Sharma', '8769577879', 'sharmajipooja232@gmail.com', '96, Keshav vihar gopalpura bypass Jaipur', 'Education Loan', 711, '', '120000', '', '2019-07-11 16:54:22'),
('Harshit ', 'Goel', '8738828996', 'hagarwal34649@gmail.com', '396/B Arya Nagar Sitapur ', 'Personal Loan', 712, '7309603636', '20000', '', '2019-07-11 16:56:38'),
('Upendra ', 'Ray', '8877292146', 'upendra_ray13@rediffmail.com', 'Deoghar', 'Home Loan', 713, '8809967228', '100000', '1', '2019-07-17 05:56:28'),
('Dattatreya', 'Aihole', '9880066920', 'supercomputersafz@gmail.com', 'Afzalpur, Afzalpur', 'Education Loan', 714, '6360625613', '500000', '', '2019-07-11 17:00:55'),
('Rajeev', 'Yadav', '8059316210', 'priyanka482050@gmail.com', 'Vill-Masit, P.o-Rampuri', 'Personal Loan', 715, '7056482050', '30000', '1', '2019-07-17 06:10:24'),
('Samuel', 'Lalawmpuia', '8731875426', 'samuelmszeedurtlang@gmail.com', 'Aizawl', 'Personal Loan', 716, '', '1500000', '', '2019-07-11 17:07:27'),
('Jagadish', 'Rajput', '7970034232', 'Jagdishrajput2764216@gmail.com', 'Chhattisgadh bilaspur kota ranibachhali', 'Auto/Vehicle Loan', 717, '7970034232', '500000', '', '2019-07-11 17:09:33'),
('Jogu', 'Sandhyarani', '9866140926', 'jogusandhyarani@60173gimail.com', '8-6-34nehrunagar rajanna Siricilla', 'Personal Loan', 718, '09866140926', '1 lack', '', '2019-07-11 17:13:26'),
('Ramkishan singh', 'Ramkishan singh', '8112213847', 'Ramkishan bhati@emel×www', '6 himmt nagar', 'Home Loan', 719, '8112213747', '5oooi', '', '2019-07-11 17:15:14'),
('Awais', 'Faridi', '8285879667', 'faridiawaish@gmail.com', 'Akg engineering college,nh-24', 'Personal Loan', 720, '7834905361', '5000', '', '2019-07-11 17:20:00'),
('Rajalaxmi', 'Devarakonde', '7259826671', 'rajlakshmidevarkonde@gmail.com', 'Shri guru Krupa nilaya h.no. 18 gandhinagar 1st main cross dharwad 580004', 'Personal Loan', 721, '6360334286', '50000', '1', '2019-07-17 06:08:27'),
('Jay', 'Grover', '8718944011', 'jaygrover011@gmail.com', '132/2 yashwant marg, mahidpur', 'Personal Loan', 722, '08718944011', '20000', '', '2019-07-11 18:42:27'),
('Kommuru', 'Yamuna', '7893274669', 'kommuruyamuna8418@gmail.com', 'Opp judge court cmr back side s.b.i. colony dno 29-42-25', 'Personal Loan', 723, '7893273225', '30000', '1', '2019-07-17 05:55:01'),
('RAHUL', 'SHARMA', '9039032171', 'rs56191@gmail.com', 'Ward no.14 ALKH DHAM NAGAR MANGLA BILASPUR CHHATTISGARH', 'Personal Loan', 724, '9907377774', '50000', '', '2019-07-11 19:17:19'),
('Udayraj', 'Varma', '8451918887', 'udaysnehavarma@gmail.com', 'Taloja phase, I dream sity plot no.41sec, 11 flot no.605 sity home creater taloja', 'Personal Loan', 725, '8433939758', '20000000', '', '2019-07-11 19:30:28'),
('Afzal', 'Anis', '7905966124', 'afzal_anis_ansari2279@hotmail.com', '16/1Faithful ganj cantt kanpur', 'Personal Loan', 726, '9795164501', '150000', '', '2019-07-11 19:37:52'),
('Mohd afroz', 'Ahmad', '7007433677', 'khan.afroz192@gmail.com', 'H No 188da hydel colony shivpuri sarojini nagar', 'Personal Loan', 727, '9889408670', '50000', '', '2019-07-11 19:51:21'),
('Sujeet Kumar ', 'Kutarmare', '9300146989', 'SUJEETKKUMAR20@GMAIL.COM', 'House no71 ward no1 dubeyseloo', 'Auto/Vehicle Loan', 728, '9468319062', '1500000', '', '2019-07-11 20:24:26'),
('Satendra', 'Chaubey', '7219007577', 'chaubey.satyendra@gmail.com', 'A24 tushar housing society, Shreeram Nagar bilalpada nalasopara east', 'Personal Loan', 729, '9930196150', '100000', '', '2019-07-11 20:53:56'),
('Prasanthi', 'Dasari', '8465866848', 'Prasu1553@gmail.com', '6-2-12 adundlepet', 'Personal Loan', 730, '8897172363', '100000', '', '2019-07-11 21:17:31'),
('BABULAL BABULAL ', 'BABULAL ', '9928879345', 'blbhati9928879345@gmail.com ', 'Owan wali dhañiya tapu tehsil osian', 'Personal Loan', 731, '8209659915', '500000', '', '2019-07-11 21:30:14'),
('Zarina', 'Khatoon', '7003056325', 'zarinakhan98615@gmail.com', '95b martinpara uttar panchannogram kolkata ', 'Auto/Vehicle Loan', 732, '7003056325', '500000', '', '2019-07-11 21:52:57'),
('Kotholla ', 'Chou reddy ', '9908680546', 'choureddykotholla1438@gmail.com', '14-58,pulusugunthalu,thettu,kurabalakota,chittor,AP', 'Education Loan', 733, '7013994033', '10000', '', '2019-07-11 23:59:10'),
('Nakul ', 'Wadhwa ', '9953201104', 'goldlineventures@gmail.con', '10 east avenue road east punjabi bagh', 'Personal Loan', 734, '01123636277', '600000', '', '2019-07-12 00:34:02'),
('TALLAPALLI ', 'VENGAIAH ', '9652606508', '93vengaiah@gmail.com', 'H.no 33-92,ramnagar, addanki,prakasam distic, andhra pradesh', 'Personal Loan', 735, '6281527048', '10000', '', '2019-07-12 00:37:42'),
('Kamiruddin ', 'Sk', '9153339011', 'kamirsk.1999@gmail.com', 'Kadampur Rajgaram Murarai Birbhum', 'Education Loan', 736, '9153339011', '5000', '', '2019-07-12 01:14:49'),
('Sanjay ', 'Kumar', '8979329872', 'sanjaykumar07894@gmail.com', 'Nataliya Kherabad atruali Aligarh 202280', 'Education Loan', 737, '8979329872', '30000', '', '2019-07-12 01:23:41'),
('MARRAPU', 'SRIKANTH', '9492928937', 'marrapu.srikanth92@gmail.com', 'Dr.no.1-46, chinnarayudupeta, maripivalasa, seethanagaram, vizianagaram', 'Personal Loan', 738, '8309488429', '25000', '', '2019-07-12 01:52:16'),
('Barish ', 'Roy', '7005458321', 'barishroy@gmail.com', 'Madhya Banamalipur Near 82 No. Ration Shop', 'Personal Loan', 739, '9774388752', '20000', '', '2019-07-12 01:57:16'),
('Gautam ', 'Manjhi', '7988429193', 'd.k143.25897@gmail.com', 'H. No.197, Govt. Colony, Hisar Haryana-125001', 'Personal Loan', 740, '8607276224', '50000', '', '2019-07-12 02:13:25'),
('Tapas ', 'Banerjee ', '9800202472', 'tapasbanerjee243@gmail.com ', '17/18, Ambika babu lane, po: Khagra, Dist: Murshidabad, West Bengal ', 'Personal Loan', 741, '7363025290', '200000', '', '2019-07-12 02:14:24'),
('Fazalu rahiman', 'VP', '9544650026', 'Fazalvp48167@gmail.Com', 'VAliyapeediyakkal house,urangattiri post,vadakkummury', 'Personal Loan', 742, '9061929580', '100000', '', '2019-07-12 02:44:05'),
('Shibu', 'Sj', '9048690125', 'Shibusj69912@gmail.com', 'Madeena lodge, RMno:108, vizhinjam, trivandrum. 695521.', 'Personal Loan', 743, '7994940188', '300000', '', '2019-07-12 03:14:46'),
('Kishor', 'Rajput', '7600504866', 'Rajanrajput398@gmail.com', 'A.12 satyensociety  utkarshpetrolpump, Petrol pump karelibaug', 'Personal Loan', 744, '9327566697', '100000', '', '2019-07-12 03:17:14'),
('Thomas', 'C F', '9656949797', 'Judythomas081@gmail.com', 'Chittettu, ezhacherry p. o', 'Personal Loan', 745, '9656949797', '150000', '', '2019-07-12 03:18:55'),
('Laishram', 'Tony', '9366081812', 'tonylaishram007@gmaul.com', 'Imphal Manipur', 'Personal Loan', 746, '9089034429', '100000', '', '2019-07-12 03:56:36'),
('Mukesh', 'Sharma', '7839072557', 'mukesh.mukesh.sharma95@gmail.com', '229 indrapuri manasnagar alambagh Luckniw', 'Personal Loan', 747, '8601514218', '60000', '', '2019-07-12 04:00:54'),
('Sanjaykumar', 'Borde', '431008', 'sanjaykumarborde5432@gmail.com', 's.s.park Gut No -169, Flat No-B-10 Sawangi Aurangabad', 'Personal Loan', 748, '9158149256 ', '20000', '', '2019-07-12 04:10:33'),
('Sanchita', 'SAMANTA', '8240686307', 'chance31@rediffmail.com', '51E/2,KABIGURU SARANI, behala, nandana park', 'Personal Loan', 749, '9748661606', '50000', '', '2019-07-12 04:31:22'),
('Rahul', 'Wakchaure', '7414929359', 'rswakchaure338@gmail.com', 'New Mumbai CBD belapur  secter 6', 'Auto/Vehicle Loan', 750, '8796363320', '5', '', '2019-07-12 04:33:30'),
('Biswajit', 'Chakraborty', '8016738776', 'bchakraborty456@gmail.com', 'Khanakul  Ghaghorpur Hooghly', 'Home Loan', 751, '8016738776', '250000', '', '2019-07-12 04:49:32'),
('ANIL', 'KUMAR ', '9591285383', 'bjanilbj@gmail.com', 'Temple road harihar 54/54/54 alya enterprises ', 'Personal Loan', 752, '8884131749', '30000', '', '2019-07-12 05:08:13'),
('Krishna ', 'Pawar ', '9075880856', 'krishnapawar50706@gmail.com', '207030248283', 'Personal Loan', 753, '9075880856', '20000', '1', '2019-07-17 05:53:21'),
('THEEPALLIPUDI', 'RAMESH', '7337409826', 'rameshbulls143@gmail.com', '15/343,NAKKALOLLA CENTER,SUBEDAR PET 524001.NELLORE.ANDHRA PRADESH.', 'Personal Loan', 754, '8466961326', '100000', '', '2019-07-12 05:13:50'),
('Ajeesh', 'T M', '9846024844', 'ajeeshtm@gmail.com', 'Rakesh bhavan, plavara, pacha p.o,palode', 'Personal Loan', 755, '7012649939', '75000', '', '2019-07-12 05:20:54'),
('P', 'Dowerah', '8473091135', 'preronadowerah93@gmail.com', 'P.N.Road,Chring Chapori', 'Personal Loan', 756, '9957278554', '500000', '', '2019-07-12 05:37:48'),
('Yogendra', 'Mishra', '9455305752', 'Yogendramishra25@gmail.com', '145Q/1R/Q gayasuddinpur mundera', 'Personal Loan', 757, '8840373974', '15000', '', '2019-07-12 05:47:48'),
('Chandan', 'Kumar', '9164022581', 'Rohitkumar8032@gmail.Com', '1969 JANTHA NAGARA, MARUTHI TENT', 'Personal Loan', 758, '9164022581', '20000', '', '2019-07-12 05:47:59'),
('Munni', 'kumari', '8337877444', 'rdaruka2@gmail.com', 'Near kulti rly station kulti district paschim burdwan hi, Same', 'Personal Loan', 759, '08337877444', '25000', '', '2019-07-12 05:49:32'),
('Kadu ashok', 'Aute', '8605834569', 'Kaduaute1234@gmail.com ', 'Wasusaigaon ta gangapur aurngabad maharashtra ', 'Personal Loan', 760, '8999489564', '300000', '', '2019-07-12 05:56:14'),
('Vijaykumar', 'Vijaykumar', '8417872224', 'vijaykumar3200191@gmail.com', 'krishnagiri', 'Personal Loan', 761, '7063125793', '50000', '', '2019-07-12 06:17:31'),
('Mrityunjoy', 'Biswas', '9831677668', 'mrityunjoy 597@gmail.com', '102 Ramkrishna pally kol 96', 'Personal Loan', 762, '7980584391', '100000', '', '2019-07-12 06:21:18'),
('Kirankumar', 'Accha', '9972547633', 'Sajjankirankumar79@gmail.com', '  Post office road sirwar', 'Personal Loan', 763, '6363614093', '5 lak', '', '2019-07-12 06:36:59'),
('Puchakayala', 'umadevi', '8367487876', 'umadevip2021@gmail.com', 'Dr.no-55-4-66old venkojipalem,h.b.colony', 'Personal Loan', 764, '7036514091', '40000', '', '2019-07-12 06:41:19'),
('Amar nath', 'Gupta', '7408000767', 'rinkugupta7408@gmail.com', 'Mankapur road utraula balrampur', 'Auto/Vehicle Loan', 765, '9170805776', '80000', '', '2019-07-12 06:43:13'),
('Vijay ', 'Prasad', '7002410014', 'govindprasad365@gmail.co', 'KUNDESWAR, KUNDESWAR', 'Personal Loan', 766, '7002410014', '200000', '', '2019-07-12 06:54:58'),
('K SANTOSH KUMAR', 'RAO', '8247626002', 'ksantoshkumarrao1985@gmail.com', '8-3-228/202/b vivekananda nagar society, rahmath nagar yousufguda', 'Personal Loan', 767, '7787053926', '50000', '', '2019-07-12 06:59:36'),
('akhilesh', 'kumar', '9717752212', 'shahiakhilesh@rocketmail.com', 'R3,A3/14 MOHAN GARDEN UTTAM NAGAR NEW DELHI -110059', 'Personal Loan', 768, '8368950072', '20000', '', '2019-07-12 07:27:44'),
('Raj', 'Kumar', '9934661663', '9173002596', 'Buxer', 'Personal Loan', 769, '7352508563', '400', '', '2019-07-12 08:33:05'),
('Ravi Kumar', 'Ravi Kumar', '6392311969', 'ravikumar495@gmail.com', 'Village.baherapur.post, Kumbhi', 'Personal Loan', 770, '6392311969', '300000', '1', '2019-07-16 08:06:17'),
('Farman ali', 'Ali', '7073060794', 'Farmanali48355@gmail.com', 'Ward No 26 Mohlla kumarhan Jhunjhunu ', 'Personal Loan', 771, '7073060794', '100000', '1', '2019-07-16 05:49:43'),
('Anand', 'dubey', '8437788794', 'ananddubey0009@gmail.com', 'Street No 6 Near Bachhan Gais Agency 33\'road Gaispura Ludhiana', 'Auto/Vehicle Loan', 772, '+918437788794', '100000', '', '2019-07-12 09:02:40'),
('Prashant', 'Singh', '9369639685', 'prashant25647@gmail.com', 'Daktar colony poni road Shukla ganj unnao', 'Personal Loan', 773, '9151244245', '100000', '', '2019-07-12 09:26:49'),
('Nirmala ', 'Zalake ', '9538104537', 'shaileshgavali52948@gmail.com', 'At-Benchinmardi, Post -Urabinahatti, Taluka -Gokak, Dist-Belgavi ', 'Personal Loan', 774, '9172552562', '250000', '', '2019-07-12 09:46:44'),
('Kalyani ', 'Khuntia', '8445228308', 'Kalyanikhuntia69@gmail.com', 'Bangla gaon sabji mandi moradabad', 'Personal Loan', 775, '7248282521', '25000', '', '2019-07-12 09:57:37'),
('Ashish', 'Kumar', '9319460286', 'Absp099@gmail.com', 'New Delhi', 'Personal Loan', 776, '9319460286', '10000', '', '2019-07-12 10:06:33'),
('saqibkhan', 'khan', '9591135512', 'saqibsajidullakhan@mail.com', 'mysore. nr.mohlla', 'Personal Loan', 777, '9945876170', '30000', '', '2019-07-12 10:42:52'),
('Iftekhar', 'Lari', '9258142492', 'Ialari1954@gmail.com', '4/7b nagla prithvinath shahganj', 'Personal Loan', 778, '7901180764', '25000', '', '2019-07-12 10:54:21'),
('Suresh', 'Vemuri', '9346711026', 'vemurivsuresh@gmail.com', 'Nellore', 'Personal Loan', 779, '8074499044', '100000', '', '2019-07-12 10:55:11'),
('NARENDRA', 'NARENDRA', '9606736819', 'nari.ram6878@gmail.com', '2/96 manur village madakasira mandal anantapur', 'Personal Loan', 780, '9676113671', '3000', '', '2019-07-12 10:55:50'),
('Nitin', 'Jagdhane', '9834588144', 'nitinjagdhane333@gmail.com', 'Plot no 148 Parkashnagar Mukundwadi', 'Personal Loan', 781, '09834588144', '10,00,000', '', '2019-07-12 10:59:18'),
('Mamta ', 'Mehra ', '9992703266', 'mamtamehra5477@gmail.com', 'House number 1085/13/2 Purana allu godown Ambala cantt', 'Personal Loan', 782, '9991323266', '30000 ', '', '2019-07-12 11:05:38'),
('Ramesh', 'Singh', '8010514666', 'rameshsingh111183@gmail.com', 'J 51 .gali no. 01 jai prakash nager', 'Personal Loan', 783, '7011603005', '200000', '', '2019-07-12 11:06:48'),
('SHYAM ', 'SUNDAR ', '8127090972', 'Sambharti19@gmail.com', '283 ka/334 b Rajendra Nagar  Lucknow ', 'Personal Loan', 784, '7355250904', '40000', '', '2019-07-12 11:14:32'),
('Hemant singh ', 'Jadon', '9685845209', 'hemantjadon506@gmail.co', 'Dwarikapuri colony Karondi sampwell ke piche shivpuri madhya Pradesh ', 'Education Loan', 785, '9685845209', '200000', '', '2019-07-12 11:19:27'),
('VICKY', 'GHANGHAV', '8421448745', 'vicky.ghanghav143@gmail.com', 'NAGSEN NAGAR USMANPURA AURANGABAD', 'Personal Loan', 786, '7757819621', '50000', '', '2019-07-12 11:34:57'),
('Tarun ', 'Kandpal ', '7248203500', 'tarunkandpal92@gmail.com', 'Choudhary khola, pokharkhali, almora, uttrakhand ', 'Personal Loan', 787, '7310910689', '100000', '', '2019-07-12 11:37:02'),
('Murugesh', 'Murugesh', '636927387', 'gmurugesh22051@gmail.com', '1/197 kollupalayam gudimangalam tirupur', 'Personal Loan', 788, '8610359621', 'no', '', '2019-07-12 11:44:58'),
('Karunanidhi', 'Yadav', '9794610386', 'yadavkarunesh617@gmail.com', 'Maheshpur mehadiya dist. Maharaj ganj', 'Education Loan', 789, '9794610386', '200000', '', '2019-07-12 11:57:08'),
('KURASALA', 'NARESH', '7893445553', 'KURASALANARESH@GMAIL.COM', '5-3-417/1a, 2nd floor', 'Personal Loan', 790, '8520988222', '100000', '', '2019-07-12 12:09:23'),
('Mamta ', 'Bajaj', '8007819550', 'Abajaj231@gmail.com', 'Amravati ', 'Personal Loan', 791, '9403309550', '300000', '', '2019-07-12 12:28:51'),
('Praveen ', 'Nautiyal', '9818690950', 'nauti479@gmail.com', 'Shiv Shakti mandir Rk puram sector-1 new Delhi', 'Personal Loan', 792, '6395452917', '10000', '', '2019-07-12 12:33:47'),
('Sagir', 'Beg', '8796522143', 'Sagirbeg91@gmail.com', 'Lane no 5 dhule', 'Personal Loan', 793, '9028794313', '10000', '', '2019-07-12 12:34:01'),
('Muthumari ', 'Raja', '9629657967', 'coppergood1540@gmail.com', '261/348h.indra colony,subramaniyaburam poththai.ambasamudram ', 'Personal Loan', 794, '9848854329', '20000', '', '2019-07-12 12:51:05'),
('Palivela', 'Rajeev', '8260601666', 'palavellarajeev@gmail.com', 'Akula Street Baidiya Nath Pur', 'Personal Loan', 795, '7008771010', '15000', '', '2019-07-12 12:52:22'),
('KUraku', 'Anusha', '9381022714', 'anu.hazzu@gmail.com', 'Lakshmipuram Rapur', 'Personal Loan', 796, '7032007443', '200000', '', '2019-07-12 13:03:11'),
('Dheeraj', 'Joshi', '8755322514', 'minnyjoshi2011@gmail.com', 'Vill shail PO Baldhauti Almora', 'Personal Loan', 797, '9411742084', '50000', '', '2019-07-12 13:09:59'),
('Radhika', 'Arora', '9773783722', 'arora.radhika519@gmail.com', 'A213 west Vinod nagar', 'Education Loan', 798, '9773783721', '25000', '', '2019-07-12 13:20:51'),
('Neelam', 'Sagar', '9759282958', 'neelamsagar2017@gmail.com', 'Ho. No. 415 rajnagar caloni bulandshahr', 'Education Loan', 799, '9458288341', '50000', '', '2019-07-12 13:29:21'),
('Ramathal', 'Ramathal', '8754645558', 'jothivenkat984@gmail.com', 'Thindal, erode', 'Personal Loan', 800, '6382584025', '70000', '', '2019-07-12 14:31:48'),
('Bhure ', 'Shah ', '9675743723', 'Khan Bhura 082@geeml.com', 'Village milk papdi ', 'Personal Loan', 801, '7065321692', 'Dist amroha', '', '2019-07-12 15:08:50'),
('Arunkumar', 'Nehru', '9566914546', 'arunanu500@gmail.com', '176,Balavinayagar kovill street, muhavur', 'Personal Loan', 802, '7358870578', '100000', '', '2019-07-12 17:25:55'),
('Anit Kumar', 'Dohar', '7049367265', 'anitkumardohar', 'villge mohaniya post tihae dist.Satna m.P.', 'Personal Loan', 803, '7049367265', '6000', '', '2019-07-12 19:14:09'),
('Nimit Kumar', 'Srivastava', '7905784239', 'nimitkumar12@gmail.com', '740 kamal singh colony near sai atta chakki ,Jhansi', 'Personal Loan', 804, '9766396516', '50000', '', '2019-07-12 19:52:10'),
('vadlakonda', 'nagaraju', '8106063695', 'githendra21@gmail.com', 'warangal kashibugga', 'Personal Loan', 805, '8332934908', '5000', '', '2019-07-12 20:19:27'),
('Brijeshkumar ', 'Somabhai patel', '8347848428', 'patelbrijesh879@gmail.com', 'Bodvank, bhagat faliya ta. Chikhli. Dist. Navsari', 'Personal Loan', 806, '6352264677', '100000', '', '2019-07-12 21:07:44'),
('Rakesh', 'Sahoo', '9853762917', 'arnrakesh@gmail.com', '106,Ashoka residency,gnr enclave , patel narayana reddy layout,kormnagala,Bangalore ,560095', 'Personal Loan', 807, '8249366904', '100000', '', '2019-07-12 21:51:57'),
('Horen', 'Bedia', '8011674934', 'horenbedia22@gmail.com', 'Dibrugarh,Dikom,Assam 786101', 'Education Loan', 808, '8135072519', '1,00,000', '', '2019-07-12 23:51:55'),
('ROOP Chand ', 'TIWARI', '8130408730', 'roopchandtiwari7154@gmail.com', 'A213 Karan Vihar Sulemaan Nagar Kirari, Tula Ram Publick School', 'Personal Loan', 809, '8468998562', '200000', '', '2019-07-13 00:51:15'),
('Gurprit', 'Singh', '8094348845', 'gurpritd29657@gmail.com', '22ksp araiyanwali ward no 8', 'Education Loan', 810, '6350121901', '10000', '', '2019-07-13 01:03:33'),
('Kishore', 'Daniel', '6380502331', 'a.kishoredanielfdo@gmail.com', 'No 33 kerocope street thoothukudi', 'Personal Loan', 811, '8903007631', '25000', '', '2019-07-13 01:21:07'),
('Amol', 'Ambolikar', '7709563520', 'amolbiker@gmail.com', 'Vitthal gujari ward pauni bhandara Maharashtra', 'Personal Loan', 812, '08380908643', '5000', '', '2019-07-13 02:02:37'),
('Vikram ', 'Singh', '7807819172', 'viksin452@gmail.com', 'Himachal Pradesh patohala wardno2 kardiyana dharamshala kangra', 'Personal Loan', 813, '8988844637', '10000', '', '2019-07-13 03:02:45'),
('Pabitra Tandi', 'Bhole', '6372709220', 'tandipabitra03@gmail.com', 'India Orissa Nuapada khariar Rajamunda', 'Personal Loan', 814, '6372709220', '1000000', '', '2019-07-13 03:03:54'),
('Aditya', 'Kumar', '8013176118', 'Adityakumar2296@gmail.com', '71/14 babgasree pally swamiji road, kolkata 700060', 'Personal Loan', 815, '8910159117', '100000', '', '2019-07-13 03:09:33'),
('GOVINDAMMAL', 'VARADARAJ', '6374470558', 'samjerin24@gmail.com', '75 PHASE 1 ANNANAGAR NAVALPATTU ', 'Personal Loan', 816, '9080367483', '40000', '', '2019-07-13 03:11:09'),
('Dinesh', 'Ahirwal', '9691215414', 'Nraj@691917email.com', 'Gram hinota post bansatarkheda mp', 'Personal Loan', 817, '2', '500000', '', '2019-07-13 03:27:44'),
('Satish', 'Rana', '9634420982', 'cenatimeoo7@gmail.com', '58 B Balajipuram Alwatiya Road', 'Personal Loan', 818, '9958855370', '500000', '', '2019-07-13 03:41:48'),
('Haranchandra ', 'biswas', '8158806625', 'haranchandrabiswas@gmail.com', 'Debgarh m bongaon', 'Personal Loan', 819, '8158806625', '30000', '', '2019-07-13 04:07:58'),
('Sandeep', 'Shejule ', '8828495807', 'Sandeepshejule1982@gmail.com', 'Saibaba nagar diva naka rabale navi Mumbai ', 'Personal Loan', 820, '8805378527', '10000', '', '2019-07-13 04:20:45'),
('Mithun', 'Shirpi M', '8667278299', 'shirpionline@gmail.com', '5/428 m.n.Nagar nandavanapatti', 'Personal Loan', 821, '8939210824', '100000', '', '2019-07-13 04:29:05'),
('Aditya kumar', 'Mehta', '7294157416', 'adaditya5308@gmail.com', 'Hazaribagh jharkhand', 'Personal Loan', 822, '9309810059', '5000', '', '2019-07-13 04:29:51'),
('Sonu', 'Sharma', '6396486327', 'Sonusharmavidholiya@gmail.com', 'Bidhouli jagner Agra jagner uttar pradesh 283115', 'Auto/Vehicle Loan', 823, '9568461184', '5.5 lakh', '', '2019-07-13 04:33:19'),
('Sebastian ', 'P R', '9633013003', 'roshanroyrosh@gmail.com', 'Udayamperoor, Nadakkavu', 'Personal Loan', 824, '9895787534', '6000', '', '2019-07-13 04:51:36'),
('Shubham', 'Kumar', '9582205557', 'shubhamsurya44@gmail.com', 'Vill+post jasmour behat saharanpur', 'Personal Loan', 825, '8979023557', '5000', '', '2019-07-13 04:56:22'),
('Arjunbhai ', 'Solanki ', '8160024326', 'amitrajput2209w@gamil.com', 'Ambasank, Ambasankri, Ambasankari', 'Personal Loan', 826, '8160024326', '50000', '1', '2019-07-17 10:43:21'),
('Subrat behera', 'Subrat behera', '9543667309', 'subrat24961@gmail.com', 'at.Othaka. kakatpur.dist.Puri.state.odisha', 'Personal Loan', 827, '8056034124', '25.000', '', '2019-07-13 05:27:39'),
('NIRMALA DEVI ', 'S', '6383818306', 'ks.nirmala@yahoo.com', 'Kuttikadu, otterkarattupalayam, kurumandur ', 'Personal Loan', 828, '9677314886', '30000', '', '2019-07-13 05:36:25'),
('Parmar maheshbhsi', 'Laxmanbhai', '9978545107', 'Maheshparmar171985@gimil..com', 'At.khandha ta waghodiya dist.vadodars', 'Personal Loan', 829, '9978545107', '50000', '', '2019-07-13 05:40:13'),
('Nancy', 'Nancy', '9501149949', 'Michaelmeet291@gmail.com', 'V.P.O-NANDGARH,SRI MUKTSAR SAHIB,PUNJAB,152026', 'Personal Loan', 830, '9915920967', '10000', '', '2019-07-13 05:44:30'),
('sanjay', 'singh', '9425874828', 'sanjaykumarsingh.1975@gmail.com', 'D4-12,Nirma staff colony,nimbol,pali(raj)306308', 'Personal Loan', 831, '8094903208', '100000', '', '2019-07-13 06:06:03'),
('Nitin ', 'Kumar ', '7248790807', 'Nitindhiman497@gmali.com', 'Dhimanpuar shamli', 'Education Loan', 832, '7248790807', '500000', '', '2019-07-13 07:14:45'),
('vishal', 'dubey', '9906002822', 'dubeyvishal715@ gmail.com', 'm/s r k bags main bazar katra', 'Personal Loan', 833, '7006092862', '300000', '', '2019-07-14 08:53:04'),
('Latha', 'Gopalakrishnan ', '8270715526', 'Lathanext.09@gmail.com ', '9, 13th cross,Krishna anger, lawspet,  pondicherry ', 'Personal Loan', 834, '8270715525', '50000', '', '2019-07-14 08:54:40'),
('Lokchand', 'Kurre', '9348551352', 'Lokchandkurre@gmail com', 'Parsuli post khatti teh. Bagbahera jila mahasamund chhattishgarh', 'Personal Loan', 835, '8144219261', '10000', '', '2019-07-14 09:03:38'),
('Lebaku', 'Vishnupriya', '9642320448', 'abmvenkatesh@gmail.com', 'F.no.243 anilcastles gollapudi vijayawada', 'Personal Loan', 836, '9393768999', '50000', '', '2019-07-14 09:10:10'),
('SALEEM ', 'AHAMAD ', '9755468824', 'rh1431@gmail.com', 'Ganesh Dham Colony ', 'Home Loan', 837, '9755468824', '500000', '', '2019-07-14 09:24:13'),
('Bishwamitra Roy', 'Roy', '8119820913', 'Trishularoy277@gmail.com', 'Ananda bazaar north tripura', 'Personal Loan', 838, '8119820913', '100000', '', '2019-07-14 09:31:53'),
('Broockshield', 'Sangma', '9774558417', 'mbroockshield@gmail.com', 'Deimseiniong', 'Personal Loan', 839, '9862957494', '60000', '', '2019-07-14 09:49:06'),
('Vikki ', 'Saini', '9166337226', 'Vikkisaini580@gmail.com', '247 indracolony alwar', 'Personal Loan', 840, '9166337226', '30000', '', '2019-07-14 10:02:06'),
('Jessy', 'boby', '9526850162', 'jessyboby162@gmail.com', 'Kulathoor house pandappilly', 'Personal Loan', 841, '+919526850162', '100000', '', '2019-07-14 10:28:31'),
('Surendra kumar', 'Maurya', '8417908783', 'surendram898@gmail.com', 'Bhahadiya gaura pipra Mihinpurwa Bahraich', 'Personal Loan', 842, '8417908783', '150000', '', '2019-07-14 10:34:24');
INSERT INTO `enquiry_master` (`fname`, `lname`, `mobile`, `email`, `address`, `loan_type`, `trial_id`, `alt_mobile`, `amount`, `status`, `entry_date`) VALUES
('Nitesh', 'tank', '8740002124', 'tanknitesh50@gmail.com', 'H.no.115 bajrangpura sakatpura kota', 'Personal Loan', 843, '8209432652', '200000', '', '2019-07-14 10:35:33'),
('Kayalvizhi', 'Rajamanickam', '9940927780', 'kayalvizhi.r23@gmail.com', '12/9, iyyanarappan koil street, iyyappa Nagar east kanchipuram 631501', 'Personal Loan', 844, '6383909027', '30000', '', '2019-07-14 10:42:46'),
('Shabana begum ', 'Begum ', '8123748465', 'Mohamedarhan1981@gmail.com', '#18, ISAQ SAHIBE STREET,J.C NAGAR', 'Personal Loan', 845, '08123748465', '300000', '', '2019-07-14 10:56:03'),
('Ilaiyaraja', 'Subramanian', '8508510457', 'ilaiyarajar187@gmail.com', '28/a John thoppu bharathiyarst trichy', 'Personal Loan', 846, '9344767995', '60000', '', '2019-07-14 11:00:37'),
('Kunal', 'Gupta', '9797547378', 'Kunalgupta11788@gmail.com', '503 b vignaharta apartments sector 7 koparkhairane ', 'Personal Loan', 847, '9137726056', '30000', '', '2019-07-14 11:09:27'),
('Nilesh', 'Sirwani', '7741891955', 'nileshsirwani77@gmail.com', 'Nanak nagar shivaji nagar amravati', 'Personal Loan', 848, '8862080698', '50000', '', '2019-07-14 11:09:27'),
('Deepak', 'Kumar', '7009909243', 'Dhiman13deep@gmail.com', 'House no 298, vill- cunnikalan', 'Personal Loan', 849, '8054375069', '200000', '', '2019-07-14 11:12:57'),
('SALMAN', 'ABBASH', '8630542911', 'Salmanabbash562@gmail.com', 'Nagla devjeet motimahal agra', 'Personal Loan', 850, '7417165478', '100000', '', '2019-07-14 11:18:23'),
('Ram', 'Lal', '9736584327', 'ramlal97365@gmail.com', 'v.p.o Dari  Tehsil Dharam Shala  Distt.Kangra Himachal Pradesh', 'Personal Loan', 851, '9882289407', '500000', '', '2019-07-14 11:20:27'),
('AKHTAR', 'AKHTAR', '9050197849', 'raheeskhan13198@gmail.com', 'VILL BIBIPUR PO UJINA TEH NUH DIST NUH', 'Auto/Vehicle Loan', 852, '9050197849', '100000', '', '2019-07-14 11:24:09'),
('Lalit', 'Chahar', '9034123974', 'Lalitna336@gmail.com', 'V.P.O silani Kesho Pana District jhajjar haryana', 'Education Loan', 853, '9034123974', '250000', '', '2019-07-14 11:28:07'),
('Suvarna pawar', 'Suvarna awar', '9945172565', 'Bunny.gowda.ha.@gmail.com', '17th Word bagapalli', 'Personal Loan', 854, '9945172565 ', '200000', '', '2019-07-14 11:35:14'),
('ddharmendrasingh9449@gmail.com', 'ddharmendrasingh9449@gmail.com', '8429431244', 'ddharmendrasingh9449@gmail.com', 'Lucknow', 'Personal Loan', 855, '8429431244', '100000', '', '2019-07-14 11:38:37'),
('Pawandev j chauhan', 'Jaydevbhai valji chauhan', '7600653676', 'pavandevbhadaru8899@gmail.com', 'NCC cmpound vejitebale road Morbi 2', 'Personal Loan', 856, '9725847619', '260000', '', '2019-07-14 11:57:27'),
('Shamshad', 'Shamshad', '9760002323', 'ahmed.shamshad76@gmail.com', '202 Laddhawala', 'Personal Loan', 857, '8077191841', '100000', '', '2019-07-14 12:01:23'),
('RAJA', 'BABU', '7060041704', 'rajababu7304@gmail.com', 'Shiv Nagar, Niwari Kalan, Mahewa-Achhlda Rod.', 'Personal Loan', 858, '7060041704', '200000', '', '2019-07-14 12:07:31'),
('Rahul', 'Rahul', '9653253062', 'rahulshori210@gmail.com', 'H no.496 , Sarai sant ram , Amritsar', 'Personal Loan', 859, '9877189867', '1000000', '', '2019-07-14 12:09:58'),
('Niahant', 'Pateriya', '7999771759', 'Niahant_pateriya@gmail.com', '89/c,Rajendra Nager indore', 'Personal Loan', 860, '7999771759', '500000', '', '2019-07-14 12:17:15'),
('Amit ', 'Shaw', '9073330194', 'amit.shaw.edu@gmail.com', '87 BAKAR MAHAL SADAR BAZAR, BARRACKPORE', 'Personal Loan', 861, '9831162084', '50000', '', '2019-07-14 12:17:37'),
('Adarsh', 'G', '9380040914', 'adarshrajg1999@gmail.com', '#1462 NS road devraja mohalla, Mysore', 'Personal Loan', 862, '9663954461', '5000', '1', '2019-07-17 10:26:54'),
('Saleem ', 'Miya', '7206759350', 'Saleemmian78@gmail.com', 'Alora electronics shop no 24 aggsain Mkt Opp bsnl office ', 'Personal Loan', 863, '9359442000', '100000', '', '2019-07-14 12:19:35'),
('Anandhakumar', 'Jagadeesan', '9398531147', 'J.anandhakumaar0323@gmail.com', 'Tip top cotton bazaar municipality opp', 'Personal Loan', 864, '9444553864', '2500000', '', '2019-07-14 12:20:40'),
('Akshay', 'Malto', '7766998541', 'akahaymalto1990@gmail.com', 'New Market Patratu dist-Ramgarh Jharkhand', 'Personal Loan', 865, '+917766998541', '5000000', '', '2019-07-14 12:27:54'),
('Sourav', 'Khandey', '8005954698', 'manishskhandey@gmail.com', '828, Sita Badi Dhnka Colony, Ward No- 11, Jhotwara, Jaipur', 'Personal Loan', 866, '7062561203', '20000', '', '2019-07-14 12:29:06'),
('Anuradha', 'Rankule', '8446177329', 'kolianuradha227@gmail.com', '603, B wing Buttercup CHS, Near father Agnel school, Ambernath', 'Personal Loan', 867, '8080793878', '50000', '', '2019-07-14 12:34:16'),
('Musthafa ', 'Vs', '9847086187', 'Musthafa.Vs@gmail.Com', 'Vellakkattu house  vengola po chelakkulam', 'Personal Loan', 868, '6282374490', '10000', '', '2019-07-14 12:36:57'),
('DIVYA JS', 'SOMAN NSIR', '8714522859', 'divyaalex.diya@gmail.com', 'Aswathy near ammankovil, Nedumangad.trivandrum', 'Personal Loan', 869, '08714522859', '200000', '', '2019-07-14 12:37:01'),
('Anis ', 'Fathima', '7397244483', 'aneesfathibe@gmail.com', 'No:17,Ramesh Nagar 2nd cross street,west tambaram', 'Home Loan', 870, '7358633683', '30000', '', '2019-07-14 12:54:17'),
('Roja', 'Roja', '7353805019', 'mahendraroja1@gmail.com', 'Gundlupete', 'Personal Loan', 871, '7353805019', '50000', '', '2019-07-14 12:54:45'),
('Sagar', 'parakh', '9850646613', 'sagar27883@gmail.com', 'Shashrtinagar yerawda', 'Personal Loan', 872, '7218046845', '500000', '', '2019-07-14 13:00:17'),
('Jagbir', 'Singh', '8053787150', 'jagbirkashyap33867@gmail.com', 'Gurgaon rural', 'Personal Loan', 873, '9485706541', '50000', '', '2019-07-14 13:13:28'),
('Pawan ', 'Gond', '6303695694', 'pawank78664@gmail.com', 'Village hariapar post mishanari dist deoria ', 'Home Loan', 874, '6303695694', '500000', '', '2019-07-14 13:21:53'),
('NAGMA PARVEEN', 'Nagma', '9380854676', 'syedaaleema74951@gmail.com', 'Kuvempu nagar 2nd stage, House no 32', 'Personal Loan', 875, '9986825466', '300000', '', '2019-07-14 13:22:00'),
('DHANANJAY', 'SHARMA', '8757977108', 'DHANANJAYSHRM3@GMAIL.COM', 'K G ASHRAM, KOLAKUSMA, KORADIH, HARI OM NAGAR', 'Personal Loan', 876, '8340180477', '200000', '', '2019-07-14 13:35:37'),
('Supriya ', 'Bhalerao', '7972890782', 'supriyabhalerao7700@gmail.com', 'Geeta nagar nanded', 'Education Loan', 877, '7888228088', '20000', '', '2019-07-14 13:39:10'),
('Santosh', '', '8287800057', 'ak512978@gmail.com', 'E-620 JJ colony camp no2 nangloi delhi', 'Personal Loan', 878, '9717344042', '100000', '', '2019-07-14 13:42:58'),
('Ashish', 'Kohli', '7303806665', 'akohli061@gmail.com', 'D 39 , Sandhu appartment , flat no. 4 , 1st floor , Indra enclave, sainik farm , New delhi', 'Personal Loan', 879, '9897403749', '500000', '', '2019-07-14 13:51:33'),
('Nikhitha R ', 'Nikhitha R ', '9188596127', 'nnikhitharejikumar@gmail.com', 'Valiyaparambil Thekkethil Aranootimanagalam Po mavelikara', 'Education Loan', 880, '9188596127 ', '300000', '', '2019-07-14 13:55:43'),
('Jutendra', 'Varma', '9828566751', 'oct25_inout@yahoo.com', 'A14/11, COMFORT ZONE APARTMENT, BALEWADI baner pune 411045', 'Personal Loan', 881, '7014884032', '200000', '', '2019-07-14 13:58:04'),
('Ponmani', 'Jayarei', '8675894988', 'Santhiya jayarei19@gmail.com', '18.2.4cheekapatti main stret. Chinnalapatti  Dingal', 'Personal Loan', 882, '6384409524', '50000', '', '2019-07-14 14:06:38'),
('Muzaffar ', 'Hussain', '8058485442', 'Hussainmuzaffar1995@gmail.com', 'Bandha basti nahri ka naka sastri nager jaipur ', 'Personal Loan', 883, '7297805380', '300000', '', '2019-07-14 14:09:02'),
('Mahendran', 'S', '9003606764', 'jaikiran.vs@gmail.com', '25, gokulam colony , kovaipudur, Coimbatore- 641001', 'Personal Loan', 884, '9843988560', '50000', '', '2019-07-14 14:25:46'),
('Pallvi', 'Sharma', '7018606885', 'sjeenu22@gmail.com', 'Co omprakesh vpo Jagatsuk teshil Manali dusty Kullu HP 175143', 'Personal Loan', 885, '9816139358', '100000', '', '2019-07-14 14:26:02'),
('Subho', 'Das', '8101822478', 'subhodas9840@gmail.com', 'RAIGANJ, BINFOLE', 'Personal Loan', 886, '08101822478', '50000', '', '2019-07-14 14:26:43'),
('Joly Anto', 'Esayad', '9526844674', 'enochantoessayas@gmail.com', 'Vpv hall panavila kulathoor pozhiyoor', 'Personal Loan', 887, '7025300473', '25000', '', '2019-07-14 14:30:31'),
('Hussain', 'Basha', '9884488031', 'Hussian007@gmail.com', 'Amuthiya mosque garden 1 st saligramam chennai', 'Personal Loan', 888, '7010519712', '300000', '', '2019-07-14 14:32:24'),
('Akshay ', 'Burghate', '8208236848', 'burghateakshay147@gmail.com', 'Subhash ward', 'Personal Loan', 889, '7875922128', '100000', '', '2019-07-14 14:49:13'),
('Manoj', 'Majhi', '9136875291', 'mangumajhi100@gmail.com', 'At-budhiabar po-gorual ps-puri Sadar, Same, Same', 'Property Loan', 890, '9136875291', '100000', '', '2019-07-14 14:51:25'),
('Kulbir', 'Kaur', '6239906874', 'kulbirkaur1595@gmail.com', 'bsnl colony sst nagr patiala', 'Personal Loan', 891, '09115689336', '4000', '', '2019-07-14 14:56:10'),
('Mohammed Faisal', 'K', '9597860618', 'fais8888@gmail.com', '6/A Ayapillai yacoob street Noorullahpet Ambur', 'Personal Loan', 892, '959789068', '100000', '', '2019-07-14 14:56:15'),
('Karthi', 'B', '9677423715', 'eeeproduction96@gmail.com', '3,MGR NAGAR-2,SOOLAI,PERIYA SEMUR-ERODE-4', 'Personal Loan', 893, '08248079097', '50000', '', '2019-07-14 14:57:22'),
('VINOD', 'VYAS', '9168229904', 'vinodvyas4321@gmail.com', '15/6A/1/1  be water tank malunge gaon Pune 411045, Milkat 994 Sunil Nagar .malunge gaon Pune 411045', 'Personal Loan', 894, '7040735291', '50000', '', '2019-07-14 14:57:35'),
('Saba', 'Nikhath', '7097717540', 'Sabanikhath555@gmail.com', 'Navya global apartment macharam', 'Personal Loan', 895, '9701692475', '200000', '', '2019-07-14 14:59:25'),
('Pawan', 'Prajapati', '8630622744', 'pgola213@gmail.com', 'Trans yamuna COLLONY face 2 agra', 'Education Loan', 896, '8630622744', '200000', '', '2019-07-14 15:03:08'),
('Ebrar', 'Ali', '09452140651', 'ebrarali001@gmail.com', 'Village baras ,post madhukarpur raibareli. up', 'Personal Loan', 897, '9455490020', '100000', '', '2019-07-14 15:04:40'),
('HARIRAM', 'SOLANKI', '9602639661', 'solankihariom96@gmail.com', 'Salamgarh road dalot', 'Personal Loan', 898, '9602639661', '100000', '', '2019-07-14 15:05:24'),
('Amit', 'Kanyal', '7602869919', 'amitkanyal1@gmail.com', 'Lohariyasal talla,p.o. kathgharia, haldwani,nainital, Uttarakhand,263139', 'Personal Loan', 899, '7384859868', '50000', '', '2019-07-14 15:08:18'),
('Abhay ', 'Mishra ', '8175853276', 'ajaymishra989389@gmail.com', 'Shyamnath purnagiri nagar sitapur ', 'Education Loan', 900, '6388058285', '30000', '', '2019-07-14 15:10:57'),
('Anuj ', 'Dixit ', '9639537274', 'anujd5518@gmail.com', 'Village  & Post  Ghungchai  PURANPUR  Pilibhit Uttar  Prades', 'Personal Loan', 901, '9045206115', '200000', '', '2019-07-14 15:11:17'),
('Jawed', 'Jangariya', '9723735281', 'www.pateljawed87@gmail.com', 'Adhar card', 'Personal Loan', 902, '7567182353', '150000', '', '2019-07-14 15:14:56'),
('Kuldeep', 'kumar', '8307648207', 'Kuldeepkumar@sagbidaj.org', 'H.no -705/12 salara mohalla ,rohtak', 'Personal Loan', 903, '8199998525', '15000', '1', '2019-07-17 06:27:19'),
('Gali ', 'Ramakrishna', '9908038543', 'galiramakrishna4@gmail.com', 'Door no29-1403,vanthena line,kothapeta,vinukonda,guntur district', 'Personal Loan', 904, '8297451000', '50000', '', '2019-07-14 15:25:13'),
('Har veer ', 'Singh', '7409286909', 'harvir323@gmail.com', 'NAVAL dockyard vishakhapattanam', 'Personal Loan', 905, '7409286909', '400000', '', '2019-07-14 15:27:41'),
('Muthulaxmi', 'Ramesh', '9967228023', 'muthulaxmi565@gmail.com', ', Room no113bajia Nagar Khambadevi Road 90feet dharavi', 'Personal Loan', 906, '+919967228023', '50000', '', '2019-07-14 15:34:34'),
('Gadicherla', 'Indhra', '7993070079', 'vijay404153@gmail.com', 'Gadicherla Pally (Chowdaram)', 'Personal Loan', 907, '9133580413', '1000', '', '2019-07-14 15:35:01'),
('Abrar', 'khan', '7008102331', '1984khanabrar@gmail.com', '1114/2579/433,satabdi Nagar unit 8 bhubanswar', 'Personal Loan', 908, '9777888587', '50000', '', '2019-07-14 15:35:24'),
('PARDEEP', 'Kumar', '8295353726', 'pardeepkumar82421@gmail.com', 'RATTANGARH Kakrali pehowa kurukshetra', 'Personal Loan', 909, '9896330911', '50009', '', '2019-07-14 15:38:38'),
('Chotam', 'Barman', '8101575057', 'chotuasonali@gmail.com', 'Santinogar, kamakhaguri', 'Personal Loan', 910, '8101575057', '60000', '', '2019-07-14 15:48:22'),
('Gurjeet', 'Singh', '7888926772', 'gurjeet1996gill@gmail.com', 'House no 1612 ward no 5 doctar ambedkar colony khanna', 'Personal Loan', 911, '7888926772', '15000', '', '2019-07-14 15:53:32'),
('Tejbhadur', 'Gangwar', '9012888714', 'gangwar. tejbhadur@gmail.com', 'B452 kanshiram nagar single story moradabad', 'Personal Loan', 912, '9012888714', '200000', '', '2019-07-14 15:58:41'),
('Yasmin ', 'Shah', '7567646352', 'Suzenshah978@gemail.com', 'Dhanoli Muslim faliya ', 'Personal Loan', 913, '8238235952', '20000', '1', '2019-07-17 09:36:33'),
('Sreelakshmi ', 'Prakasan ', '8921651789', 'Sreelakshmip388@gmail.com ', 'Chennamanthottam kogal paravur kollam', 'Personal Loan', 914, '8921651789', '100000', '', '2019-07-14 17:15:57'),
('Aman ', 'Gupta ', '9889488327', 'Amangupta1710@gmqil.com', 'Badi imali Chowk uttari khalwa balraampur', 'Personal Loan', 915, '9049491382', '300000', '', '2019-07-14 17:17:24'),
('Vijay Kumar ', 'Goutam ', '7004048435', 'vijayunited2016@gmail.com', 'Anand nagar lohar near shiv mandir saridhela ', 'Property Loan', 916, '9709761443', '1000000', '', '2019-07-14 17:36:29'),
('Mattapalli', 'Harish', '7989273467', 'Padmaharish547@gmail.com', 'Vijayasri jewellers rangirijuveeddi', 'Personal Loan', 917, '9848775389', '300000', '', '2019-07-14 17:41:06'),
('JAYAPRAKASH', 'MANI', '9751523146', 'jpcivil1980@gmail.com', '513kutattimedu brammapuram Vellore Tamil Nadu', 'Personal Loan', 918, '8695495961', '1000000', '', '2019-07-14 17:57:11'),
('Jyoti', 'Devi', '9810405014', 'jyoti9810kumar@gmail.com', 'Wz63 OMVIHAR Uttam nagar', 'Personal Loan', 919, '8860938685', '150000', '', '2019-07-14 18:05:55'),
('Sakina', 'Khatoon', '8240391514', 'khatoon210@gmail.com', 'G3, Banerjee para Garia Kolkata -700084', 'Personal Loan', 920, 'Do', '200000', '', '2019-07-14 18:08:46'),
('muhammed', 'jowhar', '9074345878', 'jawharapz143@gmail.com', 'illickel purayidom vattayal ward alappuzha', 'Personal Loan', 921, '7012329565', 'urgent', '', '2019-07-14 22:29:27'),
('OM', 'PRAKASH', '9899012409', 'opv678@gmail.com', 'H.No 148/1 Rajiv Colony Samaypur Road Sec.56a Near Gujjar Chowk Ballabgarh', 'Personal Loan', 922, '8383049291', '500000', '', '2019-07-15 02:07:50'),
('Ajay', 'Mishra', '9512689380', 'ajaymishra107@gmail.com', '403 Shukan 1 Ramdevnagar satellite Ahmedabad', 'Personal Loan', 923, '7675047333', '50000', '', '2019-07-15 03:59:15'),
('Gopal', 'Hampiholi', '9535315633', 'gops_79178@yahoo.co.in', 'Green Garden Gokul Road', 'Personal Loan', 924, '9743312826', '5000', '', '2019-07-15 04:16:32'),
('Bajrang', 'lal', '7013311671', 'pinkubhati183@gmail.com', 'Badangpet bus stop', 'Auto/Vehicle Loan', 925, '7013311671', '40000', '', '2019-07-15 04:23:52'),
('Inder', 'Kumar', '6202173080', 'indrajeetkumar9576442932@gmail.com', 'Ramgarh ', 'Education Loan', 926, '9576442932', '10000', '', '2019-07-15 04:27:21'),
('Bandela', 'Venkata ', '9962463874', 'Umeshume77@gmail.com', '58/55 thiruvalluvar salai teynampet chennai 600018', 'Personal Loan', 927, '09962463874', '100000', '', '2019-07-15 04:42:20'),
('GURPREET', 'singh', '8437429977', 'preetrandhawa072@gmail.com', 'vill rupowali p.o kathunangal disst  amritsar', 'Personal Loan', 928, '8437429977', '5', '', '2019-07-15 04:55:35'),
('Uma', 'Kant', '9911207205', 'umakant825502@gmail.com', 'B 210, HIMALYA ENCLAVE KHORA', 'Personal Loan', 929, '7017822837', '50000', '', '2019-07-15 05:24:56'),
('Akbar', 'Hussain', '9556098130', 'Bazzarmumbai786@rediffmail.com', 'Odisha', 'Personal Loan', 930, '8280773441', '1500000', '', '2019-07-15 05:38:04'),
('Priya.', 'Palath Mundachalil', '9544627840', 'priyarajeevballb@gmail.com', 'Nivedyam. Kadambur. Edakkad.p.o.', 'Personal Loan', 931, '09544627840', '1000000', '', '2019-07-15 05:54:14'),
('Mundath', 'Chandran', '8547434925', 'chandranmdth@gmail.com', 'Periyanam Periya  Pallikkara kasaragod kerala', 'Personal Loan', 932, '9447034925', '50000', '', '2019-07-15 05:55:05'),
('Muhammad', 'Imtiyazahmad', '6307342674', 'Ffffaas444@gmail.com', 'Fatheganj barabanki u.p', 'Home Loan', 933, '9621457135', '200000', '', '2019-07-15 06:02:50'),
('Mohammad ', 'Akram', '7309831407', 'moakram7309831407@gmail.com', '6/235 Rajni Khand Lucknow Cantonment Dilkusha Bakshi ka Talab Lucknow Uttar Pradesh', 'Personal Loan', 934, '7309831407', '50000', '', '2019-07-15 06:36:45'),
('Vijay', 'Muruganand', '7760153521', 'm.vijaymuruganand84@gmail.com', 'A-206,vakil marigold,vakil whispering Woods, chandapura, Bangalore', 'Personal Loan', 935, '7090233098', '5000', '', '2019-07-15 06:39:03'),
('Mohammed', 'Akram', '730 98 31 407', 'moakram7309831407@gmail.co', '6/235 Rajni Khand Lucknow Cantonment Dilkusha Bakshi ka Talab Lucknow Uttar Pradesh', 'Personal Loan', 936, '99 56 90 18 66', 'Personal loan', '', '2019-07-15 06:39:04'),
('Mahendhiran balraj', 'Balraj', '6381402788', 'Mahe3112@gmail.com', '121, sivanantha colony Gandhinagar', 'Personal Loan', 937, '9787792977', '200000', '', '2019-07-15 07:02:45'),
('ANISH', 'GUPTA', '9631211925', 'anishanand667@gmail.com', 'Main road kuju. At+po:- kuju. Op:- kuju. District:- ramgarh. Jharkhand', 'Personal Loan', 938, '7283083455', '2000', '', '2019-07-15 07:36:59'),
('Shyamkumar', 'Shirsalkar', '7028124202', 'Sshirsadkar@gmail.com', 'S R shirsalkar Ganesh Nagar Dryapur shower OFVarangaon Dist Jalgaon pin 425308', 'Personal Loan', 939, '8007576066', '10000', '', '2019-07-15 07:48:09'),
('Yamini', 'Ko?luru', '7032709279', 'yaminikolluru94@mail.com', '22-901/1A', 'Personal Loan', 940, '8096121805', '50000', '', '2019-07-15 08:06:35'),
('Roshani', 'Pohankar', '8999684390', 'roshanipohabkar22588@gmail.com', 'Wardha', 'Personal Loan', 941, '9049404178', '30000', '', '2019-07-15 08:11:47'),
('Mayur ', 'Raurkar', '7001309159', 'mrkgp11@gmail.com', 'Chota tengra near Kali mandir kharagpur ward no 28 721036', 'Personal Loan', 942, '8116673024', '20000', '', '2019-07-15 08:30:03'),
('Manish', 'Kumar', '9548214429', 'Krishnaminti83950@gmail.com', 'Hapur, utter pradesh', 'Personal Loan', 943, '7078542861', '2,50,000', '1', '2019-07-17 09:22:56'),
('Nadeem', 'Hussain', '9897682509', 'nr832461@gmail.com', 'Village mehndi nagla majra jagmanpur bareilly', 'Education Loan', 944, '8791203825', '50000', '', '2019-07-15 08:59:13'),
('Shenaz', 'S', '7996999935', 'shenazs5658@gmail.com', '#26,1st f cross,22nd main,5th phase Jpnagar', 'Personal Loan', 945, '9663558966', '1,50000/', '', '2019-07-15 09:06:23'),
('G srinath', 'Seenu', '7287869707', 'mounishree143@gmail.com', '18 mathrushri nilaya 6th main 2nd cross balajilayout nagashetty Halli Banglore', 'Personal Loan', 946, '6302989793', '15000', '', '2019-07-15 09:22:21'),
('Khurshed ', 'Alam', '9953882568', 'kda2370@gmail.com', 'E60 gali no 25 sector 23 faridabad Haryana ', 'Personal Loan', 947, '9953882568', '50000', '', '2019-07-15 09:33:09'),
('Sakali Pawan ', 'kumar', '9110782328', 'pk6259311@gmail.com', '1-42/a ,Natavelly  Kothakota mandal, wanparthy (dist)', 'Personal Loan', 948, '9110782328', '50000', '', '2019-07-15 09:43:26'),
('Raju ', 'Das', '8778962038', 'das66311@gmail.com', 'Village. &.P. O Paikan. District Hailakandi. Pin 788155 (Assam) India ', 'Home Loan', 949, '8778962038', '500000', '', '2019-07-15 10:07:02'),
('Ravendra', 'Pandey', '6265426529', 'ravendrapandey30@gamil.com', 'Village dheki posts khutar singrauli mp', 'Personal Loan', 950, '6265426529', '5000000', '', '2019-07-15 10:09:42'),
('Pooja', 'Nalavade', '9552648296', 'poojask2018@gmail.com', 'Shinde chawl nr Ganesh Provision Stores Khandobamal Bhosari Pune', 'Personal Loan', 951, '7385648296', '15000', '', '2019-07-15 10:18:04'),
('Syed ', 'Samreen', '8341000282', ' Sk.samreen784@gmail.com', 'Guntur', 'Personal Loan', 952, '6302563844', '30000', '', '2019-07-15 10:29:19'),
('Baidyanath', 'Dutta', '9934376484', 'rajsumit4u@rediffmail.com', 'Kailashdham appartment, flat no 205,2 ND floor,dimna, MGM medical college road dimna jamshedpur jharkhand', 'Personal Loan', 953, '7903699346', '500000', '', '2019-07-15 10:35:53'),
('Abhujit', 'Majumder', '8787748928', 'abhijitmak@gmail.com', 'Agartala,tripura', 'Auto/Vehicle Loan', 954, '8414024289', '300000', '', '2019-07-15 10:47:51'),
('Dewas ', 'Kumar', '9521998888', 'ranjanachaturvedi010@gmail.com', 'Rundhiya Nagar Bharatpur', 'Personal Loan', 955, '9413909024', '100000', '', '2019-07-15 10:55:58'),
('Shrikrushan', 'sawang', '9822995509', 'shrikrushnasawang555@gmail.com', 'Ganesh bag, pingle wasti, mundhwa', 'Personal Loan', 956, '09822995509', '10000', '', '2019-07-15 11:19:25'),
('Ilsvaradan', 'Rajakannu', '9566186085', 'ilavarasanr85@gmail.com', 'Periyar street', 'Personal Loan', 957, '7448693402', '25000', '', '2019-07-15 11:28:41'),
('Manish', 'Dewangan', '9131851588', 'Manishdewangan859@gmail.com', 'House No. 80 Ward No 22', 'Personal Loan', 958, '9770056447', '50000', '', '2019-07-15 11:45:28'),
('Amit', 'Kumar', '9821171339', 'amit49537@gmail.com', 'Dabri extension house number 79, Nasirpur Road', 'Personal Loan', 959, '91+9821171339', '50000', '', '2019-07-15 11:50:29'),
('Velu ', 'Venuh', '8978470334', 'veluvenuh24@gmail.com', '8-3-231/254/c venkatgiri yousufguda', 'Personal Loan', 960, '8978470334', '30000', '', '2019-07-15 11:51:06'),
('ASIF', 'KHAN', '9594408921', 'asifkhan.3.ak70@gmail.com', 'Room no 21 c sector g1line near Kerala masjid cheeta camp trombay Mumbai Maharashtra 400088', 'Personal Loan', 961, '8652871615', '50000', '', '2019-07-15 11:52:14'),
('Sakthi', 'Kumar', '9994626097', 'sakthikumar220@gmil.com', '3/625ganthijinagar colony kamarajar nagar colony post selam 606314', 'Personal Loan', 962, '9994101385', '200000', '', '2019-07-15 11:56:49'),
('Shibily', 'salu', '9496221805', 'ancysalu2017@gmail.com', 'Thundiparambil House Koottickal P. O,  Kottayam Kerala', 'Personal Loan', 963, '8075899083', '100000', '', '2019-07-15 12:08:23'),
('Sandra', 'Dmello', '7769952260', 'sandradmello75@gmail.com', 'St Dominic Road Ghogele Wadi Holi Vasai(W)', 'Personal Loan', 964, '+919823361708', '200000', '', '2019-07-15 12:18:18'),
('Sushma ', 'S', '7624956177', 'bujjammasushu@gmail.com', 'no.28 sriram layout hirandahalli bangalore 560049', 'Personal Loan', 965, '9606660585', '7000', '1', '2019-07-17 06:50:27'),
('B. Pallavi', 'Bhagavathi', '6304783773', '3146@gamil.com', 'Puranapul Hyderabad', 'Personal Loan', 966, '7095560534', '300000', '', '2019-07-15 12:19:01'),
('Amit', 'Kumar', '8756096477', 'V.kumaramit77@gmail.com', 'C1149 Indra Nagar Lucknow up', 'Personal Loan', 967, '8756096477', '40000', '', '2019-07-15 12:19:29'),
('Sumit ', 'Kumar', '9540299924', 'kumarsumit94431@gmail.com', '271 B Gali no 5 Seva Nagar Meerut road Ghaziabad', 'Personal Loan', 968, '9971952463', '50000', '', '2019-07-15 12:29:06'),
('Jadab', 'Handique', '7842330206', 'chaoyadavhandique738@gmail.com', '11/6/416E,Redhills,Lakdikapul,HYDERABAD, Telengana500004', 'Personal Loan', 969, '8978953775', '10000', '', '2019-07-15 12:30:23'),
('Amit kumar singh ', 'Singh', '8929916788', 'singhamitidbi@gmail.com', 'Home number 708 Gali Number 6c k2 block mahipalpur ', 'Personal Loan', 970, '9205560398', '15000', '', '2019-07-15 12:31:42'),
('Kalpana ', 'Kashyap ', '6388045008', 'kalpanakasyap121@gmail.com', '78/254 Anwar Ganj Phool Wali Gali Kanpur ', 'Personal Loan', 971, '9696070370 ', '50000', '', '2019-07-15 12:44:09'),
('Manjusri', 'Chatterjee gupta', '8697600208', 'cgmanjusri2018@gmail.com', 'D 25 Kala chant PARA Garia kolkata 700084', 'Personal Loan', 972, '9038327104', '50 thousand only', '', '2019-07-15 12:44:25'),
('Pandurang', 'Surve', '9975324474', 'survepandurang43@gmail.com', 'At post Narsinhpur Tal Walwa Dist Sangli', 'Auto/Vehicle Loan', 973, '7798929931', '70000', '', '2019-07-15 12:46:48'),
('Ravi', 'Kumar', '8073736510', 'ravikumarkuar8400@gmil.ccom', 'Sone Na Halli Devara mullur post sidlaghatta Taluk chikkaballapura district', 'Personal Loan', 974, '8073736510', '100000', '', '2019-07-15 12:47:55'),
('Harish ', 'Chand', '9760055718', 'hi.enterprises2233@gmaill.com', 'Village kajipura post malagarh disst Bulandshahr up ', 'Personal Loan', 975, '8445514293', '200000', '1', '2019-07-17 06:50:56'),
('Ajay', 'Tak', '8278696802', 'ajaytak433@gmail.com', '323/17c kagdiwada mount road brampuri jaipur', 'Personal Loan', 976, '7732810971', '25000', '', '2019-07-15 12:56:12'),
('Amar', 'Rasali', '9910284913', 'amar.rasali@gmail.com', 'B-114/1, 206 , Vill Mohammadpur, RK Puram Main, New Delhi- 110066', 'Personal Loan', 977, 'NA', '25000', '', '2019-07-15 12:56:26'),
('Subir', 'barman', '9332997264', 'barmansubir833@gmail.com', 'Barojirakpur.Basirhat, Basirhat Railway Station', 'Personal Loan', 978, '7584935408', '80000', '', '2019-07-15 12:59:00'),
('jaspreet', 'singh', '9256085584', 'singhjaspreet394@gmail.com', '#12, St. No. 6, ANAND NAGAR - B', 'Personal Loan', 979, '7009061103', '3000', '', '2019-07-15 13:00:19'),
('Reena', 'Rajput', '9594597291', 'reenarajput0083@gmail.com', '185bldg 6807room Kannamwar Nagar 2 vikhroli East', 'Personal Loan', 980, '9004327616', '200000', '', '2019-07-15 13:00:49'),
('Anuj', 'Panchal ', '9817611840', 'anujpanchal4611@gmail.co', 'Datter Muzaffarnagar', 'Personal Loan', 981, '8950701034', '70000', '', '2019-07-15 13:05:26'),
('Satyanarayan ', 'Mishra ', '7749064491', 'satya.narayan2008@rediffmail.com', 'At - Nuasasan, Po-Parakena, Via-Gabakund, Dist-Puri', 'Personal Loan', 982, '8908545059', '20000', '', '2019-07-15 13:20:47'),
('DESHRAJ', 'meena', '9982319420', 'DESHRAJ. ME1990@RAJASTHAN.IN', 'VI_RANETA, POST_NARAULI DANG,TEH,SAPOTRA, DIST,KAROLI, RAJ', 'Personal Loan', 983, '9982319420', '100000', '', '2019-07-15 13:28:04'),
('Prudhvi', 'Bhavani', '7993899703', 'prudhvibhavani123@gmail.com', 'Flat no. G4, Sri Lakshmi nilayam appartment , near tdp office ramalingapuram Nellore', 'Personal Loan', 984, '900705699', '50,000', '', '2019-07-15 13:33:59'),
('Bhanu', 'Bhanu', '8800607583', 'Bhanuprtapdehli62@gmail.com', 'F3 gali 1/haus no106 Sangam vihar', 'Personal Loan', 985, '8178896511', '60000', '', '2019-07-15 13:34:19'),
('DESHRAJ', 'meena', '9982319520', 'Dkmeena32220102@gmail', 'VI,Raneta, POST, NAROULI, THE, SAPOTRA, DIST,KAROLI, RJ,PIN,_322203', 'Auto/Vehicle Loan', 986, '9982319420', '2,00000', '', '2019-07-15 13:37:27'),
('Munnagoni', 'Abhinav', '9182261050', 'abhichinna78@gmail.com', '26-102, sundaraya colony, Naspur, Mancherial, 504302.', 'Auto/Vehicle Loan', 987, '8074466305', '30000', '', '2019-07-15 13:43:34'),
('Arup', 'Konwar', '9365277807', 'Simagogoisima9159@gmail.com', 'Vill_Khariabheta, p o_baruanagar, p s_borhat, dist_choraideo, pin_785692', 'Personal Loan', 988, '9365876235', '30000', '', '2019-07-15 13:48:59'),
('Vikss', 'Anduri', '9597020678', 'vikasanduri1414@gmail.com', 'D5 housing, south Sivan kovil, kodambakkam Chennai 600024', 'Personal Loan', 989, '9344897977', '20000', '', '2019-07-15 13:49:38'),
('Shivansh', 'Savita', '9610507678', 'shreyanshshrivastav166@gmail.com', 'F269 vaishali nagar jaipur', 'Personal Loan', 990, '8949974224', '5000', '', '2019-07-15 13:52:16'),
('Mullathadathil', 'sudheesh', '9744734897', 'sudheeshmv767@gmail.com', 'Mullathadathil house, Ponkothra, P. O parappukkara, Pin 680310', 'Personal Loan', 991, '9544020485', '30000', '', '2019-07-15 13:53:31'),
('Sairem', 'Luxmi', '9366269014', 'sairemluxmi123@gmail.com', 'Uripok huidrom leikai', 'Personal Loan', 992, '9862126565', '5000', '', '2019-07-15 13:59:25'),
('Karmjeet ', 'Singh', '9781131282', 'karmjeetranna@gmail.com', 'Harpalpur', 'Personal Loan', 993, '9781139494', '20000', '', '2019-07-15 14:05:23'),
('Prakash', 'Rathore', '6377686276', 'pr1848878@gmail.com', '20-197/96/2753,east nandamuri nagar,jawaharnagar,balajinagar,k.v.rangareddy', 'Personal Loan', 994, '7731857035', '500000', '', '2019-07-15 14:13:09'),
('Salonee', 'Pusalkar', '9967180829', 'salonee.cutie@gmail.com', 'Room b/5 , plot no -18, Dattakrupa CHS , gorai -1, borivali (w) , mumbai', 'Personal Loan', 995, '8454079676', '5000', '', '2019-07-15 14:17:09'),
('GANESAN K', 'GANESAN K', '7592060827', 'ganesankariyakottilmanjeri@gmail.com', 'Kariyakottil (h), Pappinipara(post), Manjeri,Malappuram ,676122', 'Personal Loan', 996, '8943074697', '50,000', '', '2019-07-15 14:19:22'),
('Uma', 'Magheswari', '9489730151', 'Umaramasamy1709@gmail.com', '6kodiyampalaym North Street. uthukuli', 'Personal Loan', 997, '9047396495', '50000', '', '2019-07-15 14:22:38'),
('MUJEEF', 'MOHAMED', '7639697754', 'mujeef565@gmail.com', '116,CMC COLONY,REDFIELDS ROAD', 'Personal Loan', 998, '9036007839', '30000', '', '2019-07-15 14:24:57'),
('SANTOSH', 'PRAJAPATI', '9628575463', 'sk6810055@gmail.com', 'Anand bhavan civil line allahabad', 'Education Loan', 999, '7350172364', '5000', '', '2019-07-15 14:25:15'),
('Deepak', 'Mishra', '9783113029', 'Deppakmishra@gmail.com', 'Bhairu ji ki gali near clock tower sikar', 'Personal Loan', 1000, '9828456828', '100000', '', '2019-07-15 14:26:26'),
('Reddimaasi', 'Sowjanya', '9880385677', 'sowjanyar2902@gmail.com', 'Ram Nagar 1crosd', 'Personal Loan', 1001, '9880385677', '500000', '', '2019-07-15 14:27:25'),
('Gunja ', 'Gaonkar ', '9765451543', 'gunja.gaonkar@gmail.com', 'Goa ', 'Personal Loan', 1002, '9765451543', '150000', '', '2019-07-15 14:32:03'),
('Dimpal ', 'Kumar ', '8219458375', 'abhi06.gcs@gmail.com', 'Village lungoan p/o baroti teh sarkaghat district mandi ', 'Personal Loan', 1003, '8988341170', '10000', '', '2019-07-15 14:32:26'),
('Kandimalla Mani', 'Reddy', '8977165654', 'mani.reddy2107@gmail.com', '1-90-72 Housing Board Colony, Near Vt Temple', 'Personal Loan', 1004, '7981110716', '25000', '', '2019-07-15 14:33:25'),
('Joydip', 'Das', '7001808388', 'joydipdas094@gmail.com', 'Salbagan bishnupur', 'Personal Loan', 1005, '+917001808388', '3000', '', '2019-07-15 14:33:27'),
('Uttam kumar', 'Paul', '8777830766', 'paul.subhankar85@gmail.com', '9/2 circular 2nd by lane., Shibpur', 'Personal Loan', 1006, '9836761739', '100000', '', '2019-07-15 14:34:36'),
('nazmul', 'hossain', '7990417518', 'nmhossain0@gmail.com', '380001', 'Personal Loan', 1007, '9724280282', '25000', '', '2019-07-15 14:35:11'),
('MANI ', 'MAKALAI', '9865030774', 'jananiraghul261999@gmail.com', '12/1 MAHALAKSHMI NAGAR SULUR VIA KANNAMPALAYAM', 'Personal Loan', 1008, '9789190776', '5000', '', '2019-07-15 14:49:14'),
('Varkala', 'Prasad', '7893794640', 'Prasadvarkal@gmail.com', 'SY No 495/C/D/E,, ,Medchal Mandal', 'Personal Loan', 1009, '9948939166', '20000', '', '2019-07-15 14:56:32'),
('Pinki ', 'Mahato', '9903094055', 'pinkimahato9696@gimall.com', 'Arunovo saroni north ghosh para ally haworah', 'Education Loan', 1010, '9007319696', '50000', '', '2019-07-15 14:57:34'),
('Bhuvaneshwari', 'Bhuvaneshwari', '9994514969', 'bharathibhuvi97@gmail.com', 'Pn palayam, Ponninagar', 'Personal Loan', 1011, '9994514969', '50000', '', '2019-07-15 15:12:54'),
('Anusha', 'Joshi', '917 283 5678', 'joshianush4@gmail.com', 'DUraga matha mandir swarget pune, Swami samarth math len no  15, Near rohit vadevale', 'Personal Loan', 1012, '91 866 954 7198', '40000', '', '2019-07-15 15:17:01'),
('Narendra ', 'Nehra', '7357552062', 'Nnahara2021@gamil.com', 'Vill- bakshipura teh-neem ka thana sikar rajasthan india', 'Personal Loan', 1013, '9571542062', '50000', '', '2019-07-15 15:20:40'),
('Gurjit ', 'Singh ', '7986907234', 'gurjitsingh361975@gmail.com', 'Phagwara punjab ', 'Personal Loan', 1014, '7986907234', '200000', '', '2019-07-15 15:27:13'),
('Baboolal', 'Vishwakarma', '8319434107', 'bholanathgeetkar@gmail.com', 'Katni road maihar', 'Home Loan', 1015, '9425885234', '4000000', '', '2019-07-15 15:28:04'),
('AHAMED', 'Mohiuddin', '8421112730', 'ahamedmohiuddin319@gmail.com', '1.13.134 Near maulana azad High school aurangabad Maharashtra ', 'Personal Loan', 1016, '9923788880', '200000', '', '2019-07-15 15:29:58'),
('Mohammad lutfur', 'Sheikh', '9143436424', 'mdlutfarsk1999@gmail.com', '25a,martinpara', 'Personal Loan', 1017, '8617788278', '100000', '', '2019-07-15 15:29:59'),
('Mayuri ', 'Udawant ', '9561914003', 'abhijeetudawant1111@gmail.com', 'Nevasa road, Market yard', 'Personal Loan', 1018, '7057653611', '300000', '', '2019-07-15 15:35:43'),
('Sathya', 'narayan', '8660615989', 'suryaayan32@gmail.com', '01 hebbal colony basavangudi road metagalli post mysore', 'Personal Loan', 1019, '8150859190', '50000', '', '2019-07-15 15:39:38'),
('Venkatesh', 'G', '9900425052', 'venkey555@rediffmail.com', 'Durai swamy building Vivek nagar robertsonpet KGF', 'Personal Loan', 1020, '9902287636', '50000', '', '2019-07-15 15:42:37'),
('Samrat', 'Mondal', '9836967670', 'samratmondal0102@gmail.com', '34 dr. A. N. Paul lane bally howrah 711201', 'Personal Loan', 1021, '94777785645', '50000', '', '2019-07-15 15:47:47'),
('Ravi', 'Singh', '9855935710', 'rs25621@gmail.com', '#3507 Maloya Colony Chandigarh Punjab,160025, #3507 Maloya Colony Chandigarh Punjab,160025', 'Personal Loan', 1022, '7347390420', '100000', '', '2019-07-15 15:49:30'),
('Yogesh', 'Sharma', '8076843021', 'sharmayogesh0567@gmail.com', 'H No 825 Housing Board Colony', 'Personal Loan', 1023, '8851639930', '100000', '', '2019-07-15 15:55:13'),
('Rahul', 'Pawar', '9669531561', 'rp5082542@gmail.com', 'Hasinabad khaknar Burhanpur', 'Personal Loan', 1024, '9669531561', '100000', '', '2019-07-15 15:56:19'),
('Jatinder', 'Singh', '9814785767', 'js4204482@gmail.com', 'Rasulra majri khnna', 'Personal Loan', 1025, '8360969156', '120000', '', '2019-07-15 15:56:28'),
('Rapeekkenaganoor', 'Rapeekkenaganoor', '8971496853', 'rapeekkenaganoor@gmail.com', 'At.post. khodanpur. Taluq.bailhogal.dist.Belgaum. pin.code.591104.', 'Personal Loan', 1026, '8971496863', '150000', '', '2019-07-15 15:57:43'),
('Dalip', 'Singh', '7011710409', 'dalipsingh5551@gmail.com', '1f/90 b-block, baba colony, burari, delhi', 'Personal Loan', 1027, '9717277693', '10000', '', '2019-07-15 15:59:17'),
('MONIKA', 'BHOIRE', '7828255894', 'monikabhoire3473@gmail.com', 'Peer karadiya', 'Personal Loan', 1028, '5757', '50000', '', '2019-07-15 16:18:24'),
('Khadangbam Pearl', 'Chanu', '8794294281', 'pearl.krishand@gmail.com', 'Thangmeiband meisnam leikai', 'Personal Loan', 1029, '9612689452', '50', '', '2019-07-15 16:21:45'),
('Nampo', 'lagachu', '9678383647', 'lagachunampo@gmail.com', 'Ward no 4, Natun nagar, Opp:BCB CARE HOSPITAL, DHEMAJI CHARIALI', 'Personal Loan', 1030, '9678383647', '200000', '', '2019-07-15 16:26:47'),
('Dhananjaya ', 'R ', '8147386120', 'Sheeladhanu.s@gmail.com', 'Rangenahalli Post tarikere T Chikmagalur D', 'Personal Loan', 1031, '8147386120', '30000', '', '2019-07-15 16:27:33'),
('Salim', 'Vasani', '7666132161', 'salimvasani396@gmail.com', '22 apna ghar SAHYOG BLDG no 03 SONA MANSHI CHAWAL AALISHAN SINEMA MUMBRA ', 'Personal Loan', 1032, '8390983262 ', '100000', '', '2019-07-15 16:37:22'),
('Aditya', 'kumar', '9759519211', 'adityashk@gmail.com', 'Rani Awanti Bai Nagar Shikohabad Road, New Chungi , Bhaghipur, Etah 207001', 'Personal Loan', 1033, '6399766021', '5000', '', '2019-07-15 16:39:16'),
('Indrajeet', 'KUMAR', '8076136162', 'indrajeetk093@gmail.com', 'New railway road near post office gurgaon', 'Personal Loan', 1034, '8076136162', '300000', '', '2019-07-15 16:39:53'),
('Fazlu Hoque', 'Hoque', '9265139326', 'monowarhossain@233gmail.com', 'Bohram Gitaldaha Dinhata cooch Behar pH 736175', 'Home Loan', 1035, '9265139326', '15000000', '', '2019-07-15 16:48:35'),
('Nani', 'Barman', '8800446319', 'nanibarman88@gimal.com', 'Kakrmodel', 'Personal Loan', 1036, '9990246425', '700000', '', '2019-07-15 16:49:14'),
('Mohammed', 'Sameer', '7899090218', 'Sameerlanka9@gmail.com', 'usman nagarbhatkal', 'Education Loan', 1037, '8150856878', '100000', '', '2019-07-15 16:59:20'),
('Manju', 'Manju', '8892924764', 'Manjusuni39@gmail.com.com ', '#161 hebbal 1st stage 3rd cross mysore ', 'Personal Loan', 1038, '9916141841', '30000', '', '2019-07-15 17:09:34'),
('Jogdane', 'Shashikant', '8087493009', 'Jogdandshashikant97604@gnail.com', 'Manjri kamal colouny fg 3 godbolewati pune', 'Personal Loan', 1039, '8459886363', '200000', '', '2019-07-15 17:22:50'),
('Girraj', 'Mahawar', '8740881441', 'girraj0077@gmail.com', 'Plot no A24 ajmer road gajsingh pura jaipur', 'Personal Loan', 1040, '6377496159', '20000', '', '2019-07-15 17:30:47'),
('TAVITI ', 'TARAKA RAMA RAO', '8897860989', 'tarakaramaraotaviti7@gmail.com', 'Sattenapalli', 'Personal Loan', 1041, '6281550845', '600000', '', '2019-07-15 17:32:07'),
('S ', 'Sathish kumar ', '7995311200', 'Bhavyachiitra2@Gmail.Com', '4-100 gollapalli  rkpuram post puttur mandalam chittur distct ap', 'Personal Loan', 1042, '9985448351', '50000', '', '2019-07-15 17:32:41'),
('Ponnu', 'Mani', '8973628071', 'ponnumani33@gmail.com', '51 southvaithiyanathapuram street', 'Personal Loan', 1043, '8973628071', '25000', '', '2019-07-15 17:39:34'),
('Fakrul ', 'Islam', '9678856355', 'fakruli666@gmail.com', 'Vill nayatilla po.chamela Bazar ps.R k nagar .dt.karimganj', 'Education Loan', 1044, '6001056206', '300000', '', '2019-07-15 17:44:16'),
('R6', 'Mandpe ', '9075858596', 'gawaisuwarna@gmail.com', 'Jewel apartment Sadguru nagar nashikroad ', 'Personal Loan', 1045, '7083190100', '200000', '', '2019-07-15 17:48:51'),
('RAJENDRA PRASAD', 'T S', '9989090324', 'rajendraprasad.rp934@gmail.com', '19,1,3/2, MANTHAVARI STREET,, MAHARANIPETTA, VISAKHAPATNAM', 'Personal Loan', 1046, '8309532918', '200000', '', '2019-07-15 17:51:02'),
('Alka', 'Singh', '9120726791', 'ts4901179@gmail.com', 'Adarsh nagar kalyanpur Lucknow', 'Personal Loan', 1047, '9721702514', '500000', '', '2019-07-15 17:51:04'),
('Bhagaban ', 'Parida', '9938337496', 'Bhagabanparida175@gmail.com', 'Chauliaganj mathasahi po nayabazar ps chauliaganj pin 753004  dist cuttack', 'Personal Loan', 1048, '9348444105', '200000', '', '2019-07-15 18:04:50'),
('KISMAT ', 'ALI', '9859271255', 'kismatmajida14@gmail.com', 'NO2 CHAULKHOWA P/S MANGALDAI P/O DHALPUR DARRANG', 'Personal Loan', 1049, '9365277479', '200000', '', '2019-07-15 18:09:47'),
('Chhagan', 'Purohit', '8955416097', 'cmpurohit878@gmail.com', 'Ambaji mandir ke samne Ice factory ke piche balaji parler', 'Personal Loan', 1050, '8955416097', '400000', '', '2019-07-15 18:12:31'),
('Afrose', 'LN', '9894766453', 'afraaz1234@gmail.com', '605Gaffoor sahib street konavattam. Vellore', 'Personal Loan', 1051, '9944411787', '50000', '', '2019-07-15 18:18:16'),
('Sarika', 'Salve', '7977116324', 'sarikasalve95@gmail.com', 'B-2vikas nagari phoolpada sai baba mandir virar e', 'Personal Loan', 1052, '9029191118', '30000', '', '2019-07-15 18:51:34'),
('Vijay ', 'Ahuja', '9850098362', 'vijayahuja6678@gmail.com', 'A/2 second floor, Ganesh apartment', 'Personal Loan', 1053, '8698388292', '12000', '', '2019-07-15 19:40:59'),
('Amresh chandra', 'Maurya', '9415532810', 'amreshmaurya4@gmail.com', 'Renukoot sonebhdra', 'Personal Loan', 1054, '7752961994', '30000', '', '2019-07-15 20:27:38'),
('Neeraj ', 'Koli ', '7500600280', 'kolineeraj000@gmail.c', 'Rudrapur udam Singh nagar ', 'Personal Loan', 1055, '7253044800', '200000', '', '2019-07-15 21:11:01'),
('Ilamurugan', 'P', '9025206980', 'vimalcse14@gmail.com', '1/36, Lakshmi Nagar, railway back side , Karaikudi.630003 Sivagangai.dt', 'Personal Loan', 1056, '9025206980', '5000', '', '2019-07-15 22:36:34'),
('Asmita', 'Sakpal', '9769729926', 'Asmita12sakpal@gmail.com ', 'Charkop kandivali ', 'Personal Loan', 1057, '9987533421', '25000', '', '2019-07-15 23:52:41'),
('Gayathri ', 'Nagulavancha ', '9059173793 ', 'gayathri.n.candy@gmail.com ', 'Paritala village, kanchikacherla mandalam, Krishna district, Andhra Pradesh ', 'Personal Loan', 1058, '9177765696 ', '50000 ', '', '2019-07-15 23:53:00'),
('Rahul', 'Bhardwaj', '8851381484', 'rahulbhardwaj0520@gmail.com', 'Gopal nagar nayi Delhi nanak piao second', 'Auto/Vehicle Loan', 1059, '9560260499', '100000', '', '2019-07-16 00:48:29'),
('Solomon', 'Patir', '9878524281', 'patirsolomon@gmail.com', 'Kalbari, Bahphala, Jorhat, Assam', 'Personal Loan', 1060, '9877524281', '100000', '', '2019-07-16 01:24:15'),
('Vemulavada', 'Lokeswararao', '7893870773', 'lokeswararaovemulavada@gmail.com', 'Metta veedhi Street Bannuvada Village Tekkali Mandal 1-232', 'Personal Loan', 1061, '9703030235', '25000', '', '2019-07-16 01:29:38'),
('Anjali ', 'Pradhan ', '9648556903', 'pradhan.vishal56@gmail.com', '239/4 juhi lal colony kanpur', 'Personal Loan', 1062, '8127087104', '500000', '', '2019-07-16 01:30:32'),
('ASIM', 'SHARMA', '9101585109', 'ashimsharma888@gmail.com', 'VILL-PO-MORIDHAL-DIST-DHEMAJI', 'Personal Loan', 1063, '8876393908', '60000', '', '2019-07-16 01:54:12'),
('Pranav', 'Karn', '8800686931', 'pranav.karn91@gmail.com', 'Flat no -112 ,block -B , pocket -3, Rohini , sector - 17', 'Personal Loan', 1064, '9313778485', '20000', '', '2019-07-16 01:57:41'),
('SHIVARAJA', 'SARKAR', '7483688907', 'Surajsarkar701@gmail.com', '#22 sindhnur r h colony no 3', 'Auto/Vehicle Loan', 1065, '7353591041', '50000', '', '2019-07-16 02:11:24'),
('Laxminarayan ', 'Pattanayak', '9078661581', 'pattanayaklaxminarayan79@gmail.com', 'Garage Mahavir lane, garage square, Bhubaneswar', 'Personal Loan', 1066, '9040808458', '2,00,000', '', '2019-07-16 02:24:35'),
('Degala ', 'Soma sekhar ', '9505420893', 'somasekhardegala@gmail.com', 'VEMULURIPADU, 1-24', 'Personal Loan', 1067, '9912170582', '50000', '', '2019-07-16 02:24:47'),
('Vinod', 'zinjurke', '9420122233', 'zinjurkevinod@gmail.com', 'Near sai mandir, Bhosari alandi road bhosari', 'Personal Loan', 1068, '9552936309', '250000', '', '2019-07-16 02:52:53'),
('Aswini kumar', 'sahoo', '8763839762', 'sahoo.aswini86@gmail.com', 'plot no 32, lane 1, tankapani road, bhubsneswar,751018', 'Personal Loan', 1069, '7978941017', '10000', '', '2019-07-16 02:53:46'),
('Raman', 'Kumar', '8307760546', 'ramanlamba617@gmail.com', 'Vill Budani', 'Education Loan', 1070, '8814084061', '20000', '', '2019-07-16 03:10:54'),
('Vishal', 'Singh', '9761662777', 'vt16486@gmail.com', 'Vill nagariya satan post aonla distt bareilly', 'Personal Loan', 1071, '7452808758', '50000', '', '2019-07-16 03:22:54'),
('Punam', 'Devi', '09140902388', 'punamdevigola742@gmail.com', 'Hazaratpur', 'Home Loan', 1072, '09140902388', '5000000', '', '2019-07-16 03:24:24'),
('Vivek', 'Sekaran', '9789538668', 'velkvivek@gmail.com', 'No. 17,  First cross angalamman Nagar muthialpet Puducherry ', 'Personal Loan', 1073, '8248723287', '200000', '', '2019-07-16 03:26:30'),
('Shaik mohammed ', 'Firozuddin ', '9985506300', 'fearozsm@gmail.com', '20/6/6.chwtra.guntur.522003', 'Personal Loan', 1074, '7799338270', '200000', '', '2019-07-16 03:28:39'),
('Navin govind bhai khabhu', 'navin govind bhai khabhu', '7874168322', 'Navinkhabhu@gimeal.com', 'Sarkari taluka pustkaly bhachau kutch', 'Personal Loan', 1075, '7046677021', '50000', '', '2019-07-16 03:30:28'),
('Sanjay kumar ', 'Sanjay kumar ', '9050722325', 'sanjaykumarnayyar961@gmail.com', 'Bahadurgarh ', 'Personal Loan', 1076, '9050722325', '500000', '', '2019-07-16 03:38:51'),
('Debalina', 'Chakraborty ', '7003387896', 'Www.debolinachakraborty922@gmail.com', 'Nabapally, Colony more', 'Education Loan', 1077, '8337027506', '40000', '', '2019-07-16 03:54:30'),
('Bibhisana', 'Patra', '8458032515', 'Kumarbobbyraj2@gmail.com', 'At-patala.po-sasang.ps-champua.dist-keonjhar.odisha-758034', 'Personal Loan', 1078, '9438633515', '200000', '', '2019-07-16 03:54:44'),
('Raju', 'Sorathiya', '7351803588', 'Rajukumar37888@gmail.com', '68-B dayal nagar (indra colony) shahganj agra', 'Personal Loan', 1079, '9012860618', '100000', '', '2019-07-16 04:01:06'),
('Shubhajit', 'Bose', '6290015759', 'shubhajitbose1989@gmail.com', 'Agarpara South Station Road, Nutan Pally', 'Personal Loan', 1080, '7278701578', '100000', '', '2019-07-16 04:02:26'),
('sangeeta', 'singh', '7999978543', 'suhas chowk supla', 'talpuri  B block 59p parijaat', 'Personal Loan', 1081, '9755219141', '400000', '', '2019-07-16 04:05:38'),
('Sugyani', 'Samal', '9078418165', 'samalsugyani@gmail.com', '-404,Gatikrushna green apartment, bhubaneswar', 'Personal Loan', 1082, '8327775415', '200000', '', '2019-07-16 04:07:33'),
('Gopal T', 'Gopal T', '8750207412', 'gopalgopal42679@gmail.com', 'The ambassador sky chef IGI airport complex new delhi', 'Personal Loan', 1083, '8750207412', '300000', '', '2019-07-16 04:09:23'),
('Mohana', 'Matheswaran', '8778227883', 'srisrimohana@gmail.com', 'Salem', 'Personal Loan', 1084, '8220573525', '4000', '', '2019-07-16 04:23:03'),
('M', 'madasamy', '8248020934', 'mkannansamy48374@gmail.com', 'Kprtextil', 'Personal Loan', 1085, '8870692086', '300000', '', '2019-07-16 04:31:38'),
('Dinesh', 'Ahirwar', '7225967368', 'dineshahirwar09767@gmail.com', 'Bajrang colony nowgong chattarpur mp', 'Personal Loan', 1086, '9755934103', '20000', '', '2019-07-16 04:39:24'),
('BOYA ', 'GIRI ', '7842282641', 'giri.suvve143@gmail.com', 'Kurnool ', 'Personal Loan', 1087, '7287968115', '12000', '', '2019-07-16 04:42:16'),
('Bhagabana', 'Jena', '7226873373', 'rihujenaa32145@gmail.com', 'Bhakti nagar,Motibhujpur,mundra,kutchh,Gujarat,370421', 'Personal Loan', 1088, '8249594142', '70000', '', '2019-07-16 04:50:14'),
('Uttam kumar', 'Tripura', '7005336643', 'uttamkumartripura076@mail.com', '', 'Education Loan', 1089, '', '100000', '', '2019-07-16 04:56:15'),
('Vamahi ', 'Mohan', '7780428126', 'Bunnysunny2200@gmail.com', '21/633/B teachers colony hindupur', 'Personal Loan', 1090, '7780428126', '10000', '', '2019-07-16 05:00:31'),
('Jeevan Kumar ', 'bharti', '9939964098', 'Jeevankumar.bharti17286@gmail.com ', 'Rc-1120 Dipak Bihar Khora Colony Ghazibad ', 'Personal Loan', 1091, '9939964098', '100000', '', '2019-07-16 05:00:40'),
('Pardeep ', 'Tiweri ', '9877860838', 'sweetrahul87@yahoo.com', 'Shiva Ji Nager St no 3 house no 4142', 'Personal Loan', 1092, '6284400558', '25000 ', '', '2019-07-16 05:12:46'),
('Fajar K M', 'K M', '9995025605', 'faju2011@gmail.com', 'PulariMahal Kumbalam p.o ernakulam', 'Education Loan', 1093, '9496472274', '60000', '', '2019-07-16 05:12:51'),
('Mahesh', 'Biradar', '9765404163', 'Maheshbiradar99289@gmail.com', 'Near Hanuman temple merces kumarvaddy panjim goa', 'Personal Loan', 1094, '9370274355', '1,00000', '', '2019-07-16 05:17:57'),
('harsh', 'singh', '8840351900', 'hs22022002@gmail.com', 'I-1-d, railway colony', 'Personal Loan', 1095, '7860386584', '10000', '', '2019-07-16 05:46:46'),
('Sonudas ', 'Vaishnav ', '9860343073', 'Sonuvai4247@gmail.com ', 'Aghur post rotegaon vaijapur aaurangabad ', 'Personal Loan', 1096, '9561273897', '200000', '', '2019-07-16 06:04:16'),
('Mohd shabaz', 'QUreshi', '8979323416', '23/587 Wazir pura agra', '23/587 Wazir pura AGRA', 'Personal Loan', 1097, '8979323416', '5 Lakh', '', '2019-07-16 06:22:53'),
('Pankaj ', 'Kumar ', '7018183013', 'Ranaji8628825179@gmail.com', 'Village naisaarli post office kothipura tehsil sadar district bilaspur himachal pradesh ', 'Personal Loan', 1098, '7018183013', '59000', '', '2019-07-16 06:25:46'),
('Sathish', 'Kumar', '8110810270', 'dsathishkumar625@gmail.com', '43,nort street, anupanadi', 'Personal Loan', 1099, '9092394142', '20000', '', '2019-07-16 06:26:37'),
('Prasanta', 'Roy', '7005345487', 'dipanjaliroy2011@gmail.com', 'A D Nagar road no 6 Agartala', 'Personal Loan', 1100, '7085918509', '250000', '', '2019-07-16 06:34:09'),
('sandip', 'kamble', '7387366884', 'kamblesandip166@gmail.com', 'Meerut, Up', 'Personal Loan', 1101, '9536105879', '150000', '', '2019-07-16 06:46:16'),
('Raj', 'Kumar', '7871162672', 'rajkum3979@gmail.com', '10/7,kalaivannar st,hasthampatti,salem-7', 'Personal Loan', 1102, '8220111449', '200000', '', '2019-07-17 13:06:55'),
('Kajal', 'Choudhury', '7675042400', 'kajal.choudhury@hotmail.com', '1-4-879/69/C street no. 8 SBI officers colony gandhinagar bakaram Hyderabad 500080', 'Personal Loan', 1103, '9676762400', '400000', '', '2019-07-17 13:10:31'),
('Diksha', 'Hajare', '9145471767', 'johnnymojes82@gmail.com', 'Usarli post office vaje, tal panvel 410206', 'Property Loan', 1104, '9209666714', '500000', '', '2019-07-17 13:15:39'),
('raju', 'jat', '6367739956', 'jatrakesh909@gmail.com', 'altawa', 'Personal Loan', 1105, '7728823928', '50000', '', '2019-07-17 13:15:40'),
('vishal', 'kumar', '7226972785', 'vishalvanodiya007@gmail.com ', '203shree ram apartment next to gayatri temple around dabholi circle in surat 395004', 'Property Loan', 1106, '7226972785', '300000', '', '2019-07-17 13:33:06'),
('Tamizhiniyan ', 'Kc', '9080116765', 'tamizhiniyan345@gmail.com ', '8 meiyappan street kolappalur Gobichettipalayam erode tamilnadu ', 'Personal Loan', 1107, '8300526521', '50000', '', '2019-07-17 13:34:32'),
('Yogen', 'Rai', '8548872961', 'Yogenr94@gmail.com', 'Venkatareddy house do', 'Personal Loan', 1108, '9353470591', 'Chef', '', '2019-07-17 13:41:35'),
('PRASHANT', 'MORE', '7972252415', 'pmprashant708@gmail.com', 'Serve No 156/4 ,Lane no 3,surewadi,harsul,aurangabad., 431 001', 'Personal Loan', 1109, '7040251057', '100000', '', '2019-07-17 13:49:19'),
('M D', 'FAHEEM', '6300427998', 'mohdshabeerali@gmail.com', 'Patancheru, Hyderabad', 'Personal Loan', 1110, '9032717497', '500000', '', '2019-07-17 13:51:45'),
('NAZEER', 'ALI', '9989357971', 'nazeerali1475@gmail.com', 'Patancheru, Hyderabad', 'Personal Loan', 1111, '9381324549', '300000', '', '2019-07-17 13:54:25'),
('Ramijabi', 'Magdum', '8007257581', 'ramijamagdum833@gmail.com', 'A/p dhulgaon tal_tasgaon dist_sangli', 'Education Loan', 1112, '7558548241', '15000', '', '2019-07-17 13:58:07'),
('Madduri', 'Suman', '8179273146', 'Sumanmadduri@gmail.com', 'H no 2 80 vachunoor thammper kariamager', 'Auto/Vehicle Loan', 1113, '8179273146', '200000', '', '2019-07-17 14:35:25'),
('Santosh', 'Dewangan', '9926111307', 'dewangansantosh11@gmail.com', 'K.k.ward ringrod bhatapara cg', 'Personal Loan', 1114, '8085502066', '85000', '', '2019-07-17 14:35:39'),
('Abhishek', 'Singh', '9359361470', 'as4883069@gmail.com', 'Roshnabad Haridwar Uttarakhand', 'Property Loan', 1115, '6396960414', '800000', '', '2019-07-17 14:39:50'),
('Boyina Revanth ', 'Kumar', '9182800074', 'revanthboyina42@gmail.com', 'ramalingeswar nagar, gulabi road, dno:61-22/2-22, Vijayawada-13', 'Personal Loan', 1116, '9494515090', '5000', '', '2019-07-17 14:45:46');
INSERT INTO `enquiry_master` (`fname`, `lname`, `mobile`, `email`, `address`, `loan_type`, `trial_id`, `alt_mobile`, `amount`, `status`, `entry_date`) VALUES
('Preeti', 'Preeti', '8920341339', 'neetugurjar158@gmail.com', '257 kallupura Laxmi vihar', 'Personal Loan', 1117, '9871554919', '100000', '', '2019-07-17 14:45:56'),
('Suman', 'Daud', '9767427999', 'kirandaudpatil3594@gmail.com', '79b Kolthan Harsul Aurangabad', 'Personal Loan', 1118, '7620327633', '500000', '', '2019-07-17 14:54:25'),
('Arbaz', 'Pathan', '6356656726', 'baraiyanano@gmail.com', 'Nesdi mohala,jafarabad', 'Personal Loan', 1119, '7046572346', '130000', '', '2019-07-17 14:56:25'),
('Sachin', 'gawande', '8796107453', 'Sachingawande0077@gmail.com', 'Waluj ta.gangapur dist.aurangabad', 'Personal Loan', 1120, '+17507790040', '50000', '', '2019-07-17 14:57:39'),
('Kavitha', 'K', '9645280207', 'kavithasubi2011@gmail.com', 'T.C 36/72,Jaya Bhavan, Karali Road,vallakadavu', 'Personal Loan', 1121, '+919645280207', '50000', '', '2019-07-17 14:59:45'),
('Pradeep ', 'Kumar', '8755911221', 'Pk2787370@gmail.com', 'Bulandshahar', 'Personal Loan', 1122, '8755911221', '300000', '', '2019-07-17 15:02:34'),
('Rakesh', 'Sharma', '9007294294', 'sonus4108@gmail.com', '31/2fa,bhujangdhar road,patuah para,liluah,howrah', 'Personal Loan', 1123, '9903863554', '200000', '', '2019-07-17 15:02:37'),
('Lata ', 'Pawar', '9890345765', 'nanaawsarmal248@gmail.com', '402 Guru siddhi sadan apt, Achole gaon chimghar aali, Nallasopara E', 'Personal Loan', 1124, '989034765', '100000', '', '2019-07-17 15:07:01'),
('Ranjay', 'Kishore', '8318406945', ' Ranjay kishore@gmail.com', 'C-5486 Sector 12, Near Taxi Stand', 'Personal Loan', 1125, '8318406945', '10000', '', '2019-07-17 15:11:12'),
('Rupesh', 'shrivastav', '9161581346', 'rupeshshrivastav399@gmail.com', '33 koiriyapar mau utter Pradesh 275306', 'Personal Loan', 1126, '9118762721', '10000', '', '2019-07-17 15:14:26'),
('AMIT', 'GHOSH', '7449589719', 'GHOSHAMIT749@GMAIL.COM', 'NOAPARA', 'Auto/Vehicle Loan', 1127, '8961609756', '100000', '', '2019-07-17 15:17:04'),
('Thamizhselvan', 'A', '9600969265', 'Thamizhkabaddi01@gmail.com', 'C3 police quters 3rd mile thoothukudi', 'Personal Loan', 1128, '7904581509', '500000', '', '2019-07-17 15:18:12'),
('Harish', 'Kumar', '9463172234', 'Sudhanshugarg44@gmail.com', 'House no-333 Sec-3A', 'Personal Loan', 1129, '7508660436', '200000', '', '2019-07-17 15:20:02'),
('Varinder', 'PAl singh', '9569051065', 'varinderpalsingh122@gmail.com', 'Bathinda', 'Personal Loan', 1130, '7814190353', '100000', '', '2019-07-17 15:20:49'),
('Sherine', 'Chithara', '7094048297', 'Www.radhidras87@gmail.com', '23/3kannki street,alagappa colony.', 'Personal Loan', 1131, '9715454402', '1000', '', '2019-07-17 15:23:37'),
('  ??????? ????', '   ??????? ???? ??????', '7024785198', 'rajversingh@gimal.com', '????? ????? ????? ????? ??? ???? ????? ????? ???? ????? ???? ??????', 'Personal Loan', 1132, '7869128798', '800000', '', '2019-07-17 15:27:05'),
('Pradeep kumar', 'Rajput', '7905172998', 'PK7250498@GMAIL.COM', 'C/859 abhay nagar dewa road barabanki', 'Personal Loan', 1133, '8354983070', '50000', '', '2019-07-17 15:31:41'),
('Anmol ', 'Srivastava ', '7317586574', 'anmoltex32@gmail.com', '11/4 sewa gram colony dada nagar kanpur', 'Education Loan', 1134, '7317586574', '10000', '', '2019-07-17 15:34:10'),
('Gopal', 'Pandey', '8076627193', 'gp2901322@gmail.com', 'Keshav kunj apartment near sahani farm govindpuram', 'Personal Loan', 1135, '8076627193', 'Ballia', '', '2019-07-17 15:34:41'),
('Rajendra nath', 'Ghosh', '9674212839', 'Rajendra.ghosh2018@gmail.com', 'Chandannagore,hooghly', 'Personal Loan', 1136, '9831833558', '70000', '', '2019-07-17 15:38:35'),
('Masudur', 'Rahaman ', '8641958572', 'Masudur005@gmail.com', 'Plassey. Nadia. WestBengal', 'Education Loan', 1137, '8641958572', '500000', '', '2019-07-17 15:40:19'),
('Mercy', 'Selton', '9207250332', 'Seltonmj0@gmail.com', 'Menakattu house,kannamali  p.o,kannamali,ernakulam', 'Property Loan', 1138, '9847431279', '100000', '', '2019-07-17 15:41:32'),
('Hara shek', 'Sahadat shek', '9015083460', 'harask1995@gmail.com', 'Vill malti rampur ring road ranchi Jharkhand', 'Personal Loan', 1139, '9547605116', '150000', '', '2019-07-17 15:44:09'),
('Rambhai', 'Patel', '9712611727', 'patelrambhai89@gmail.com', 'Ghar no:-18,At-Po:-Odhav,Ta:-Detroj,Ahmedabad,Pin:-382120', 'Personal Loan', 1140, '9638560699', '50000', '', '2019-07-17 15:52:51'),
('Sumit', 'Patel', '8839208214', 'sumitpatel574@gmail.com', '179, Patel ward, Gadarwara', 'Education Loan', 1141, '9977471834', '280000', '', '2019-07-17 15:55:38'),
('Nitesh ', 'Sulakhe ', '7972534880', 'niteshsulakhe745@gmail.com', 'Sanskar Colony Jalna Road Beed ', 'Personal Loan', 1142, '7808465615', '25000', '', '2019-07-17 16:05:02'),
('Rajesh p ', 'Naidu', '9573506261', 'Naiduprajesh1984@gmail.com', 'Kadapa ', 'Personal Loan', 1143, '9398459503', '500000', '', '2019-07-17 16:06:56'),
('Suresh', 'Janga ', '7981848379', 'Sureshjanga021@gmail.com', 'Narasaropet Guntur basikapuram ', 'Education Loan', 1144, '9603070715', '100000', '', '2019-07-17 16:11:42'),
('Dharmendra kumar ', 'Dharmendra kumar', '8964997363', 'kumardhar115@gmail.com', 'vill+post ratanhari begamganj raisen ', 'Personal Loan', 1145, '8964997363', '100000', '', '2019-07-17 16:14:56'),
('Sagar', 'Dung', '8767970459', 'Sdhanwani34@gmail.com', 'Bkk no 899 room no 32 sec 20, Opp shiv mandir', 'Home Loan', 1146, '8850207072', '300000', '', '2019-07-17 16:20:28'),
('Suresh', 'Kumat', '9655354242', 'sureshmech28@gmail.com', 'Arani', 'Personal Loan', 1147, '9976954242', '150000', '', '2019-07-17 16:30:41'),
('Shruti', 'Rayanagoudra', '9845973988', 'shrayanagoudra27@gmail.com', '191/1 1 st flore 21st main road 2nd stage Banashakari', 'Personal Loan', 1148, '9591140683', '45000', '', '2019-07-17 16:32:11'),
('Niranjan', 'Dubey', '8765711371', 'Niranjan.dubay@gmail.com', 'Nademau thana saurikh disst. Kannauj uttar Pradesh 209728', 'Personal Loan', 1149, '8299383814', '500000', '', '2019-07-17 16:41:43'),
('Chandra', 'Roy', '8389978260', 'chandraroy520@gmail.com', 'Siliguri', 'Personal Loan', 1150, '7031089857', '50000', '', '2019-07-17 16:43:06'),
('Irfan', 'Ahmed', '9797362240', 'khanji.irfan9@gmail.com', 'Gotha Bhagwah Doda, cont 9797362240', 'Personal Loan', 1151, '7006747548', '1000000', '', '2019-07-17 16:43:48'),
('lovkesh', 'lovkesh', '9213853853', 'kavityagi007@gmail.com', 'nehar par wazirpur road jeevan nagar faridabad', 'Personal Loan', 1152, '9873853853', '200000', '', '2019-07-17 16:51:20'),
('Toophan ', 'Singh ', '9667018736', 'Toophansingh1112@gmail.com', 'B/p mundiya teh todabhim karauli ', 'Personal Loan', 1153, '9667018736', '2lak', '', '2019-07-17 16:58:10');

-- --------------------------------------------------------

--
-- Table structure for table `get_newsletter`
--

CREATE TABLE `get_newsletter` (
  `newsltr_id` int(10) NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT 'website',
  `email` varchar(255) NOT NULL,
  `subscription` int(1) NOT NULL DEFAULT '1',
  `entry_by` int(10) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `txn_id` varchar(255) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `payment_method` varchar(50) NOT NULL DEFAULT '0',
  `total_price` double NOT NULL,
  `coupon_discount` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL DEFAULT 'NULL',
  `product_delivery_status` varchar(100) DEFAULT 'New',
  `return_status` varchar(100) NOT NULL DEFAULT 'Null',
  `reason_id` int(50) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_date` datetime NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active',
  `reason_des` text NOT NULL,
  `return_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `o_details_id` int(20) NOT NULL,
  `order_id` int(11) NOT NULL,
  `audio_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_price` double NOT NULL,
  `prod_status` varchar(100) NOT NULL,
  `cancel_status` int(2) NOT NULL DEFAULT '0' COMMENT '0 = not cancel and 1 = cancel',
  `return_status` varchar(100) NOT NULL DEFAULT 'NULL',
  `reason_id` int(11) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `reason_des` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `page_type` varchar(255) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `page_description` text NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active',
  `ip_addr` varchar(100) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(100) NOT NULL,
  `entry_by` int(10) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `title`, `description`, `image`, `entry_date`, `ip_addr`, `entry_by`, `status`) VALUES
(8, 'ADVENTURE TOURS', '      ', 'cc58c384c0f39645c5a0a7104add0cca.jpg', '2018-10-08 14:29:21', '::1', 1, 'Active'),
(9, 'WANT THIS TOO?', '', 'cac73d8bf3ac0e50fadb7287ecf941ff.jpg', '2018-10-08 14:30:18', '::1', 1, 'Active'),
(10, 'BUY ON ENVATO ', '   ', '42fa34a9cad040e2ce57197e48cc0def.jpg', '2018-10-08 14:37:17', '::1', 1, 'Active'),
(11, 'GREAT ADVENTURES', '', 'ec52dd7d439aa56bd87b14752aaa61f8.jpg', '2018-10-08 14:31:04', '::1', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonials_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonials_id`, `name`, `image`, `description`, `location`, `entry_by`, `entry_date`, `ip_addr`, `status`) VALUES
(2, 'TAPASYA', 'c95150a8b5ac8a5a9fa418dcc85d67a0.jpg', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal making it look like readable English\r\n', 'Delhi', 1, '2018-10-10', '::1', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `oauth_provider` varchar(255) NOT NULL,
  `oauth_uid` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `entry_by` int(20) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `oauth_provider`, `oauth_uid`, `mobile`, `plain_password`, `entry_by`, `entry_date`, `ip_addr`, `status`) VALUES
(1, 'Vikram thakur', '94ecec962f3dba6a08484bed4b21bd47', 'vikky.raghuwanshi@gmail.com', '', '', '9871593665', '786810', 0, '2018-06-03 10:01:03', '::1', 'Active'),
(2, 'Jitendra thakur', 'e10adc3949ba59abbe56e057f20f883e', 'jeetsirts19@gmail.com', '', '', '8010533449', '123456', 0, '2017-11-08 19:20:10', '206.72.206.123', 'Active'),
(3, 'Aditya', '827ccb0eea8a706c4c34a16891f84e7b', 'adityatm28@gmail.com', '', '', '9999169816', '12345', 0, '2017-12-08 16:35:11', '47.31.13.178', 'Active'),
(4, 'vikram purviya', 'e10adc3949ba59abbe56e057f20f883e', 'vikky.raghuwanshi@gmail.comm', '', '', '9871593665', '123456', 0, '2018-04-19 17:18:33', '::1', 'Active'),
(5, 'vanti', 'e10adc3949ba59abbe56e057f20f883e', 'banti@admin.com', '', '', '8010533449', '123456', 0, '2018-05-06 14:07:15', '::1', 'Active'),
(6, 'Dr Shirish Singhal', '7689a9cc8291281833c9facf395b0fcf', 'ssinghaldr@gmail.com', '', '', '9826876745', 'Shirish10', 0, '2018-05-27 13:56:49', '171.61.30.81', 'Active'),
(7, 'Anjani', '98a15eb73bb41ee05d647bab5b0a9d5c', 'anjani.kumar@aksoft.in', '', '', '8285318531', '363320', 0, '2018-06-19 05:53:10', '47.31.195.188', 'Active'),
(8, 'Priyanka Yerandekar ', 'f007108461e9e1938e4391f2e7959f86', 'yerandekarpriyanka777@gmail.com', '', '', '9833566507', 'priyanka21', 0, '2018-05-31 15:15:49', '42.109.7.104', 'Active'),
(9, 'Marshall Patel', 'b3d97746dbb45e92dc083db205e1fd14', 'marshallpatel@gmail.com', '', '', '9879002525', 'phoenix', 0, '2018-05-31 17:37:43', '43.242.116.211', 'Active'),
(10, 'Bhanumathy Velendra ', '877f04a2a2ff0981823e65960302cac4', 'banuveli@gmail.com', '', '', '9940687872', 'kambankau', 0, '2018-06-02 03:53:22', '49.205.218.103', 'Active'),
(11, 'Ashna Malhotra ', '114576718507068675b5789faee9bf16', 'ashna@amlf.net', '', '', '9619044072', 'Ashna123', 0, '2018-06-02 05:32:28', '42.109.53.237', 'Active'),
(12, 'Muniraju Muniswamy ', 'bd6d3e9d343e6ecc8c0822b38e30a428', 'drmuniraju30@Yahoo.co.on', '', '', '9880139779', 'FORgot1973@', 0, '2018-06-03 03:01:48', '157.49.137.203', 'Active'),
(13, 'Vipul Kukreja', '81304f0c3562fa005671954b66ff845e', 'vipul.kukreja@gmail.com', '', '', '9820422311', 'janu0603', 0, '2018-06-05 09:22:18', '122.170.130.223', 'Active'),
(14, 'Ajay Sharma', 'b84fc688a8212df8e40117b5a9b3e890', 'ajayrai3666@gmail.com', '', '', '9811633743', '@Ashish1', 0, '2018-06-10 08:00:55', '139.5.253.228', 'Active'),
(15, 'Mugdha gupta', '9ada54d14452ffb91002ce872715cd7a', 'muggu15@gmail.com', '', '', '9999992252', 'piyush2005', 0, '2018-06-10 08:08:37', '42.111.3.27', 'Active'),
(16, 'meeti ', '0a284a52b944eb2b3c6e6d790adaf957', 'meetibatra@gmail.com', '', '', '9717391515', 'amiti1dd', 0, '2018-06-10 08:17:51', '47.31.88.55', 'Active'),
(17, 'Nidhi handa', '151a107aaf483f32b1a32afa56114c6c', 'nidhi.handa9@gmail.com', '', '', '9818044763', 'nhanda999', 0, '2018-06-10 08:38:19', '171.61.128.76', 'Active'),
(18, 'Ekta Chitkara ', '12d30a874f568dc49dc6e75e14c43d7c', 'ektac4@gmail.com', '', '', '9915552912', 'ssdn1088', 0, '2018-06-11 09:32:31', '47.31.159.48', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `enquiry_master`
--
ALTER TABLE `enquiry_master`
  ADD PRIMARY KEY (`trial_id`);

--
-- Indexes for table `get_newsletter`
--
ALTER TABLE `get_newsletter`
  ADD PRIMARY KEY (`newsltr_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`o_details_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonials_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `user_id_3` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `contact_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `enquiry_master`
--
ALTER TABLE `enquiry_master`
  MODIFY `trial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1154;

--
-- AUTO_INCREMENT for table `get_newsletter`
--
ALTER TABLE `get_newsletter`
  MODIFY `newsltr_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `o_details_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonials_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
