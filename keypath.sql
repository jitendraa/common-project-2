-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 14, 2019 at 01:29 AM
-- Server version: 5.6.44-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `keypath`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `plain_pass` varchar(255) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `type` int(10) NOT NULL DEFAULT '1',
  `role` varchar(25) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_by` int(12) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'Active',
  `ip_addr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `fname`, `lname`, `plain_pass`, `phone`, `type`, `role`, `entry_date`, `entry_by`, `status`, `ip_addr`) VALUES
(1, 'admin', '95228f3702f78bbb4cbbd356a01a5e2b', 'anubhav@credencedigital.com', 'admin', '', 'ADMIN!@12', '', 1, '', '2015-09-16 08:57:04', 0, 'Active', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_slug` text NOT NULL,
  `name` varchar(128) NOT NULL,
  `image` varchar(100) NOT NULL,
  `company` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_addr` varchar(128) NOT NULL,
  `status` varchar(128) NOT NULL DEFAULT 'Active',
  `popularity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `title`, `title_slug`, `name`, `image`, `company`, `description`, `meta_title`, `meta_keyword`, `meta_description`, `entry_by`, `entry_date`, `ip_addr`, `status`, `popularity`) VALUES
(4, 'MYSTERY OF PITRU DOSHA IN YOUR LIFEddfdfd', 'mystery-of-pitru-dosha-in-your-lifeddfdfd', 'Sumant And Sumeet Kaul', 'b3c5beaf92fd11ddb9f41f975755d653.png', 'Sumant And Sumeet Kaul', '<p style=\"text-align:justify\"><strong>The most common Karmic trouble seen in almost every house is between daughter-in-law and mother-in-law. Karma Guru ji, can the technique resolve this Karma?</strong></p>\r\n\r\n<p style=\"text-align:justify\">Many people come with the karma of daughter-in-law and mother in law; it&rsquo;s a prevalent karma all over the world. However, people don&rsquo;t realize that it&rsquo;s not an individual karma but it&rsquo;s a family karma. The family travels together with a family karma. Members of the family do not consciously take this karma but the family consciousness decides along with unconscious of the individual to delegate the karma. So whenever there is a mother-in-law daughter-in-law dispute and you can see it in the ancestors also in that family. It means at the great-grandfather level, the mother-in-law died early or had some trauma and couldn&rsquo;t give love to her daughter-in-law and this way the soul has been forgotten. In the family, now, there is a dosha &nbsp;I.e defect regarding not receiving love between mother in law and daughter in law. The new daughter in laws who come in that family unconsciously will take that family karma and out of bonding, sympathy, and love will take the burden of their mother in law&rsquo;s - mother In law to represent them in family because they didn&rsquo;t get love with their daughter in law and to settle that karma the new daughter in law has karma with her mother in law. This script continuously runs until someone in the family consciousness breaks it.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>What can be the underlying cause of migraines and depression?</strong></p>\r\n\r\n<p style=\"text-align:justify\">One lady aborted a child and her next child was having migraines. Her mother started expecting a lot from her because she lost her earlier child and that suppressed grief is stored inside the mother and the present child is identifying with that depression and sadness of lost sibling because that lost sibling hasn&rsquo;t been acknowledged. That&rsquo;s why Kashmir and Palestine and Syria problem cannot be solved unless they acknowledge what&rsquo;s happen to their ancestors and pray for them because the ancestors are giving pain to progeny and it is an endless cycle</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Why do people fall in love, get married and fight and divorce?</strong></p>\r\n\r\n<p style=\"text-align:justify\">One of the major reasons for this is the ancestral defect. Ancestral defect comes from our family members who have not ascended to the highest dimension and we are caught up them through our DNA. For example, Children in the family may behave in an erratic manner become aggressive and angry because of the energetic cord of our ancestors whose desires are left unfulfilled. Our unsatisfied ancestors make us attract business &amp; LIFE partners who are going to betray us and create obstacles. It is affecting the entire world population and the catch is that they do not even know it. Mother sends the child to USA to study and the child then marries a girl and relocates there which deprives the mother of the affection of the child and daughter in law. The child feels happy and attracts a life partner who betrays him and many times have children with sickness</p>\r\n\r\n<p style=\"text-align:justify\"><strong>There are Rituals done for Pitru Dosh eradication. Does it help?</strong></p>\r\n\r\n<p style=\"text-align:justify\">Rituals are just formal processes and not emotional processes. So they might only be mechanically doing it. They do a funeral, but not really release the grief from the DNA AND THIS GRIEF STAYS WITH THE PERSON EATING HIS BODY FROM INSIDE. In cases of inter-caste marriages or family disputes, members are thrown out of family which is a wrong Karma. So, this upsets the order of the family and the parents curse their children and it affects them genetically bringing problems for the children as well as the parents and it stays as a genetic curse even after they die. Only a proper karmic healing can resolve the issue. Brahmin who does the puja ritual himself has ancestral defects. So, the spirit does not listen to that Brahmin as the spirit only listens to mindful and pious yogi.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Karma Guru ji, how can we tackle a child or an adult with attention disorder and extreme anger issues?</strong></p>\r\n\r\n<p style=\"text-align:justify\">In India, mental illness is a taboo. Every physical issue in your life is based on a mental program. For example, a child with attention disorder is aggressive, hyper, anxious or depressed. Even though 50% of the children in the world go through these phases, however, there is some extra drama in their life which makes them have attention disorder. These children exhibit this behavior to attract attention from the mother. They want the mother to acknowledge them. A fetus is the unconscious mind of the mother. The source of this attention seeking behavior comes from the time of conception of the child. Additionally, a miscarriage or an abortion before the conception adds to the fear of the mother for the child that the child will not make it and the energy of the aborted child looms over, Karma Healing facilitates by going to the time and before conception removing all the negative fear and energy which transforms behavior of the child. So, it is not the child who needs healing the most. It is the mother. All the mental illnesses are the result of the unhealed ancestral defect.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>How does my family Karma affect my life?</strong></p>\r\n\r\n<p>Your family karma is like a tree and you are a branch of that tree. A traumatic event in the history of a family can create generational waves impacting future off springs, Divorces, Migrations, Wars or acts of violence, Early deaths, Mothers who died giving birth, Abortions, Exclusions, Dis- eases and Frauds all affect our family karma. One may attract the partner based on the family Karma. Mother-in-law gets daughter-in-law with lot of love and affection to only fight with her later on. This is Karma.</p>\r\n\r\n<p><strong>How can we heal it?</strong></p>\r\n\r\n<p>This can only be healed through special techniques of ancestral defects. Now, most of the readers are married. They will not leave their partner although they might be having marital issues. The beauty of the technique is that the moment you heal the family tree karma, you will observe that the behaviour of your family members and even extended family will completely heal and will become cordial.</p>\r\n\r\n<p>An uncordial relationship of a parent with their father and mother or also between their parents will make the that person behave like father or mother unconsciously because that parent when they were a child were missing the father&#39;s attention and support and this parent starts to look for that support in their children and the child feels burden and ends up losing the childhood at a young age to act like a good father to his mother or father. This spoils the family dynamics and harms relationships in the family. So, it is the mother who has to release the defect. It can only be resolved by following 11 steps of Karma Yoga and releasing the defect in Samadhi state. This defect is continuing in the world for centuries. Karmayog healing is the Original Shiva Technique which people just knows as a mere term but the actual technique was kept a secret for deserving few .In this technique, we see the past as an objective observer and transform the memory of all the present and future Karma in Samadhi State to a joyous or a neutral karma.</p>\r\n', 'Mystery of pitru dosha in your life', 'Mystery of pitru dosha in your life', 'Mystery of pitru dosha in your life', 1, '2018-06-27 09:22:16', '157.38.200.169', 'Active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `contact_id` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_id`, `name`, `email`, `phone`, `message`, `entry_date`) VALUES
(1, 'Jitendra Purviya', 'jitendra.purviya19@gmail.com', '9685243467', 'ssasdsdasd', '2018-11-03 18:29:08'),
(2, 'Patrickvaree', 'bjorn.oste@aventureab.com', '111677282', ' Hi What we arrogate here is , an wonderfultruck \r\n Upstanding click \r\n \r\n \r\nhttp://topratinglist.com?20527', '2018-12-02 03:07:35'),
(3, 'RobertJaw', 'per.augustsson@acousort.com', '131438677', '  What we be durable here is , a okdole \r\n To equipped click on the mutual underneath  \r\n \r\n \r\nhttp://bit.ly/2wlNhB6', '2018-12-10 12:30:40'),
(4, 'Veretillum', 'winnieburington5gx@yahoo.com', '14806175979', 'Ciao! That is a good offers - Fantastic bonus offers that will double or even triple your deposits if you register casino account in the next 24 hours.  Are you in?  http://bit.ly/2J8HfXp', '2018-12-13 08:45:54'),
(5, 'MichaelJoise', 'shopen@anglarna.se', '163725223', '  a okoffers \r\n Honest click on the scrape under the day-star to temper  \r\n \r\n \r\nhttp://bit.ly/2PxuQ0V', '2018-12-14 04:58:48'),
(6, 'DavidCip', 'linnea@vetlandaresebyra.se', '255524886', ' Hi an engrossingcontribution \r\n To preside over click on the trammels underneath  \r\n \r\n \r\nhttp://bit.ly/2UxXP8e', '2018-12-18 02:25:21'),
(7, 'Thomastip', 'kalalu.namaste@gmail.com', '242212363', ' Hey an ill-fatedoffers \r\n Permitted click \r\n \r\n \r\nhttp://bit.ly/2rKMggT', '2018-12-22 00:34:06'),
(8, 'Alfredamupe', 'michael.s.moller@gmail.com', '383137342', ' Hy there,  an captivatingstock up \r\n Are you in?  \r\n \r\n \r\nhttp://bit.ly/2rKOP2v', '2018-12-25 04:18:08'),
(9, 'Williamtew', 'lapergolaorvieto@gmail.com', '157412438', ' Hy there,  Uplifted information ! an enticingoblation \r\n Licit click \r\n \r\n \r\nhttp://bit.ly/2EP11XZ', '2018-12-28 22:09:26'),
(10, 'LEWIS', '19tigrincs1@freemail.hu', '16096819995', 'Hello! That is an important present - more than 400 incredible games & 300 top online slots waiting for you.  Try and be our next winner. http://bit.ly/2yqyF22', '2018-12-30 13:30:28'),
(11, 'Rickeygeack', 'reth.iris@gmail.com', '347771458', '  What we put into place here is , a thingsoffers \r\n Permitted click \r\n \r\n \r\nhttps://drive.google.com/file/d/1iPRDRyO2jf4WcmL2KHogcDaJan03kRjj/preview', '2019-01-02 16:10:23'),
(12, 'Scottmiz', 'ulrikaek.mail@gmail.com', '142344417', '  What we have here is , a fineoffer \r\n To qualify click on the link below  \r\n \r\nhttps://drive.google.com/file/d/1Te3zGsAVTi3z9DtzLIeUKPBzcj1p-bFD/preview', '2019-01-07 22:22:44'),
(13, 'JamesVuh', 'missilesound@gmail.com', '151671634', '  Good news ! an importantoffering \r\n Are you in?  \r\n \r\nhttps://drive.google.com/file/d/1KNqEwq0JU2FCCLAqQuaXv34LP145Yd8w/preview', '2019-01-12 18:02:21'),
(14, 'EdmundPog', 'groombridgewi@gmail.com', '346312557', 'Fasanya Oluwatenq Feyi Osf   Hi niceoffers \r\n To qualify click on the link below    http://bit.ly/2STsU57', '2019-01-16 19:44:16'),
(15, 'JamesthUrf', 'lindstromleif5@gmail.com', '372681876', '  a goodpresent \r\n Just click on the tie below to meet the requirements    http://bit.ly/2Hplg1j', '2019-01-21 14:06:48'),
(16, 'Westonacerb', 'pm.familytrees@gmail.com', '367475435', ' Hi Righteous dope ! a fineoblation \r\n Straight click on the constituent under to suitable    http://bit.ly/2S5nqah', '2019-01-25 15:18:17'),
(17, 'Rhexia', '1948_nina@rambler.ru', '13078899217', 'Good day! There is an important offers - Join us now, and we will double or even triple your first deposit.  To qualify click on the link below. http://bit.ly/2JaPkea', '2019-01-25 19:29:13'),
(18, 'RobertPot', 'ksdsoffice@gmail.com', '133516435', ' Hi Good news ! an importanttender \r\n Just click on the link under to modify    http://bit.ly/2S3yYLf', '2019-01-29 19:38:11'),
(19, 'RobertPot', 'chingforddentallab@gmail.com', '133516435', ' Hi What we clothed here is , a finetender \r\n To condition click on the association below    http://bit.ly/2SaP5qA', '2019-02-02 14:03:26'),
(20, 'RobertPot', 'bonarbridgeardgaygolfclub@gmail.com', '133516435', ' Hi Actual word ! a fineoffer \r\n Just now click on the constituent under to qualify    http://bit.ly/2S3Z1Sr', '2019-02-06 15:19:41'),
(21, 'RobertTaivy', 'ghinilambor700@yahoo.com', '273766686', 'Hello! \r\n \r\nBitcoin price is falling down. What to do? \r\n \r\nYou have to increase the number of coins until the price of Bitcoin starts rising again! \r\n \r\nThe best choice for this is http://dcbtc.info \r\n \r\nDC-BTC increases bitcoins by 10% in 48 hours. \r\nYou will automatically make a profit in to your bitcoin wallet. \r\n \r\nStart participating with small amounts and make a profit tomorrow! \r\nGuaranteed!', '2019-02-11 02:39:26'),
(22, 'Zacharybit', 'chateaumonteil@gmail.com', '371423556', ' Hy there,  Kind information ! a oksacrifice \r\n Are you in?  \r\n \r\nhttp://T5Y8V.TK', '2019-02-14 09:37:44'),
(23, 'HowardRhype', 'dabbott.mail@gmail.com', '185243414', ' Hy there,  Good tidings ! an amazingoffer \r\n Are you in?  \r\nhttp://bit.ly/2S92hfi', '2019-02-19 15:48:26'),
(24, 'RobertZer', '30thhallbookings@gmail.com', '213245362', '  What we be subjected to here is , an amazingdonation \r\n Just click \r\n \r\nhttp://servicerubin.ru', '2019-02-26 23:32:41'),
(25, 'Raghunath', 'miskin.raghu@gmail.com', '7842442944', 'Hi, \r\nI  lost around 25k from you guys. Two persons worked with me but both are waste fellows, they do not have any strategies.\r\nEvery day I got lost, every day -ve only. They are waste.', '2019-03-01 04:02:01'),
(26, 'Leroyalalp', 'housofrolfe@gmail.com', '384872813', 'Confirm that you are not a robot, and learn how to earn $ 1000 a day \r\nhttp://guide-traveler.ru', '2019-03-01 14:31:45'),
(27, 'DonaldCag', 'ghinilambor700@yahoo.com', '331577245', 'Hello! \r\n \r\nDo you know how to get +10% in bitcoins during your coffee break? \r\nSpend this time with benefits. \r\n \r\nMake fast donation to http://dcbtc.club and get automatically payout to your wallet after two days. \r\n \r\nFor example, you donate 100$ in btc today, so you automatically get 110$ day after tomorrow. \r\nReward will come directly to your btc wallet. \r\n \r\nTry and get reward! \r\n \r\nBowered by Blockchain technology.', '2019-03-05 18:39:35'),
(28, 'Lesliefum', 'aglioeoliofulham@gmail.com', '352467411', 'Confirm that you are not a robot, and find out very interesting \r\n \r\nhttp://bit.ly/2EQJlur', '2019-03-06 11:03:35'),
(29, 'Prem Singh Bisht ', 'prem670@yahoo.in', '9319526081', 'I have paid Rs.5000 and again Rs.11500 total 16500 to you as directed by Mr.Avinash Patel and Mr. Kayal for trading by them on my behalf but they have put me on loss to the tune of Rs.30000.  They insisting me to pay 1.2 lakh for another service which gives profit as this is basic service and my losses cannot be recovered.  Please give me proper service through message and registered me with my mobile no.9315526081 mail prem670@yahoo.in ', '2019-03-08 10:10:54'),
(30, 'ThomasRak', 'byntest2019@gmail.com', '218684717', '\"Traffic is expensive\" \r\n \r\n\"Traffic is hard\" \r\n \r\n\"I don’t know where to get traffic from” \r\n \r\nIf you are experiencing this problem, consider social media marketing through influencer traffic on Snapchat. It’s cheap, simple, and straight forward. All we do is post your ad on an influencer’s page that matches your business needs (niche/audience). This traffic is known to convert, and our company is very experienced in social media marketing. If you want more information, contact us now at contactboostyournetwork@gmail.com . We would love to work with you and your company and are excited to help and watch the growth of your business. \r\n \r\nThank you, \r\nAustin Chambliss \r\nCMO Boost Your Network \r\n \r\nTelegram: @austinchambliss \r\nKik: austinchambliss32 \r\nEmail: austinkchambliss@gmail.com \r\nMobile: (858) 531 4722', '2019-03-22 23:43:58'),
(31, 'JamesTrago', 'partogripa@gmail.com', '182175853', 'We offer you the opportunity to advertise your products and services. \r\nHello! Look at a good offering for you. I want to offer the possibility of sending your commercial offers or messages through feedback forms. The advantage of this method is that the messages sent through the feedback forms are included in the white list. This method increases the chance that your message will be read. Mailing is made in the same way as you received this message. \r\n \r\nSending via Feedback Forms to any domain zones of the world. (more than 1000 domain zones.). \r\n \r\nThe cost of sending 1 million messages is $ 49 instead of $ 99. \r\nDomain zone .com - (12 million messages sent) - $399 instead of $699 \r\nAll domain zones in Europe- (8 million messages sent) - $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\n \r\nDiscounts are valid until March 31. \r\nFeedback and warranty! \r\nDelivery report! \r\n \r\nIn the process of sending messages, we do not violate the rules of GDRP. \r\nThis message is created automatically use our contacts for communication. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm2019@gmail.com \r\n \r\nBest wishes', '2019-03-24 05:09:05'),
(32, 'Prem Singh Bisht ', 'prem67047@yahoo.in', '9910784970', 'I have an urgent issue in regard to your services. Please contact me mostly.\r\nPrem ', '2019-03-25 06:04:19'),
(33, 'Waynethifs', 'pete-gvm-affiliate@gmail.com', '386584457', 'My name is Pete and I want to share a proven system with you that makes me money while I sleep! This system allows you to TRY the whole thing for F R E E for a whole 30 days! That\'s right, you can finally change your future without giving up any sensitive information in advance! I signed up myself just a while ago and I\'m already making a nice profit. \r\n \r\nIn short, this is probably the BEST THING THAT EVER HAPPENED TO YOU IF YOU TAKE ACTION NOW!!! \r\n \r\nIf you\'re interested in knowing more about this system, go to http://globalviralmarketing.com/?ref=qkgWOPkN5RoC1NWh and try it out. Again, it’s FREE! \r\n \r\nYou can thank me later \r\n \r\n/Pete', '2019-04-08 15:12:44'),
(34, 'Jamestophy', 'cgorillamail@gmail.com', '266416821', 'Hi, \r\n \r\nI\'ve been visiting your website a few times and decided to give you some positive feedback because I find it very useful. Well done. \r\n \r\nI was wondering if you as someone with experience of creating a useful website could help me out with my new site by giving some feedback about what I could improve? \r\n \r\nYou can find my site by searching for \"casino gorilla\" in Google (it\'s the gorilla themed online casino comparison). \r\n \r\nI would appreciate if you could check it out quickly and tell me what you think. \r\n \r\ncasinogorilla.com \r\n \r\nThank you for help and I wish you a great week!', '2019-04-19 05:08:31'),
(35, 'LouisDiege', 'svetlanacol0sova@yandex.ua', '383772863', 'Grow your bitcoins by 10% per 2 days. \r\nProfit comes to your btc wallet automatically. \r\n \r\nTry  http://bm-syst.xyz \r\nit takes 2 minutes only and let your btc works for you! \r\n \r\nGuaranteed by the blockchain technology!', '2019-04-19 10:06:56'),
(36, 'HarlanMOTTE', 'gunrussia@scryptmail.com', '147337462', '25 charging traumatic pistols shooting automatic fire! Modified Makarov pistols with a silencer! Combat Glock 17 original or with a silencer! And many other types of firearms without a license, without documents, without problems! \r\nDetailed video reviews of our products you can see on our website. \r\nhttp://gunrussia.pw/ \r\nIf the site is unavailable or blocked, email us at - Gunrussia@secmail.pro   or  Gunrussia@elude.in \r\nAnd we will send you the address of the backup site!', '2019-04-28 19:03:06'),
(37, 'FrancesloP', 'angelaenrotoerymn@gmail.com', '17322762868', 'Ciao!  capitalaspire.com \r\n \r\nWe present \r\n \r\nSending your commercial proposal through the Contact us form which can be found on the sites in the contact section. Feedback forms are filled in by our program and the captcha is solved. The superiority of this method is that messages sent through feedback forms are whitelisted. This technique raise the probability that your message will be read. Mailing is done in the same way as you received this message. \r\nYour   message will be seen by millions of site administrators and those who have access to the sites! \r\n \r\nThe cost of sending 1 million messages is $ 49 instead of $ 99. (you can select any country or country domain) \r\nAll USA - (10 million messages sent) - $399 instead of $699 \r\nAll Europe (7 million messages sent)- $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\n \r\n \r\nDiscounts are valid until May 10. \r\nFeedback and warranty! \r\nDelivery report! \r\nIn the process of sending messages we don\'t break the rules GDRP. \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161 \r\nhttp://bit.ly/2V1glKk \r\n \r\nSorry to bother you.', '2019-05-06 10:27:43'),
(38, 'ClairJof', 'feedbackformeu@gmail.com', '222188718', 'While on your great website I felt that my outstanding offer could be a good fit for you. \r\n \r\nI’m happy to say that I’ve come up with a way for you to get your feet wet in \r\nforex without investing 10k or more right away. 3000€ will do it here. \r\n \r\nEver seen such proven profits on MyFxBook? You’ve got to see them with your own eyes. Just go through the links below – opt-in and download a detailed presentation-pdf. \r\n \r\nhttps://www.myfxbook.com/members/OutlierFX/outlierfx/3241807/bMTxDMF2g6KnEToQSQba \r\nhttps://www.outlierfx.com \r\nPlease opt-in to get all info.', '2019-05-10 00:00:45'),
(39, 'Josephchicy', 'michaelasten@gmail.com', '235775574', ' Hy there,  Look what we arrange an eye to you! an amazingoffers \r\n To moderate click on the link underneath  \r\n \r\nhttps://drive.google.com/file/d/10ciuPifVZXjhwW7pJMEj3rrZd2VsjoA1/preview', '2019-05-10 04:26:49'),
(40, 'Aurelionef', 'micgyhaelasten@gmail.com', '135565842', ' Hey What we accept here is , an excitingoblation \r\n To moderate click on the tie-in under the sun  \r\nhttps://drive.google.com/file/d/1ICIxsJGgelpPfwPtm9rqvenA7q10hTaW/preview', '2019-05-19 15:53:08'),
(41, 'GeorgeNew', 'ddggabogados@mail.com', '111618837', 'Dearest in mind, \r\nI would like to introduce myself for the first time. My name is Barrister David Gómez González, the personal lawyer to my late client. \r\nHe worked as a private businessman in the international field. In 2012, my client succumbed to an unfortunate car accident. My client was single and childless. \r\nHe left a fortune worth $24,500,000.00 Dollars in a bank in Spain. The bank sent me message that I have to introduce a beneficiary or the money in their bank will be confiscate. My purpose of contacting you is to make you the Next of Kin. \r\nMy late client left no will, I as his personal lawyer, was commissioned by the Spanish Bank to search for relatives to whom the money left behind could be paid by my deceased client. I have been looking for his relatives for the past 3 months continuously without success. Now I explain why I need your support, I have decided to make a citizen of the same country with my late client the Next of Kin. \r\nI hereby ask you if you give me your consent to present you as the next of kin to my deceased client to the Spanish Bank as the beneficiary.  I would like to point out that you will receive 45% of the share of this money, 45% then I would be entitled to, 10% percent will be donated to charitable organizations. \r\nIf you are interested, please contact me at my private contact details by Tel: 0034-604-284-281, Fax: 0034-911-881-353, Email: ddggabogados@mail.com \r\nI am waiting for your answer \r\nBest regards, \r\nLawyer: - David Gómez González', '2019-05-21 03:15:56'),
(42, 'Samuelrow', 'svetlanacol0sova@yandex.ua', '186778236', 'Let your bitcoins brings you +10% per 2 days. \r\nGet paid automatically and earn again! \r\n \r\nTry  http://dc-btc.site \r\none click registration and getting asset \r\n \r\nPowered by Mutual assistance Algorithm and Blockchain. \r\nWarranty!', '2019-05-21 21:23:33'),
(43, 'Robertgug', 'di289281@gmail.com', '313277542', 'Dear Sir, \r\n \r\nI need your co-operation for a business Partnership. \r\nThis Partnership will give us Hundreds of Millions of Dollars as profit. \r\nKindly write to my personal email address below  so that we can agree on terms and conditions.. \r\nMy Email: h66701824@gmail.com \r\n \r\nSincerely yours, \r\nAndrei Ivanovich Lumpov, \r\nPresident of the expert consulting center ECC. \r\n\"Invest-Project\" Ltd. (Moscow). \r\nEmail: h66701824@gmail.com', '2019-05-29 20:09:50'),
(44, 'Robertsek', 'angelaenrotoerymn@gmail.com', '18011210263', 'Hi!  capitalaspire.com \r\n \r\nWe make offer for you \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication partition. Feedback forms are filled in by our program and the captcha is solved. The superiority of this method is that messages sent through feedback forms are whitelisted. This method improve the odds that your message will be open. Mailing is done in the same way as you received this message. \r\nYour  commercial proposal will be seen by millions of site administrators and those who have access to the sites! \r\n \r\nThe cost of sending 1 million messages is $ 49 instead of $ 99. (you can select any country or country domain) \r\nAll USA - (10 million messages sent) - $399 instead of $699 \r\nAll Europe (7 million messages sent)- $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\nThere is a possibility of FREE TEST MAILING. \r\n \r\n \r\nDiscounts are valid until May 31. \r\nFeedback and warranty! \r\nDelivery report! \r\nIn the process of sending messages we don\'t break the rules GDRP. \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161 \r\nhttp://bit.ly/2Wep74g \r\n \r\nIt\'s time to finish.', '2019-05-31 17:50:55'),
(45, 'capitalaspire.com', 'micgyhaelasten@gmail.com', '278475736', 'capitalaspire.com  Hy there,  Kind news ! a okoblation \r\n Reasonable click on the vinculum below to ready  \r\nhttps://tgraph.io/gridaclalitt1979-05-31', '2019-06-03 01:39:49'),
(46, 'JamesNen', 'intermstores@gmail.com', '286151251', 'Dear Sir, \r\nI bring to your notice this multi million business supply that will benefit you and me. \r\nMy company wants to purchase some raw material from your country urgently. \r\nThis raw material is the major material our company uses in production and for research purposes since 2005 and my boss wants to visit your country to source for this raw material but due to my interest in this business, I have contacted you to stand-in as the supplier to supply this raw material to my boss so that we can share the profit.Get back to me immediately for more details about this business supply on my private email:  intermstores9@gmail.com \r\nSincerely, \r\nJere Haas', '2019-06-04 05:12:15'),
(47, 'ContactForm', 'raphaeenrotoerymn@gmail.com', '375627528', 'Ciao!  capitalaspire.com \r\n \r\nWe present oneself \r\n \r\nSending your commercial proposal through the feedback form which can be found on the sites in the Communication partition. Feedback forms are filled in by our program and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This technique increases the probability that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 99 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\nWhen ordering a newsletter on June 8 and 9, you will be given a 50 percent discount. ( Weekend discount) \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nSkype live:contactform_18 \r\nEmail - ContactForm@make-success.com \r\nWhatsApp - +353899461815', '2019-06-09 06:54:10'),
(48, 'Valery Wakenight', 'valery@biglep.com', '077 1960 8720', 'Business Cooperation - Buy Your Site Traffic\r\n\r\nHow to earn more money with your website ?\r\n	\r\nMedia Advertising Platform runs the #2 largest contextual ads program globally and can generate a great deal of revenue for your website.\r\n\r\nYou will get high converting ad design options, the ad units look like navigation menus, so it will result in a very high  Click Through Rate. \r\n\r\nYou’re also permitted to place ads in sticky or fixed sidebar zones.This is a HUGE benefit.  You cannot place Adsense ads in a floating/fixed/sticky sidebar zone. \r\n\r\nGo here to sign up (bonus 10% revenue for first 3 months) : https://www.biglep.com/earn\r\n\r\n{Thank you|Thanks|Thanks a lot,\r\nValery Wakenight\r\n\r\n	IMPORTANT:  Remember to ask your customer service representative to help you {optimize|improve} ads?their ad design will dramatically increased {revenue|income|earnings}!!\r\n\r\n*Unsubscribe*: Reply \"NO\"', '2019-06-10 13:54:20'),
(49, 'capitalaspire.com', 'micgyhaelasten@gmail.com', '254611127', 'There is an important  gift for victory. capitalaspire.com \r\nhttp://bit.ly/2KAf2eO', '2019-06-11 19:57:05'),
(50, 'VIRENDRAKUMAR GUPTA', 'virugupta@gmail.com', '9867038424', 'I had opted for 16,500 premium service, I am very disappointed with the quality of the service. I was interacting with Pratap Rajput, Ankit Bansal and Arun, I have lost approximately 2,50,000 in just one moth. My request is please do not cheat innocent people', '2019-06-17 16:39:05'),
(51, 'ContactForm', 'raphaeenrotoerymn@gmail.com', '375627528', 'Hi!  capitalaspire.com \r\n \r\nWe put up of the sale \r\n \r\nSending your commercial offer through the Contact us form which can be found on the sites in the contact section. Feedback forms are filled in by our program and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This method increases the probability that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', '2019-06-21 11:12:41'),
(52, 'capitalaspire.com', 'micgyhaelasten@gmail.com', '254611127', 'There is nice  promotion for victory. capitalaspire.com \r\nhttp://bit.ly/2Kwb4DQ', '2019-06-22 12:24:31'),
(53, 'ThomasVably', 'gulfsrv94@gmail.com', '184545235', 'Good day!, capitalaspire.com \r\n \r\nOur customer want to invest in your sector for good yield. \r\n \r\nPlease contact us for more information on  +973 650 09688 or mh@indobsc.com \r\n \r\nBest regards \r\nMr. Mat Hernandez', '2019-06-24 01:43:02'),
(54, 'OuidaMolve', 'fax.promotion@consultant.com', '234725483', 'Are you an individual businessman or a business organization that wants to expand in business but has problem with funding? \r\nIntermediaries/Consultants/Brokers are welcome to bring their clients and are 100% protected. In complete confidence, we will work together for the benefits of all parties involved. \r\nContact us on this email: razulizabiti@consultant.com \r\nRegards, \r\nRazul Izabiti', '2019-06-24 12:02:37'),
(55, 'ankit', 'abc@gmail.com', '8770363734', 'hi', '2019-06-27 12:14:45'),
(56, 'WilliamLouse', 'animatedvideos33@gmail.com', '187215316', 'Hello, I just visited capitalaspire.com and thought I would reach out to you. \r\n \r\nI run an animation studio that makes animated explainer videos helping companies to explain what they do, why it matters and how they\'re unique in less than 2 minutes. \r\n \r\nThis is our portfolio: \r\nhttp://bit.ly/300j4kZ  - do you like it? \r\n \r\nI would love to make an awesome animated video for you guys. \r\n \r\nWe have a smooth production process and handle everything needed for a high-quality video that typically takes us 6 weeks to produce from start to finish. \r\n \r\nFirst, we nail the script, design storyboards you can’t wait to see animated. Voice actors in your native language that capture your brand and animation that screams premium with sound design that brings it all together. \r\n \r\nOur videos are made from scratch and designed to make you stand out and get results. No templates, no cookie cutter animation that tarnishes your brand. \r\n \r\nIf you’re interested in learning more, please get in touch on the email below: \r\nEmail: storybitevideos@gmail.com \r\n \r\nI hope to hear back from you.', '2019-07-02 05:00:51'),
(57, 'JenniSek', 'getconsultation2019@gmail.com', '233564144', 'Are you a designer, marketer from Singapore or do you have an idea for a startup in the field of AR? \r\nStatista.com forecasts AR / VR market valuation of USD 767.67 billion by 2025. \r\nFind out how easy it is to create your augmented reality without a line of code. Article from a professional designer from the team of AR developers! \r\nhttps://arvrjourney.com/an-overview-of-the-programs-that-i-study-and-use-to-develop-augmented-reality-131f6e53c349 \r\nWant to understand the AR market? Subscribe to our channels: \r\nhttps://instagram.com/archy_team \r\nhttps://medium.com/@ARchy_Team \r\nhttps://facebook.com/archyteamar \r\nhttps://www.linkedin.com/company/13987657/ \r\nhttps://twitter.com/archy_team \r\n \r\nNeed a free personal consultation from AR professionals - write to getconsultation2019@gmail.com with the topic WANT A CONSULTATION. \r\n \r\nWe try for subscribers, \r\nArchy team', '2019-07-04 14:07:15'),
(58, 'capitalaspire.com', 'micgyhaelasten@gmail.com', '217448616', 'That is an fabulous  alms for you. capitalaspire.com \r\nhttp://bit.ly/2KzEJMD', '2019-07-05 10:27:02'),
(59, 'Eric', 'eric@talkwithcustomer.com', '416-385-3200', 'Hello capitalaspire.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website capitalaspire.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website capitalaspire.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=capitalaspire.com\r\n', '2019-07-05 23:16:03'),
(60, 'Eric', 'eric@talkwithcustomer.com', '416-385-3200', 'Hello capitalaspire.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website capitalaspire.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website capitalaspire.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=capitalaspire.com\r\n', '2019-07-06 20:18:26'),
(61, 'Sean Thompson', 'SThompsonSr@gmail.com', '9177192583', 'It looks like you\'ve misspelled the word \"intrest\" on your website.  I thought you would like to know :).  Silly mistakes can ruin your site\'s credibility.  I\'ve used a tool called SpellScan.com in the past to keep mistakes off of my website.\r\n\r\n-Sean Thompson Sr.', '2019-07-09 05:44:58'),
(62, 'John Faulkner', 'noreply@mycloudaccounting2976.cat', '334-531-9192', 'Hello,\r\n\r\nAre you looking for a cloud accounting program that makes running your business effortless, fast and secure? Automate duties such as invoicing, organizing costs, monitoring your time and following up with clients in just a few clicks?\r\n\r\nCheck this quick online video : http://www.mycloudaccounting.pw and try it free for 30 days.\r\n\r\nRegards,\r\n\r\nJohn\r\n\r\nNot interested in cloud accounting? Follow this url and we won\'t contact you again : http://unsub.mycloudaccounting.club\r\n\r\nReport as unsolicited mail : http://spam.mycloudaccounting.website', '2019-07-13 23:10:04'),
(63, 'capitalaspire.com', 'micgyhaelasten@gmail.com', '217448616', 'That is an well-fixed  suggest because victory. capitalaspire.com \r\nhttp://bit.ly/2NLw6lk', '2019-07-16 06:30:14'),
(64, 'ContactForm', 'raphaeenrotoerymn@gmail.com', '375627528', 'Good day!  capitalaspire.com \r\n \r\nWe make available \r\n \r\nSending your business proposition through the Contact us form which can be found on the sites in the Communication partition. Feedback forms are filled in by our program and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This technique improve the probability that your message will be read. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +44 7598 509161 \r\nEmail - FeedbackForm@make-success.com', '2019-07-19 00:51:24'),
(65, 'WalterDax', 'casajuana@bos.shopvot.com', '161787168', 'Hello \r\nMy name is Sebastian Perez williams. I am a financial manager in la Caixa bank Spain. I sent you an email previously regarding a 100% sure transaction of 13.5 million going on in our bank. I want to share the secret information about the fund to you. If you follow my instruction with the information I will share with you I m 100% sure that the money will be transferred to your account within 5 days. \r\nMy only interest in this transaction is to get a 50% share of the money being transferred into your account once the deal is over \r\nThis transaction is 100% risk free, I am contacting you because I strongly believe that you are in a better position to execute this business transaction with me, You will definitely be amazed and satisfied when I release the full information about the transaction to you then you will also understand why I chose you for transaction. \r\nKindly respond to my personal email sebastian@ax1perez.com and add your phone number in other to contact you for the full detail. \r\nYours Faithfully, \r\nSebastian Perez williams.', '2019-07-19 02:33:43'),
(66, 'capitalaspire.com', 'micgyhaelasten@gmail.com', '217448616', 'Sate note an odd  vandalize unrivalled an tenderness to winning. capitalaspire.com \r\nhttp://bit.ly/2NJw5yh', '2019-07-19 17:28:33'),
(67, 'Edwarddrili', 'callndata.com@rediffmail.com', '118281218', 'We do Mobile App & Web Application Development and assist in implementation system from our head office in Mumbai (India) and other branches. \r\n \r\nWe have below  ready to use / customize software: - \r\n \r\n1. Custom Mobile app development in multiple language \r\n2. Custom Website and e-commerce system in multiple language \r\n3. ERP /customized software for medium-sized business \r\n4. Hospital Managemeny System \r\n5. Education Institute Managemeny System \r\n6. Events / Artist Management and work flow system \r\n7. Recruitment system \r\n8. System for Trade Intermediates inclduing International \r\n \r\n \r\n \r\nPlease feel to call me anytime on the below mentioned numbers, to discuss your Software Development requirements and I would be glad to assist you. \r\n \r\nTruly, \r\n \r\nRavi Kant \r\ncallNdata Technologies \r\n \r\nskype : ravikant2010 \r\nIndia: +91  8767 477 454 \r\nHong Kong : +852 8191 6996 \r\nMalaysia : +60 115 118 0556 \r\nEmail:  ravi@callndata.com \r\n \r\nAlso looking for associates or sales team in different countries to collaborate in sales or software system development', '2019-07-23 08:54:49'),
(68, 'capitalaspire.com', 'micgyhaelasten@gmail.com', '217448616', 'Look at an anomalous  rise an glad eye to winning. capitalaspire.com \r\nhttp://bit.ly/2NMy3hj', '2019-07-28 08:34:16'),
(69, 'Williamtus', 'raphaeenrotoerymn@gmail.com', '148333551', 'Ciao!  capitalaspire.com \r\n \r\nWe present \r\n \r\nSending your message through the Contact us form which can be found on the sites in the Communication partition. Feedback forms are filled in by our program and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This technique improve the probability that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', '2019-08-03 17:54:00'),
(70, 'http://basnegedist.tk/k7mql', 'ilektr12@mail.ru', '84762986159', 'That is a over-sufficient  perks as a replacement because the doggedness you.  http://senquocachee.ga/b1gcz', '2019-08-11 04:44:18'),
(71, 'Anthonyvot', 'edwardm@lioncourtcrypto.net', '87297933474', 'We buy all crypto currencies at good rate, with reasonable commission between sellers and the mandates. \r\n \r\nContact us with the informations below if you are or have a potential seller. \r\n \r\nTel: +353 1 4378345 \r\nFax: +353 1 6865354 \r\nEmail: edwardm@lioncourtcrypto.com \r\n \r\nThank you, \r\nCrypto currencies Purchase Department, \r\nEdward Molina.', '2019-08-11 06:36:05');

-- --------------------------------------------------------

--
-- Table structure for table `get_newsletter`
--

CREATE TABLE `get_newsletter` (
  `newsltr_id` int(10) NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT 'website',
  `email` varchar(255) NOT NULL,
  `subscription` int(1) NOT NULL DEFAULT '1',
  `entry_by` int(10) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `txn_id` varchar(255) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `payment_method` varchar(50) NOT NULL DEFAULT '0',
  `total_price` double NOT NULL,
  `coupon_discount` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL DEFAULT 'NULL',
  `product_delivery_status` varchar(100) DEFAULT 'New',
  `return_status` varchar(100) NOT NULL DEFAULT 'Null',
  `reason_id` int(50) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_date` datetime NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active',
  `reason_des` text NOT NULL,
  `return_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `o_details_id` int(20) NOT NULL,
  `order_id` int(11) NOT NULL,
  `audio_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_price` double NOT NULL,
  `prod_status` varchar(100) NOT NULL,
  `cancel_status` int(2) NOT NULL DEFAULT '0' COMMENT '0 = not cancel and 1 = cancel',
  `return_status` varchar(100) NOT NULL DEFAULT 'NULL',
  `reason_id` int(11) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `reason_des` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `page_type` varchar(255) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `page_description` text NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active',
  `ip_addr` varchar(100) NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(100) NOT NULL,
  `entry_by` int(10) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `title`, `description`, `image`, `entry_date`, `ip_addr`, `entry_by`, `status`) VALUES
(8, 'ADVENTURE TOURS', '      ', 'cc58c384c0f39645c5a0a7104add0cca.jpg', '2018-10-08 14:29:21', '::1', 1, 'Active'),
(9, 'WANT THIS TOO?', '', 'cac73d8bf3ac0e50fadb7287ecf941ff.jpg', '2018-10-08 14:30:18', '::1', 1, 'Active'),
(10, 'BUY ON ENVATO ', '   ', '42fa34a9cad040e2ce57197e48cc0def.jpg', '2018-10-08 14:37:17', '::1', 1, 'Active'),
(11, 'GREAT ADVENTURES', '', 'ec52dd7d439aa56bd87b14752aaa61f8.jpg', '2018-10-08 14:31:04', '::1', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonials_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `ip_addr` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonials_id`, `name`, `image`, `description`, `location`, `entry_by`, `entry_date`, `ip_addr`, `status`) VALUES
(2, 'TAPASYA', 'c95150a8b5ac8a5a9fa418dcc85d67a0.jpg', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal making it look like readable English\r\n', 'Delhi', 1, '2018-10-10', '::1', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `trial_master`
--

CREATE TABLE `trial_master` (
  `trial_email` varchar(255) NOT NULL,
  `trial_name` varchar(255) NOT NULL,
  `trial_mobile` varchar(255) NOT NULL,
  `trial_msg` text NOT NULL,
  `trial_city` varchar(255) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `services` text NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trial_master`
--

INSERT INTO `trial_master` (`trial_email`, `trial_name`, `trial_mobile`, `trial_msg`, `trial_city`, `trial_id`, `services`, `entry_date`) VALUES
('jitendra.purviya19@gmail.com', 'etwertewtwet', '213123123123', 'fasdfsdfrwe rwerewrwerew', 'New Delhi', 1, 'Stock Cash Tips,Bullion Tips', '2018-11-03 18:38:04'),
('jitendra.purviya19@gmail.com', 'admin@test.com', '23434234', 'sadasdas', 'New Delhi', 2, 'Stock Cash Tips', '2018-11-03 18:38:04'),
('jitendra.purviya19@gmail.com', 'admin@test.com', '23434234', 'sadasdas', 'New Delhi', 3, 'Stock Cash Tips', '2018-11-03 18:38:04'),
('arunrajput14319@gmail.com', 'arun', '9926542772', 'aaa', 'indore', 4, 'Stock Cash Tips', '2018-12-18 13:31:10'),
('kingmax.star@gmail.com', 'Jitendra kumar', '9808972816', '', 'Aligarh', 5, 'Nifty Future Tips', '2019-01-08 13:13:52'),
('amit@yahoo.in', 'Amit agarwal ', '9830557799', '', 'Kolkata', 6, 'Option Tips', '2019-01-16 17:41:21'),
('amit@yahoo.in', 'AMIT', '9830557799', '', 'kolkata', 7, 'Option Tips', '2019-01-17 05:33:23'),
('PAVITRAMITTAL12345@GMAIL.COM', 'PAVITRA', '9074225929', 'WANT SERVICES IN MCX', 'KHANDWA', 8, 'Bullion Tips', '2019-01-22 04:23:27'),
('kingmax.star@gmail.com', 'Nitin kumar', '9808972816', '', 'Agra', 9, 'Bullion Tips', '2019-04-04 12:11:19'),
('mimrotvikash@gamil.com', 'vika', '9993002896', 'jygf', 'nagda', 10, 'Stock Cash Tips', '2019-04-20 12:47:56'),
('mimrotvikash@gamil.com', 'vika', '9993002896', 'jygf', 'nagda', 11, 'Stock Cash Tips', '2019-04-20 12:48:40'),
('arunrajput14319@gmail.com', 'arun', '9926542774', 'sss', 'Indore', 12, 'Stock Cash Tips', '2019-04-25 04:42:00'),
('sumit.khanna31@gmail.com', 'Sumit', '9999613974', '', 'Delhi', 13, 'Stock Cash Tips,Nifty Future Tips,Option Tips', '2019-06-14 08:02:03'),
('virugupta@gmail.com', 'VIRENDRAKUMAR', '9867038424', 'I am interested in Option and Cash trade tips. Kindly provide the same on my WhatsApp no. 9867038424. Would like to try for some time and if found worth would like to go for full fledged service', 'MUMBAI', 14, 'Stock Cash Tips,Option Tips', '2019-06-17 16:33:39'),
('amits@gmail.com', 'Amit', '8821235621', 'Want free trail', 'mumbai', 15, 'Stock Cash Tips', '2019-07-10 10:30:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `oauth_provider` varchar(255) NOT NULL,
  `oauth_uid` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `entry_by` int(20) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_addr` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `oauth_provider`, `oauth_uid`, `mobile`, `plain_password`, `entry_by`, `entry_date`, `ip_addr`, `status`) VALUES
(1, 'Vikram thakur', '94ecec962f3dba6a08484bed4b21bd47', 'vikky.raghuwanshi@gmail.com', '', '', '9871593665', '786810', 0, '2018-06-03 10:01:03', '::1', 'Active'),
(2, 'Jitendra thakur', 'e10adc3949ba59abbe56e057f20f883e', 'jeetsirts19@gmail.com', '', '', '8010533449', '123456', 0, '2017-11-08 19:20:10', '206.72.206.123', 'Active'),
(3, 'Aditya', '827ccb0eea8a706c4c34a16891f84e7b', 'adityatm28@gmail.com', '', '', '9999169816', '12345', 0, '2017-12-08 16:35:11', '47.31.13.178', 'Active'),
(4, 'vikram purviya', 'e10adc3949ba59abbe56e057f20f883e', 'vikky.raghuwanshi@gmail.comm', '', '', '9871593665', '123456', 0, '2018-04-19 17:18:33', '::1', 'Active'),
(5, 'vanti', 'e10adc3949ba59abbe56e057f20f883e', 'banti@admin.com', '', '', '8010533449', '123456', 0, '2018-05-06 14:07:15', '::1', 'Active'),
(6, 'Dr Shirish Singhal', '7689a9cc8291281833c9facf395b0fcf', 'ssinghaldr@gmail.com', '', '', '9826876745', 'Shirish10', 0, '2018-05-27 13:56:49', '171.61.30.81', 'Active'),
(7, 'Anjani', '98a15eb73bb41ee05d647bab5b0a9d5c', 'anjani.kumar@aksoft.in', '', '', '8285318531', '363320', 0, '2018-06-19 05:53:10', '47.31.195.188', 'Active'),
(8, 'Priyanka Yerandekar ', 'f007108461e9e1938e4391f2e7959f86', 'yerandekarpriyanka777@gmail.com', '', '', '9833566507', 'priyanka21', 0, '2018-05-31 15:15:49', '42.109.7.104', 'Active'),
(9, 'Marshall Patel', 'b3d97746dbb45e92dc083db205e1fd14', 'marshallpatel@gmail.com', '', '', '9879002525', 'phoenix', 0, '2018-05-31 17:37:43', '43.242.116.211', 'Active'),
(10, 'Bhanumathy Velendra ', '877f04a2a2ff0981823e65960302cac4', 'banuveli@gmail.com', '', '', '9940687872', 'kambankau', 0, '2018-06-02 03:53:22', '49.205.218.103', 'Active'),
(11, 'Ashna Malhotra ', '114576718507068675b5789faee9bf16', 'ashna@amlf.net', '', '', '9619044072', 'Ashna123', 0, '2018-06-02 05:32:28', '42.109.53.237', 'Active'),
(12, 'Muniraju Muniswamy ', 'bd6d3e9d343e6ecc8c0822b38e30a428', 'drmuniraju30@Yahoo.co.on', '', '', '9880139779', 'FORgot1973@', 0, '2018-06-03 03:01:48', '157.49.137.203', 'Active'),
(13, 'Vipul Kukreja', '81304f0c3562fa005671954b66ff845e', 'vipul.kukreja@gmail.com', '', '', '9820422311', 'janu0603', 0, '2018-06-05 09:22:18', '122.170.130.223', 'Active'),
(14, 'Ajay Sharma', 'b84fc688a8212df8e40117b5a9b3e890', 'ajayrai3666@gmail.com', '', '', '9811633743', '@Ashish1', 0, '2018-06-10 08:00:55', '139.5.253.228', 'Active'),
(15, 'Mugdha gupta', '9ada54d14452ffb91002ce872715cd7a', 'muggu15@gmail.com', '', '', '9999992252', 'piyush2005', 0, '2018-06-10 08:08:37', '42.111.3.27', 'Active'),
(16, 'meeti ', '0a284a52b944eb2b3c6e6d790adaf957', 'meetibatra@gmail.com', '', '', '9717391515', 'amiti1dd', 0, '2018-06-10 08:17:51', '47.31.88.55', 'Active'),
(17, 'Nidhi handa', '151a107aaf483f32b1a32afa56114c6c', 'nidhi.handa9@gmail.com', '', '', '9818044763', 'nhanda999', 0, '2018-06-10 08:38:19', '171.61.128.76', 'Active'),
(18, 'Ekta Chitkara ', '12d30a874f568dc49dc6e75e14c43d7c', 'ektac4@gmail.com', '', '', '9915552912', 'ssdn1088', 0, '2018-06-11 09:32:31', '47.31.159.48', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `get_newsletter`
--
ALTER TABLE `get_newsletter`
  ADD PRIMARY KEY (`newsltr_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`o_details_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonials_id`);

--
-- Indexes for table `trial_master`
--
ALTER TABLE `trial_master`
  ADD PRIMARY KEY (`trial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `user_id_3` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `contact_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `get_newsletter`
--
ALTER TABLE `get_newsletter`
  MODIFY `newsltr_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `o_details_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonials_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `trial_master`
--
ALTER TABLE `trial_master`
  MODIFY `trial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
